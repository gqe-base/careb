  ## Program parameters to be defined before (either as parameters or in a separate file):
  ## dominance : "No" no dominance i.e. G is the mean between the strong and weak effect alleles
  ##             "Max" max dominance i.e. G is the sum of the strong effect alleles
  ##             "Min" min dominance i.e. G is the sum of weak effect alleles
  ##             "Random" random dominance i.e. G is a linear combination between the strong and weak effect alleles
  ## modeRecombination : "Haldane" Poisson law + thinning, recombination without interference
  ##                     "completeInterference" one crossover
  ##                     "freeRecombination" at each locus, probability of 0.5 to have a crossover
  ##                     "noRecombination" no crossover (direct transmission of the haplotype)
  ##                     "interferenceGamma" gamma distribution for the crossovers (if 0 < nu < 1, negative interference, if nu = 1, no interference, if nu > 1 positive interference)
  ##                     "interferenceGamma2" interference two-pathways (frequence of no interfering (nu = 1) co = p and interfering ones = 1-p, with parameters nu
  ## modeCreationGenome : "gaussian" define alleles effect from a given distribution
  ##                      "exist" read the allele effect from a file
  ## modeCreationPosition : "gaussian" take the position of QTLs and SNPs from a given distribution
  ##                        "existGaussian" read the position from a file (positions defined previously with "gaussian")
  ##                        "exist" read the position of the SNPs from a file
  ## reproductionType : "HD" deuble haploidy i.e. recombination for one gamete then duplication of this gamete (one parent)
  ##                    "SP" selfing i.e. recombination for two gametes (one parent)
  ##                    "SC" panmixia i.e. recombination for the two gametes (two parents)
  ## selectionType : "RS" random selection i.e. selection of the individuals randomly in the population
  ##                 "PS" phenotypic selection : selection on P of the x best individuals
  ##                 "MAS" marker-assisted selection
  ##                 "BLUP" genomic selection : estimation of the marker effect (only for normal scheme)
  ##                 BC selection : "PS" (selection of the ind if the QTL is from the donor parent)
  ##                                "MAS" (markers around the QTL from the donor, the others from the recurrent parent)
  ##                                "MASCarrier" (flanking markers around the QTL from the donor, the others from the carrier apart inside QTL interval from the recurrent parent)
  ##                                "MASP" (1 marker from recurrent + PS)
  ##                                "MASQTL" (all markers from recurrent + PS) 
  ##                                "MASQTLCarrier" (all markers from the carrier chr from recurrent + PS)
	##																"MASQTLFlanking"
	##																"MASQTLCrit"
  ## typeSimu : "normal" standard selection schemes
  ##            "secobra" selection scheme adapted for secobra
  ##            "backcross" backcross scheme
  
  
  rm(list = ls())
  #Rprof("~/Documents/toClassDocs/prof6.txt", memory.profiling = TRUE)
  # set.seed(120)
  
  time <- proc.time()
  # source("funMain.r")
  ## sourcing of the files containing the different functions used
  setwd("/home/etourrette/Documents/careb")
  source("funSelection.r")
  source("funAnalysis.r")
  source("funSimulation.r")
  source("funMain.r")
  source("secobra.R")
  source("backcross.r")
  
  ## R command :
  ##$ R CMD BATCH --no-save --no-restore '--args nameDir="20171004ARABIDO" name="Ns10NQTL25DominanceNo" dominance="No" NbQTLs=25 increaseLG=1 shapeParameterPositionQTLs=0.1' LaunchRepeat.r Ns10NQTL25DominanceNo.out &
  
  ## First read in the arguments listed at the command line
  args=(commandArgs(TRUE))
  
  ## args is now a list of character vectors
  ## First check to see if arguments are passed.
  ## Then cycle through each element of the list and evaluate the expressions.
  
  if(length(args)==0){
    print("No arguments supplied.")
    ## supply default values
    
    mainDir <- "/home/etourrette/Documents/articleBC/janvier2020"
    nameDir <- "TEST" ## directory where the simulations will be saved
    nameDirParameters <- "TEST" ## directory of the parameters file
    
    typeSimu <- "backcross"
    # h2 <- 1
    # NbQTLs <- 200
    
    name <- "" ## name of the simulations/folder
    repStart <- 1
    NumberOfRepeats <- 1
  
    
    
  }else{
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
    }
  }
  
  
  nameWD <- file.path(mainDir, nameDir, name)
  
  if(dir.exists(nameWD)) { ## create a directory for the results of the simulation (if it does not already exist)
    setwd(nameWD)
  } else {
    dir.create(nameWD)
    setwd(nameWD)
  }
  
  # dir.create(file.path(nameWD, "Correlation"))
  # dir.create(file.path(nameWD, "LD"))
  
  ## parameter definition from the file parameters.csv
  
  parameters <- read.csv(file.path(mainDir, nameDirParameters, "parameters.csv"), header = FALSE, stringsAsFactors = FALSE)
  parameters <- rbind(c("date", date()), c("script", typeSimu), parameters)
  
  N <- as.numeric(parameters$V2[parameters$V1 == "N"])
  VG <- as.numeric(parameters$V2[parameters$V1 == "VG"])
  h2 <- as.numeric(parameters$V2[parameters$V1 == "h2"])
  # parameters$V2[parameters$V1 == "h2"] <- h2 ## because read it in command line, else no need for it
  shapeParameterPositionQTLs <- as.numeric(parameters$V2[parameters$V1 == "shapeParameterPosition"])
  shapeParameterEffectQTLs <- as.numeric(parameters$V2[parameters$V1 == "shapeParameterEffect"])
  ## increaseLG <- as.numeric(parameters$V2[parameters$V1 == "increaseLG"]) GET RID OF THIS LINE / NO MORE A GENERAL INCREASE
  dominance <- parameters$V2[parameters$V1 == "dominance"]
  seed <- as.numeric(parameters$V2[parameters$V1 == "seed"])
  reuse <- as.logical(parameters$V2[parameters$V1 == "reuse"])
  NbChromosome <- as.numeric(parameters$V2[parameters$V1 == "NbChromosome"])
  # CIRCULARCHROMOSOME <- as.logical(parameters$V2[parameters$V1 == "CIRCULARCHROMOSOME"]) ## put it equal FALSE in funSelection.r file (Haldane function)
  modeRecombination <- parameters$V2[parameters$V1 == "modeRecombination"]
  modeCreationGenome <- parameters$V2[parameters$V1 == "modeCreationGenome"]
  modeCreationPosition <- parameters$V2[parameters$V1 == "modeCreationPosition"]
  homozygous <- as.logical(parameters$V2[parameters$V1 == "homozygous"])
  coupling <- as.logical(parameters$V2[parameters$V1 == "coupling"])
  Lcoupling <- as.numeric(parameters$V2[parameters$V1 == "Lcoupling"])
  # NumberOfRepeats <- as.numeric(parameters$V2[parameters$V1 == "NumberOfRepeats"])
  selectionGeneration1 <- as.numeric(parameters$V2[parameters$V1 == "1stGenerationSelection"]) 
  ## selectionGeneration1 : first generation of selection, before VE = 0, after VE = VG(1-h2)/h2
  ## for exemple : g=0 -> cross 2 lines, g=1 -> 1 F1, g=2 -> f2 => 1st generation of selection = 2 because the selection will be applied from the F2
  ## suppose the heritability is a constant for all generations of selection : it is the environmental variance that changes
  gTrainingPop <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "gTrainingPop"], split = ","))) ## in BC: corresponds to gSelfing ie generations after BC when selfing will be done
  SAVEG <- as.logical(parameters$V2[parameters$V1 == "SAVEGeneration"]) ## if TRUE then save all the generation, if FALSE do not save the data for each generation (generation = list containing the allelic effects for each chromosome and each individual etc); GLOBAL variable (do not put as argument of the functions)
  
  geneticLength <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "geneticLength"], split = ",")))
  # NbQTLs <- rep(NbQTLs, NbChromosome) ## if defined in command line
  # parameters$V2[parameters$V1 == "NbQTLs"] <- paste(NbQTLs, collapse = ",") ## because read it from the command line, else remove it
  NbQTLs <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "NbQTLs"], split = ",")))
  NbSNPs <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "NbSNPs"], split = ",")))
  NbKeepQTL <- NbQTLs
  parameters$V2[parameters$V1 == "NbKeepQTL"] <- paste(NbKeepQTL, collapse = ",") ## because read it from the command line, else remove it
  # NbKeepQTL <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "NbKeepQTL"], split = ",")))
  
  # NbAll <- unlist(lapply(paste("NbAlleles", 1:sum(NbQTLs), sep = ""), function(x) as.numeric(parameters$V2[parameters$V1 == x]))) ## if the number of alleles is defined for each QTL (1)
  NbAll <- as.numeric(parameters$V2[parameters$V1 == "NbAlleles"]) ## if the number of alleles is the same for all QTLs (2)
  NbAlleles <- vector("list", NbChromosome)
  ixAll <- 1
  for(iChr in 1:NbChromosome) {
    # if(NbQTLs[iChr] > 0) {NbAlleles[[iChr]] <- NbAll[ixAll:(ixAll + NbQTLs[iChr] - 1)]; ixAll <- ixAll + NbQTLs[iChr]} ## (1)
    if(NbQTLs[iChr] > 0) {NbAlleles[[iChr]] <- rep(NbAll, NbQTLs[iChr]); ixAll <- ixAll + NbQTLs[iChr]} ## (2)
  }
  
  positionQTLs <- NULL
  positionSNPs <- NULL
  
  nameDominance <- "degreeOfDominance.txt"
  degreeOfDominance <- matrix(NA, ncol = max(NbQTLs), nrow = NbChromosome)
  for(iChr in 1:NbChromosome) {
    if (dominance == "No") degreeOfDominance[iChr,] <- rep(0.5, max(NbQTLs))
    if (dominance == "Max") degreeOfDominance[iChr,] <- rep(1, max(NbQTLs))
    if (dominance == "Min") degreeOfDominance[iChr,] <- rep(0, max(NbQTLs))
    if (dominance == "Random") degreeOfDominance[iChr,] <- sample(0:10, max(NbQTLs), replace = TRUE)/10 
  }
  write.table(degreeOfDominance, file = nameDominance, sep = "\t", append = FALSE)
  print(c(name, reuse, N, NbChromosome, geneticLength, dominance))
  
  
  nameQTLs <- "effectQTLs.csv"
  
  nbRepeat <- 1
  repet <- 1
  
  nameGParameters <- file.path(mainDir, nameDirParameters, "GenerationParameters.txt") ## name of the files which will contain the number of individuals for each generation
  gparameters <- read.table(nameGParameters, header = TRUE, sep = "\t")
  nameGParameters <- "GenerationParameters.txt"
  write.table(gparameters, file = nameGParameters, col.names = colnames(gparameters), row.names = FALSE, sep = "\t", append = FALSE)
  
  
#  if(modeRecombination == "interferenceGamma") {
#    nuGamma <- c(1.887,1.479,2.392,1.946,2.263,3.084,1.64,2.036,2.418,2.362)
#    parameters <- rbind(parameters, c("nuGamma", paste(nuGamma, collapse = ",")))
#  }
#  if(modeRecombination != "interferenceGamma") {
#    nuGamma <- rep(1, NbChromosome)
#    parameters <- rbind(parameters, c("nuGamma", paste(nuGamma, collapse = ",")))
#  }

	nuGamma <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "nuGamma"], split = ",")))
	pGamma <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "p"], split = ",")))

  
  nameParameters <- "parameters.csv" ## name of the file which contains the diffrent parameters
  write.table(parameters, file = nameParameters, sep = ",", row.names = FALSE, col.names = FALSE)

  
  for(nbRepeat in repStart:NumberOfRepeats) {
    namePositionQTLs <- paste("positionQTLs", nbRepeat, ".csv", sep = "")
    # namePositionSNPs <- "~/Documents/Motamayor Cacao/Supp_Table_S4_StalinA_map.csv"
    namePositionSNPs <- paste("positionSNPs", nbRepeat, ".csv", sep = "")

    Chr <- 1
    if(reuse) {
      ## if modeCreationPosition = "gaussian" for the firsts generations, use "existGaussian"
      modeCreationPosition <- "existGaussian"
      mainSelectReuseGeneration(repet = nbRepeat, name = name, namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs,
                                modeCreationGenome = modeCreationGenome, modeCreationPosition = modeCreationPosition, modeRecombination = modeRecombination,
                                Chr = Chr, degreeOfDominance = degreeOfDominance, positionQTLs = positionQTLs) 
    } else {
      #     if(nbRepeat == 1) {
      #       seed <- sample(1:2^15,1)
      #     }
      
      if(typeSimu == "normal") {
        try(expr = mainSelectionSimuData(name = name, namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs, nameQTLs = nameQTLs,
                                         N = N, NbChromosome = NbChromosome, VG = VG, h2 = h2, homozygous = homozygous, coupling = coupling,
                                         Lcoupling = Lcoupling, geneticLength = geneticLength, NbQTLs = NbQTLs, NbSNPs = NbSNPs, NbAlleles = NbAlleles,
                                         shapeParameterPositionQTLs = shapeParameterPositionQTLs, shapeParameterEffectQTLs = shapeParameterEffectQTLs,
                                         modeCreationGenome = modeCreationGenome, modeCreationPosition = modeCreationPosition, modeRecombination = modeRecombination,
                                         Chr = Chr, degreeOfDominance = degreeOfDominance, positionQTLs = positionQTLs, gparameters = gparameters, seed = seed, repet = nbRepeat,
                                         basePopulation = selectionGeneration1, gTrainingPop = gTrainingPop, NbKeepQTL = NbKeepQTL, nuGamma = nuGamma, pGamma = pGamma))
      }
      
      if(typeSimu == "secobra") {
        mainSecobra(name = name, namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs,
                    N = N, NbChromosome = NbChromosome, VG = VG, h2 = h2, homozygous = homozygous, coupling = coupling,
                    Lcoupling = Lcoupling, geneticLength = geneticLength, NbQTLs = NbQTLs, NbSNPs = NbSNPs, NbKeepQTL = NbKeepQTL, NbAlleles = NbAlleles,
                    shapeParameterPositionQTLs = shapeParameterPositionQTLs, shapeParameterEffectQTLs = shapeParameterEffectQTLs,
                    modeCreationGenome = modeCreationGenome, modeCreationPosition = modeCreationPosition, modeRecombination = modeRecombination,
                    Chr = Chr, degreeOfDominance = degreeOfDominance, positionQTLs = positionQTLs, gparameters = gparameters, seed = seed, repet = nbRepeat,
                    selectionGeneration1 = selectionGeneration1, nuGamma = nuGamma, pGamma = pGamma)
      }
      
      if(typeSimu == "backcross") {

				namePositionQTLs <- "positionQTLs.csv" ## same positions for all replicates
				namePositionSNPs <- "positionSNPs.csv"

        iqtl <- 1 ## index of the chromosome containing the qtl
        dgb <- 1 ## marker density for the genetic background
        ## density : nbr of mrkr kept over total nbr of mrkr
        ## if d = 1, keep all of the mrkrs for the MAS
        ## if d = 0.1, keep 10% of the mrkrs
        dmrk1 <- 5 ## distance between the QTL and the left marker (in cM)
        dmrk2 <- 5 ## distance between the QTL and the right marker (in cM)
				SIDE <- 0 ## global variable, used for MASQTLFlanking (selection on the co closest to the target locus
        if(nbRepeat == repStart) write.table(data.frame(V1 = c("dgb", "dmrk1", "dmrk2"), V2 = as.character(c(dgb,dmrk1,dmrk2))), file = nameParameters, append = TRUE, col.names = FALSE, row.names = FALSE)
        print(paste("iqtl = ", iqtl, ", dgb = ", dgb, ", dmrk1 = ", dmrk1, ", dmrk2 = ", dmrk2, sep = ""))
        mainBC(name = name, namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs,
               N = N, NbChromosome = NbChromosome, VG = VG, h2 = h2, homozygous = homozygous, coupling = coupling,
               Lcoupling = Lcoupling, geneticLength = geneticLength, NbQTLs = NbQTLs, NbSNPs = NbSNPs, NbAlleles = NbAlleles,
               shapeParameterPositionQTLs = shapeParameterPositionQTLs, shapeParameterEffectQTLs = shapeParameterEffectQTLs,
               modeCreationGenome = modeCreationGenome, modeCreationPosition = modeCreationPosition, modeRecombination = modeRecombination,
               iqtl = iqtl, degreeOfDominance = degreeOfDominance, positionQTLs = positionQTLs, gparameters = gparameters, seed = seed, repet = nbRepeat,
               dgb = dgb, dmrk1 = dmrk1, dmrk2 = dmrk2, selectionGeneration1 = selectionGeneration1, nuGamma = nuGamma, pGamma = pGamma, gSelfing = gTrainingPop)
      }
      
    }
  }
  
  time <- proc.time() - time
  print(time)
  #Rprof(NULL)
  
  
  
  
  
  
  
  
  
  
