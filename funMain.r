## function used in the main
library(rrBLUP)

## simulation of a genetic map with the position and the values of the QTLs
## diploid infividuals
## input file : row -> one chromosome of one individual , column -> QTL value of the chromosome considered 
##                     eg of one row: ind1Chr1QTL1Homologous1 ind1Chr1QTL1Hom2 ind1Chr1QTL2Hom1 ind1Chr1QTL2Hom2 ...
##              -> nbr of row = N*NbChromosome

## reproductionType : HD = doubled haploidy
##                    SP = selfing (self pollination)
##                    SC = panmixie (standard crosses)
## selectionType : RS = random selection
##                 PS = phenotypic selection


# alleles <- unlist(generation)
# frequency <- as.data.frame(table(alleles)/length(alleles)) ## the frequency for each alleles is frequency$Freq
# geneBis <- array(unlist(generation), c(2, Mmax, N)) ## allele, locus, individual
# frequencyBis <- as.data.frame(table(geneBis)/length(geneBis))

mainSelectionSimuData <- function(name, namePositionSNPs, namePositionQTLs, nameQTLs, N, NbChromosome, VG, h2, homozygous, coupling, Lcoupling, geneticLength, NbQTLs, NbSNPs, 
                                  NbAlleles, shapeParameterPositionQTLs, shapeParameterEffectQTLs, modeCreationGenome, modeCreationPosition, modeRecombination,
                                  Chr, degreeOfDominance, positionQTLs, gparameters, seed, repet, basePopulation, gTrainingPop, NbKeepQTL, nuGamma, pGamma)
{
  
  ## function which perform a certain number of selection cycle
  ## arguments : name = character string : basis of the name
  ##             namePositionSNPs = character string : name of the file containing the position of the SNPs
  ##             namePositionQTLs = character string : name of the file containing the position of the QTLs
  ##             nameQTLs = character string : name of the file containing the effect of the QTLs
  ##             N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosome
  ##             VG  = double : genetic variance, for the creation of the gaussian values
  ##             h2 = numeric : haritability, to calculate the environmental variance
  ##             increaseLG -> no more as there is a different factor at each generation : is contained in the variable gparameters (parameters which change at each generation)
  ##             homozygous
  ##             coupling
  ##             Lcoupling
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             NbQTLs = vector : contains the number of QTL for each chromosome
  ##             NbSNPs = vector : contains the number of marker for each chromosome
  ##             NbAlleles = integer : nombre of alleles different for each marker
  ##             shapeParameterPositionQTLs
  ##             shapeParameterEffectQTLs
  ##             modeCreationGenome = character string : "gaussian" or "exist"
  ##             modeCreationPosition = character string : "gaussian", "existGaussian" or "exist"
  ##             modeRecombination = string of character : "Haldane", "completeInterference", "freeRecombination"
  ##             Chr = integer : number of the chromosome of interest
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs))
  ##             positionQTLs = list (NbChromosome element) of matrix (2 x NbQTLs) : contains the position of the QTLs for each chromosome; the second line contains ones (code if the locus we are looking at is a SNP(0) or a QTL(1))
  ##             gparameters
  ##             seed
  ##             repet
  ##             basePopulation = integer : generation where the selection will begin (1st segregating population)
	##             gTrainingPop
	##						 NbKeepQTL
  ## return nothing
  ## write the last generation in a file
  
  ############################################ CREATION/READING OF THE FIRST POPULATION ############################################
  #   nameGParameters <- file.path(mainDir, "GenerationParameters.txt") ## name of the files which will contain the number of individuals for each generation
  #   gparameters <- read.table(nameGParameters, header = TRUE, sep = "\t")
  
  seed <- seed + repet
  set.seed(seed)
  print(seed) ## print the seed in the .out file if need to reproduce the same exact simulation (use the same seed)
  
  gmax <- length(gparameters$g)
  
  nameExtended <- paste(name, repet, sep = "")
  nameSignalPlot <- paste(name, "signal", sep = "") ## name of the file which will contain the signal along the chromosome
  nameCor <- paste("Correlation/", nameExtended, "Cor", 1:NbChromosome, sep = "") ## name of the files which will contain the correlation matrix
  nameCorBeforeRecomb <- paste("Correlation/", nameExtended, "CorAvantRecomb", 1:NbChromosome, sep = "") ## name of the files which will contain the correlation matrix
  nameCorMean <- paste(name, "CorMean.txt", sep = "") ## name of the file which will contain the means of the correlations matrix
  # if (repet == "1") write.table(list("repet", "generation", "Chromosome", "mean of the correlation matrix", "mean of the nearest neighbour"), file = nameCorMean, append = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
  nameProba <- paste(nameExtended, "NbrOfDifferentAlleles.txt", sep = "")
  nameIBD <- paste(nameExtended, "ProbaIBD.txt", sep = "")
  
  namePEvolution <- paste(name, "Gain.txt", sep = "")
  if (repet == "1") write.table(list("repet", "g", "P", "varP", "G", "varG", "GNoDominance", "Pmax", "Gmax"), file = namePEvolution, append = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
  nameParameters <- paste(name, "Parameters.txt", sep="") ## name of the file which contains the diffrent parameters
  nameGenome <- readName(name = nameExtended, extension = "FinalPopulation", modeCreation = modeCreationGenome) ## name of the file containing the QTL values for the initial generation
  # nameGenome <- paste("/home/etourrette/Documents/20170907ARABIDO/Ns4QTL10DominanceNo/Ns4QTL10DominanceNo", repet, "InitialPopulation.txt", sep = "")
  namePosition <- readName(name = name, extension = "pos", modeCreation = modeCreationPosition) ## name of the file containing the QTL positions for the initial generation
  nameLocus <- paste(name, "NbLocus.txt", sep="") ## name of the file containing the number of QTL for each chromosome of each individuals for the initial generation
  nameLD <- paste("LD/", nameExtended, "LDChr", 1:NbChromosome, sep = "")
  nameAllelesDistrib <- paste(nameExtended, "AllelesDistribution.txt", sep = "")
  nameOriginAlleles <- paste(nameExtended, "OriginAlleles", sep = "")
  nameEffect <- paste(nameExtended, "Effect", sep = "") ## for all of the repet, always have the same effect for the QTLs and alleles?
  namePercVar <- paste(name, "PercentageVariance.txt", sep = "")
  # if(repet == "1") write.table(t(c("rep", "g", "QTLeff", paste(1:sum(NbQTLs)))), file = namePercVar, col.names = FALSE, row.names = FALSE, append = FALSE, sep = "\t")
  # if(repet == "1") write.table(t(c("rep", "g", "QTLeff", paste(1:sum(NbQTLs)))), file = "percVarr2.txt", col.names = FALSE, row.names = FALSE, append = FALSE, sep = "\t")
  if(h2 != 1) {
    namePhenotypeE <- paste(name, "PE.txt", sep = "")
    nameVarE <- paste(name, "VE.txt", sep = "")
  }
	nameGEBV <- paste(name, "GEBV.txt", sep = "") ## genomic estimated breeding values when using BLUP (genomic selection)
  
  
  if (repet != "1" & repet != "" & (modeCreationPosition == "gaussian" | modeCreationPosition == "existGaussian")) modeCreationPosition <- "existGaussian"
  geneticLength0 <- geneticLength ## geneticLength0 is the initial genetic length
  position0 <- creationPositionBis(namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs, NbChromosome = NbChromosome, shapeParameterQTLs = shapeParameterPositionQTLs,
                                   NbSNPs = NbSNPs, NbQTLs = NbQTLs, geneticLength = geneticLength0) ## creation of the QTL positions (or reading of the file containg the QTL positions) for the initial generation
  orderPosQTLs <- position0$orderPos ## order of the position of QTLs as given in the file
  position0 <- position0$position
  # position <- lapply(position0, function(x) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE)) NO MORE GLOBAL INCREASE : HAVE TO DO IT AT EACH GENERATION
  # positionSNPs <- lapply(position0, function(x) x[1,which(x[2,] == 0)]) ## position of the SNPs
  # positionQTLs <- lapply(position0, function(x) x[1,which(x[2,] == 1)]) ## position of the QTLs
  
  #   generation <- creationValueBis(N = N, NbChromosome = NbChromosome, Ms = NbQTLs, VG = VG, homozygous = homozygous, coupling = coupling, Lcoupling = Lcoupling, modeCreationMap = modeCreationGenome, nameGenome = nameGenome, nameEffect = nameEffect, NbAlleles = NbAlleles,
  #                               shapeParameterQTLs = shapeParameterEffectQTLs, degreeOfDominance = degreeOfDominance, position = position, snps = positionSNPs) ## creation of the QTL values (or reading of the file containg the QTL values) for the initial generation
  
  generation <- creationValueTer(N = N, NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, VG = VG, homozygous = homozygous, coupling = coupling, Lcoupling = Lcoupling,
                                 nameEffect = nameEffect, nameQTLs = nameQTLs, NbAlleles = NbAlleles, shapeParameterQTLs = shapeParameterEffectQTLs, degreeOfDominance = degreeOfDominance,
                                 orderPos = orderPosQTLs, position = position0)
  
  NbMarkers <- NbSNPs + NbQTLs
  # geneticLength <- increaseLG*geneticLength
  
  #   parameters <- matrix(c(N, gmax, VG, h2, NbChromosome, shapeParameterPositionQTLs, shapeParameterEffectQTLs, geneticLength, NbQTLs, NbSNPs, unlist(NbAlleles)), ncol = 1, nrow = 7+3*NbChromosome+sum(NbQTLs))
  #   write.table(parameters, file = nameParameters, sep = "\t", row.names = c("N", "gmax", "VG", "h2", "NbChromosome", "shapeParameterPositionQTLs", "shapeParameterEffectQTLs", paste("geneticLength", 1:NbChromosome, sep = ""), paste("NbQTLs", 1:NbChromosome, sep = ""), paste("NbSNPs", 1:NbChromosome, sep = ""), paste("NbAlleles",1:sum(NbQTLs), sep = "")), col.names = "Values")
  
  # write.table(list(t(paste(rep(1:NbChromosome, NbQTLs), sep = ""))), file = nameProba, append = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
  
  if(SAVEG) save(generation, file = paste(nameExtended, "InitialPopulation.RData", sep = ""))
  
  Ninitial <- N 
  
  ############################################ SELECTION CYCLES ############################################
  
  ## generation : list of individuals
  ## individual : list of chromosome
  ## chromosome : matrix with two rows (diploid individuals) and nbr QTL columns
  g <- 0
  #   P <- Analysis(repet = repet, Ninitial = Ninitial, N = N, NbChromosome = NbChromosome, g = g, h2 = h2, selectionGeneration1 = basePopulation, NbQTLs = NbQTLs, NbMarkers = NbMarkers, nameCor = nameCor, nameCorMean = nameCorMean, nameProba = nameProba,
  #                 nameProba2 = nameIBD, namePEvolution = namePEvolution, nameLD = nameLD, nameAllelesDistrib = nameAllelesDistrib, nameOriginAlleles = nameOriginAlleles, namePercVar = namePercVar, nameEffect = nameEffect, position = position, generation = generation) 
  
  VE <- 0
  if(g == basePopulation) {
    VE <- calculVE(N = N, NbChromosome = NbChromosome, h2 = h2, generation = generation) 
    if(h2 != 1) write.table(VE, file = nameVarE, append = TRUE, sep = "\t", col.names = FALSE, row.names = repet)
  }
  ## calculate the environmental variance for the first generation where the selection is applied
  ## keep the same after
  
  P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation) 
  if(h2 != 1) write.table(t(P), file = namePhenotypeE, append = TRUE, col.names = FALSE, row.names = paste(repet, "_", g, sep = ""), sep = "\t") ## save the phenotypic value when h2!=1 (if h2=1 calculate it in the pop)
  
	PPS <- P
  
  for (g in 1:gmax) { ## loop on the generation
    
    NbSelection <- gparameters$NSelected[which(gparameters$g == g)]
    selectionType <- as.character(gparameters$Selection[which(gparameters$g == g)])
    reproductionType <- as.character(gparameters$Reproduction[which(gparameters$g == g)])
    NbCross <- gparameters$NCrosses[which(gparameters$g == g)] 

		## if the selection is BLUP ie estimation of the markers effects via rrBLUP
		## can have multiple generations of marker effects estimation
    if(selectionType == "BLUP") {
      ## look at (basePopulation+1)
      ## as basePopulation as the population corresponding to g = basePopulation
      ## but do the estimation before doing the generation g (must add 1 to the generation)
      if(g >= (gTrainingPop[1]+1)) {
        ## in the case when g < gTrainingPop, use the measured P
        ## calculation of the genotype matrix
        ## for biallelic markers : -1 if AA (homozygote parent 1), 
        ##                          0 if AB (heterozygote) 
        ##                          1 if BB (homozygote parent 2)
        # Genomatrix <- lapply(generation, lapply, function(x) x[5:6,]) ## use neutral mrkrs and qtls
				Genomatrix <- lapply(generation, lapply, function(x) x[5:6,which(x[8,]==0)]) ## use neutral mrkrs only (snps), keep the origin of the markers to get the genotype
        Genomatrix <- lapply(Genomatrix, function(x) unlist(lapply(x, apply, 2, sum)) - 3)
        # Genomatrix <- matrix(unlist(Genomatrix), ncol = sum(NbMarkers), nrow = N, byrow = TRUE) ## use neutral mrkrs and qtls
				Genomatrix <- matrix(unlist(Genomatrix), ncol = sum(NbSNPs), nrow = N, byrow = TRUE) ## use neutral mrkrs only (snps)
        if(sum(g-1 == gTrainingPop) > 0) { ## if one element of gTrainingPop is equal to g-1 then one TRUE and sum > 1
          ## keep the phenotypic value of the F2 for rrBLUP (measured vie G+E)
					write.table(t(P), file = paste(name, "_PrrBLUPF2.txt", sep = ""), append = TRUE, col.names = FALSE, row.names = paste(repet, "_", g-1, sep = ""), sep = "\t") ## phenotype used to calculate the prediction model (using rrBLUP) -> save it if want to evaluate the model afterwards; use g-1 as it is P for the previous generation
          modelBLUP <- mixed.solve(y = P, Z = Genomatrix)
          ## save the model for each rep? -> should be able to obtain it via the base pop and the vector of P
        }
        P <- c(modelBLUP$beta) + Genomatrix %*% modelBLUP$u ## do not use the error when estimate the value
        # write.table(t(c(name, repet, g, cor(P,PPS))), file = paste(name, "accuracy.txt", sep = ""), append = TRUE, row.names = FALSE, col.names = FALSE) 
				write.table(t(P), file = nameGEBV, append = TRUE, col.names = FALSE, row.names = paste(repet, "_", g-1, sep = ""), sep = "\t") ## save the GEBV (genomic estimated breeding value), especially if want to calculate the accuracy of prediction (= cor(GEBV,G)); use g-1 as calculate P of the previous new population (previous pop of offsprings now parents)
      }
    } 

		## do not do MAS in the end
		## if only keep a subset of the QTL for the selection (for the value of P and G saved, keep all of the QTL
		if((selectionType != "BLUP") & (sum(NbKeepQTL)<sum(NbQTLs))) { 
			## if we use less QTL than there is in the genome, have a subpopulation to calculate P with only the used (strongest QTL)
			## no problem with BLUP as do not use the QTL to estimate the breeding value
			
			## step 1 : choose which QTL to keep (look at the max value of each QTL in the pop and keep the strongest ones
			## step 2 : create a subpopulation with only the QTL kept
			## step 3 :calculate P (for the selection) with this subpop
			popMatrix <- lapply(generation, lapply, function(x) apply(x[1:2,],2,sum)) ## keep only the allelic value both of QTL and SNP, necessary to have the correct index values (sum over both alleles on one ind as what we detect is the QTL value of one individual and not the different allelic values)
			subpop <- generation
 			for(iChr in 1:NbChromosome) {
				maxValueQTL <- apply(sapply(popMatrix, function(x) x[[iChr]]), 1, max) ## keep the QTL depending on their maximum value in the pop -> use the mean value?
				ixKeepQTL <- order(maxValueQTL, decreasing = TRUE)[1:NbKeepQTL[iChr]]
				factorQTL <- rep(0,NbMarkers[iChr]) ## = 1 if we keep the QTL, 0 otherwise
				factorQTL[ixKeepQTL] <- 1
				for(iN in 1:N) {
					subpop[[iN]][[iChr]][1:2,] <- t(apply(subpop[[iN]][[iChr]][1:2,],1,function(x) x*factorQTL))
				}
			}

			G <- calculGDominance(N = N, NbChromosome = NbChromosome, generation = subpop)
  		P <- G + rnorm(N, 0, sqrt(VE))
			## save the real PE used for the selection
			if(h2 != 1) write.table(t(P), file = paste(name, "PE_MAS.txt", sep = ""), append = TRUE, col.names = FALSE, row.names = paste(repet, "_", g, sep = ""), sep = "\t") ## save the phenotypic value when h2!=1 (if h2=1 calculate it in the pop)
		}

    generationSelected <- list()
    indexSelected <- selection(N = N, NbSelection = NbSelection, P = P, selectionType = selectionType)
    generationSelected <- generation[indexSelected]
    
    Pselect <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g - 0.5, namePEvolution = namePEvolution, repet = repet, generation = generationSelected)
    
    N <- gparameters$NOffspring[which(gparameters$g == g)]
    generation <- vector("list", N)
    
    ## new genetic lengthes and QTLs and SNPs positions to match with the modification of recombination
    increaseLG <- gparameters$Recombination[which(gparameters$g == g)]
    geneticLength <- increaseLG * geneticLength0
    position <- lapply(position0, function(x) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE))
    
    #print(NbSelection)
    
    ## without using the matrix of crosses
    #     ixParents <- indexIndividuals(Nselect = NbSelection, Noffspring = N, reproductionType = reproductionType)
    #     for (i in 1:N) {
    #       if (reproductionType == "SC") ixParentsI <- ixParents[,i]
    #       if (reproductionType == "HD" | reproductionType == "SP") ixParentsI <- ixParents[i]
    #       generation[[i]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
    #                                            reproductionType = reproductionType, position = position, generationSelected = generationSelected, degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
    #     }
    ##
    
    ## with the matrix of cross between parents
    crossMat <- crossMatrix(Nc = NbCross, Ns = NbSelection, reproductionType = reproductionType)
    NbOffspringCross <- N/NbCross
    ixParents <- indexParents(crossMat = crossMat, reproductionType = reproductionType)
    indexOffspring <- 1
    for(i in 1:NbCross) {
      for(j in 1:NbOffspringCross) {
        if (reproductionType == "SC") ixParentsI <- ixParents[,i]
        if (reproductionType == "HD" | reproductionType == "SP") ixParentsI <- ixParents[i]
        generation[[indexOffspring]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
                                                          reproductionType = reproductionType, position = position, generationSelected = generationSelected, degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
        indexOffspring <- indexOffspring + 1
      }
    }
    if(indexOffspring <= N) {
      for(i in 1:(N+1-indexOffspring)) {
        if (reproductionType == "SC") ixParentsI <- ixParents[,i]
        if (reproductionType == "HD" | reproductionType == "SP") ixParentsI <- ixParents[i]
        generation[[indexOffspring]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
                                                          reproductionType = reproductionType, position = position, generationSelected = generationSelected, degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
        indexOffspring <- indexOffspring + 1
      }
    }
    print(c(NbSelection, indexOffspring))
    ##
    
    if(g == basePopulation) {
      VE <- calculVE(N = N, NbChromosome = NbChromosome, h2 = h2, generation = generation) 
      if(h2 != 1) write.table(VE, file = nameVarE, append = TRUE, sep = "\t", col.names = FALSE, row.names = repet)
    }
    
    #     P <- Analysis(repet = repet, Ninitial = Ninitial, N = N, NbChromosome = NbChromosome, g = g, VE = VE, NbQTLs = NbQTLs, NbMarkers = NbMarkers, nameCor = nameCor, nameCorMean = nameCorMean, nameProba = nameProba,
    #                   nameProba2 = nameIBD, namePEvolution = namePEvolution, nameLD = nameLD, nameAllelesDistrib = nameAllelesDistrib, nameOriginAlleles = nameOriginAlleles, namePercVar = namePercVar, nameEffect = nameEffect, position = position, generation = generation) 
    
    P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation) ## phenotypic value calculated using the genomic information
    if(h2 != 1) write.table(t(P), file = namePhenotypeE, append = TRUE, col.names = FALSE, row.names = paste(repet, "_", g, sep = ""), sep = "\t")
    
    if(SAVEG) save(generation, file = paste(nameExtended, "_", g, ".RData", sep = ""))

    PPS <- P
    
  }
  
  
  #   gmat <- matrix(unlist(generation), ncol = 8*sum(NbMarkers), byrow = TRUE) ## save the last generation in case we want to do more generations
  #   write.table(gmat, file = nameGenome, sep = "\t", append = FALSE)
  
  # save(generation, file = paste(nameExtended, "FinalPopulation.RData", sep = ""))
  
  ## to read the last generation from a file and reuse it
  #   gmat <- read.table(file = nameGenome, sep = "\t")
  #   generation2 <- vector("list", N)
  #   for (i in 1:N) {
  #     generation2[[i]] <- vector("list", NbChromosome)
  #     indexChr <- 0
  #     for (j in 1:NbChromosome) {
  #       generation2[[i]][[j]] <- matrix(as.numeric(gmat[i, (indexChr+1):(indexChr+NbMarkers[j]*8)]), nrow = 8)
  #       indexChr <- indexChr + NbMarkers[j] * 8
  #     }
  #   }
}


mainSelectReuseGeneration <- function(repet, name, namePositionSNPs, namePositionQTLs, modeCreationGenome, modeCreationPosition, modeRecombination, Chr, degreeOfDominance, positionQTLs, nuGamma, pGamma)
{
  
  ## function which perform a certain number of selection cycle
  ## arguments : repet = integer : number of the rep
  ##             name = character string : basis of the name
  ##             modeCreationGenome = character string : "gaussian" or "exist" 
  ##             modeCreationPosition = character string : "gaussian" or "exist"
  ##             modeRecombination = string of character : "Haldane", "completeInterference", "freeRecombination"
  ##             Chr = integer : number of the chromosome of interest
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs))
  ## return nothing
  ## write the last generation in a file
  
  ############################################ CREATION/READING OF THE FIRST POPULATION ############################################
  nameGParameters <- paste(name, "GenerationParameters.txt", sep="") ## name of the files which will contain the number of individuals for each generation
  gparameters <- read.table(nameGParameters, header = TRUE, sep = "\t")
  nameParameters <- paste(name, "Parameters.txt", sep="") ## name of the file which contains the different parameters
  parameters <- read.table(file = nameParameters, header = TRUE, sep = "\t", row.names = 1)
  
  Ninitial <- parameters[1,]
  gStop <- parameters[2,] ## gmax of the previous simulation
  VG <- parameters[3,]
  VE <- parameters[4,]
  NbChromosome <- parameters[5,]
  shapeParameterPositionQTLs <- parameters[6,]
  geneticLength0 <- parameters[8:(7+NbChromosome),]
  NbQTLs <- parameters[(8+NbChromosome):(7+2*NbChromosome),]
  NbSNPs <- parameters[(8+2*NbChromosome):(7+3*NbChromosome),]
  NbAlleles <- parameters[(8+3*NbChromosome):(7+3*NbChromosome+sum(NbQTLs)),]
  gmax <- length(gparameters$g)
  N <- gparameters$NOffspring[which(gparameters$g == gStop)]
  NbMarkers <- NbQTLs + NbSNPs
  
  nameExtended <- paste(name, repet, sep = "")
  nameSignalPlot <- paste(name, "signal", sep = "") ## name of the file which will contain the signal along the chromosome
  nameCor <- paste(nameExtended, "Cor", 1:NbChromosome, sep = "") ## name of the files which will contain the correlation matrix
  nameCorBeforeRecomb <- paste(nameExtended, "CorAvantRecomb", 1:NbChromosome, sep = "") ## name of the files which will contain the correlation matrix
  nameCorMean <- paste(name, "CorMean.txt", sep = "") ## name of the file which will contain the means of the correlations matrix
  nameProba <- paste(nameExtended, "NbrOfDifferentAlleles.txt", sep = "")
  nameIBD <- paste(nameExtended, "ProbaIBD.txt", sep = "")
  namePEvolution <- paste(name, "Gain.txt", sep = "")
  nameGenome <- readName(name = nameExtended, extension = "v", modeCreation = modeCreationGenome) ## name of the file containing the QTL values for the initial generation
  namePosition <- readName(name = name, extension = "p", modeCreation = modeCreationPosition) ## name of the file containing the QTL positions for the initial generation
  nameLocus <- paste(name, "NbLocus.txt", sep="") ## name of the file containing the number of QTL for each chromosome of each individuals for the initial generation
  nameLD <- paste(nameExtended, "LDChr", 1:NbChromosome, sep = "")
  nameAllelesDistrib <- paste(nameExtended, "AllelesDistribution.txt", sep = "")
  nameOriginAlleles <- paste(nameExtended, "OriginAlleles", sep = "")
  
  ## to reuse the generation written (for multiple chromosomes):
  #   gmat <- read.table(file = nameGenome, sep = "\t")
  #   generation <- vector("list", N)
  #   for (i in 1:N) {
  #     generation[[i]] <- vector("list", NbChromosome)
  #     indexChr <- 0
  #     for (j in 1:NbChromosome) {
  #       generation[[i]][[j]] <- matrix(as.numeric(gmat[i, (indexChr+1):(indexChr+NbMarkers[j]*8)]), nrow = 8)
  #       indexChr <- indexChr + NbMarkers[j] * 8
  #     }
  #   }
  
  load(nameGenome)
  
  increaseLG <- 1
  
  position0 <- creationPosition(modeCreationPosition = modeCreationPosition, namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs, NbChromosome = NbChromosome, increaseLG = increaseLG,
                                shapeParameterQTLs = shapeParameterPositionQTLs, NbSNPs = NbSNPs, NbQTLs = NbQTLs, geneticLength = geneticLength0, positionQTLs = positionQTLs) ## creation of the QTL positions (or reading of the file containg the QTL positions) for the initial generation
  
  nameParameters <- paste(name, "Parameters2ndSimulation.txt", sep="") ## name of the file which contains the diffrent parameters
  parameters <- matrix(c(N, gmax, VG, h2, NbChromosome, geneticLength0, NbQTLs, NbSNPs, unlist(NbAlleles)), ncol = 1, nrow = 5+3*NbChromosome+sum(NbQTLs))
  write.table(parameters, file = nameParameters, sep = "\t", row.names = c("N", "gmax", "VG", "h2", "NbChromosome", paste("geneticLength", 1:NbChromosome, sep = ""), paste("NbQTLs", 1:NbChromosome, sep = ""), paste("NbSNPs", 1:NbChromosome, sep = ""), paste("NbAlleles",1:sum(NbQTLs), sep = "")), col.names = "Values")
  
  
  ############################################ SELECTION CYCLES ############################################
  
  P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = gStop, namePEvolution = namePEvolution, repet = repet, generation = generation) 
  
  for (g in (gStop+1):gmax) { ## loop on the generation
    
    NbSelection <- gparameters$NSelected[which(gparameters$g == g)]
    selectionType <- as.character(gparameters$Selection[which(gparameters$g == g)])
    reproductionType <- as.character(gparameters$Reproduction[which(gparameters$g == g)])
    
    generationSelected <- list()
    indexSelected <- selection(N = N, NbSelection = NbSelection, P = P, selectionType = selectionType)
    generationSelected <- generation[indexSelected]
    
    ## new genetic lengthes and QTLs and SNPs positions to match with the modification of recombination
    increaseLG <- gparameters$Recombination[which(gparameters$g == g)]
    geneticLength <- increaseLG * geneticLength0
    position <- lapply(position0, function(x) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE))
    
    N <- gparameters$NOffspring[which(gparameters$g == g)]
    generation <- vector("list", N)
    ## without using the matrix of crosses
    #     ixParents <- indexIndividuals(NbSelection, N, reproductionType)
    #     for (i in 1:N) {
    #       if (reproductionType == "SC") ixParentsI <- ixParents[,i]
    #       if (reproductionType == "HD" | reproductionType == "SP") ixParentsI <- ixParents[i]
    #       generation[[i]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
    #                                            reproductionType = reproductionType, position = position, generationSelected = generationSelected, degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
    #     }
    
    ## with the matrix of cross between parents
    crossMat <- crossMatrix(Nc = NbCross, Ns = NbSelected, reproductionType = reproductionType)
    NbOffspringCross <- ceiling(N/NbCross)
    ixParents <- indexParents(crossMat = crossMat, reproductionType = reproductionType)
    indexOffspring <- 1
    for(i in 1:NbCross) {
      for(j in 1:NbOffspringCross) {
        if (reproductionType == "SC") ixParentsI <- ixParents[,i]
        if (reproductionType == "HD" | reproductionType == "SP") ixParentsI <- ixParents[i]
        generation[[indexOffspring]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
                                                          reproductionType = reproductionType, position = position, generationSelected = generationSelected, degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
        indexOffspring <- indexOffspring + 1
      }
    }
    
    # P <- Analysis(repet = repet, Ninitial = Ninitial, N = N, NbChromosome, g = g, VE = VE, NbQTLs = NbQTLs, NbMarkers = NbMarkers, nameCor = nameCor, nameCorMean = nameCorMean, nameProba = nameProba,
    #               nameProba2 = nameIBD, namePEvolution = namePEvolution, nameLD = nameLD, nameAllelesDistrib = nameAllelesDistrib, nameOriginAlleles = nameOriginAlleles, namePercVar = namePercVar, nameEffect = nameEffect, position = position, generation = generation) 
    
    P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation)
    
    if(SAVEG) save(generation, file = paste(nameExtended, "_", g, ".RData", sep = ""))
  }
  
  
  # gmat <- matrix(unlist(generation), ncol = 8*sum(NbMarkers), byrow = TRUE) ## save the last generation in case we want to do more generations
  # write.table(gmat, file = nameGenome, sep = "\t", append = FALSE)
  
}

