## BACKCROSS simulation


creationValueParentsBC <- function(N, NbChromosome, Ms, VG, homozygous, coupling, Lcoupling, modeCreationMap, nameGenome,
                             nameEffect, NbAlleles, shapeParameterQTLs, degreeOfDominance, position, snps) 
{
  
  ## function to create a list containing the values of the QTL for each chromosomes of each individuals of the population
  ## either create it or read it from a file
  ## arguments : N  = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             Ms = vector : number of QTL for each chromosome
  ##             VG  = double : genetic variance, for the creation of the gaussian values
  ##             homozygous
  ##             coupling
  ##             Lcoupling
  ##             modeCreationMap = character string : method to create (read if already exist) the genetic map, "gaussian" or "exist"
  ##             nameGenome = character string : name of the file where the map will be written or name of the file being read to extract the map
  ##             nameEffect
  ##             NbAlleles = integer : nombre of alleles different for each marker
  ##             shapeParameterQTLs
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs))
  ##             position = list of vector : position of the SNPs and QTLs for each chromosome
  ##             snps = list of matrix/vector : position of the SNPs for each chromosome
  ## return a list of list of matrix (N list of NbChromosome matrix of size 8xNbQTL), each matrix contains the effect, the index, the type (QTL/SNP), the origin (+dominance) of the alleles for each markers
  ## for each chromosomes of each individuals
  ## parent 1 = donor parent
  ## parent 2 = recurrent parent
  
  
  generation <- vector("list", N)
  
  if (modeCreationMap == "gaussian") {
    
    nameEffectAllele <- paste(nameEffect, "Allele.txt", sep = "")
    nameEffectQTL <- paste(nameEffect, "QTL.txt", sep = "")
    
    ## gamma distribution of the QTL effects
    AllelesList <- vector("list", NbChromosome)
    for(iChr in 1:NbChromosome) {
      NbQTL <- Ms[iChr]
      QTLeffect <- rgamma(NbQTL, shape = shapeParameterQTLs, scale = sqrt(VG/shapeParameterQTLs)) ## scale = sqrt(VG/shapeParameterQTLs) (def of the var for gamma distribution)
      AllelesList[[iChr]] <- mapply(FUN = function(x,y) runif(x, min = 0, max = y), NbAlleles[[iChr]], QTLeffect, SIMPLIFY = FALSE)
      write.table(t(c(paste("chromosome",iChr, sep = ""), QTLeffect)), file = nameEffectQTL, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
      lapply(AllelesList[[iChr]], function(x) write.table(t(c(paste("chromosome",iChr, sep = ""),unlist(x))), file = nameEffectAllele, append = "TRUE", sep = "\t", row.names = FALSE, col.names = FALSE))
    }
    ##
    
    for (i in 1:N) {
      genome <- vector("list", NbChromosome)
      for (iChr in 1:NbChromosome) {
        NbMrkrs <- length(position[[iChr]][1,])
        genome[[iChr]] <- matrix(NA, nrow = 8, ncol = NbMrkrs)
        QTLsIx <- which(position[[iChr]][2,] == 1)
        SNPsIx <- which(position[[iChr]][2,] == 0)
        if(!homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) sample(x, 2, replace = TRUE)) ## if want only heterozygous individuals: replace = FALSE; 
        ## if want the possibility of having homozygous QTL: replace = TRUE
        if(homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) rep(sample(x, 1),2))
        genome[[iChr]][1:2,QTLsIx] <- unlist(mapply(function(x,y) x[y], AllelesList[[iChr]], AllelesIx)) 
        genome[[iChr]][1:2,SNPsIx] <- rep(0, 2*length(snps[[iChr]])) 
        genome[[iChr]][3:4,QTLsIx] <- unlist(AllelesIx)
        genome[[iChr]][3:4,SNPsIx] <- replicate(length(snps[[iChr]]), sample(c(0,1), 2, replace = FALSE)) ## SNPs' allele; suppose that the SNPs are biallelic
        genome[[iChr]][5:6,] <- rep(i, 2*NbMrkrs) 
        genome[[iChr]][7,QTLsIx] <- degreeOfDominance[iChr,1:Ms[iChr]] 
        genome[[iChr]][7,SNPsIx] <- rep(0.5, length(snps[[iChr]]))
        genome[[iChr]][8,] <- position[[iChr]][2,] 

        if(i == 1) QTLsign <- 1 ## donor parent
        if(i == 2) QTLsign <- 0 ## recurrent parent (the QTL have no effect)
        genome[[iChr]][1:2,QTLsIx] <- QTLsign * genome[[iChr]][1:2,QTLsIx]

      }
      generation[[i]] <- genome
    }
    return(generation)
  } else  if (modeCreationMap == "exist" & file.exists(nameGenome)) {
    NbMrkrs <- mapply(length, position)/2
    generation <- readValueQTL(nameGenome = nameGenome, N = N, NbChromosome = NbChromosome, NbMarkers = NbMrkrs)
    return(generation)
  } else {
    cat("The option chosen is not valid. Make a new choice.")
    modeCreationMap <- readMode()
    creationValue(N = N, NbChromosome = NbChromosome, Ms = Ms, VG = VG, modeCreationMap = modeCreationMap, nameGenome = nameGenome, nameEffect = nameEffect,
                  NbAlleles = NbAlleles, shapeParameterQTLs = shapeParameterQTLs, degreeOfDominance = degreeOfDominance, position = position, snps = snps)
  }
}

## difference with creationPosition :
## do not need the parameter modeCreationPosition
## it can take QTL ans SNP both from a file and simulated at the same time
## backcross : uniform distribution of the qtls and the snps
creationPositionBC <- function(namePositionSNPs, namePositionQTLs, NbChromosome, shapeParameterQTLs, NbSNPs, NbQTLs, geneticLength)
{
  ## function to create or read the position of the SNPs and QTLs
  ## arguments : namePositionSNPs = character string : name of the file containing the position of the SNPs
  ##             namePositionQTLs = character string : name of the file containing the position of the QTLs
  ##             NbChromosome = integer : number of chromosome
  ##             shapeParameterQTLs
  ##             NbSNPs = vector : number of SNPs for each chromosome
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ## returns a list of vector containing the position of the SNPs and of the QTLs for all chromosomes
  
  ## if the files containing the position of the QTLs and SNPs do not already exist
  ## there is no QTL/SNP already defined
  if(!file.exists(namePositionQTLs)) {NbQTLsFile <- rep(0, NbChromosome); posQTLsFile <- NULL}
  if(!file.exists(namePositionSNPs)) {NbSNPsFile <- rep(0, NbChromosome); posSNPsFile <- NULL}
  
  ## if the files exist
  ## save the position and count the number of QTLs/SNPs in it
  ## format of the file : each line represents a chromosome -> number of markers = length of the line
  if(file.exists(namePositionQTLs)) {
    posQTLsFile <- read.table(namePositionQTLs, header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE, col.names = 1:max(NbQTLs)) ## format of posQTL = list of NbChromosome elements
    ## blank.lines.skip = F because if there is no QTL then there will be a blank line -> want to take it into account
		## col.names = 1:max(NbQTLs) as it specifies the number of column using the max of the first 5 lines: if more lines and the max is after the first five ones, will have a problem, thus the specification of the col. names
    posQTLsFile <- apply(posQTLsFile, 1, list)
    posQTLsFile <- lapply(posQTLsFile, unlist)
    posQTLsFile <- lapply(posQTLsFile, rbind, 1)
    posQTLsFile <- lapply(posQTLsFile, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
    NbQTLsFile <- sapply(posQTLsFile, function(x) length(na.omit(x[1,]))) ## need the na.omit because the NA stayed in the object
    file.remove(namePositionQTLs) ## in write.table (last lines), append = T, and all QTLs are written : avoid repetition
  }
  if(file.exists(namePositionSNPs)) {
    posSNPsFile <- read.table(namePositionSNPs, header = FALSE, stringsAsFactors = FALSE, blank.lines.skip = FALSE, sep = ",", col.names = 1:max(NbSNPs)) ## format of posSNP = list of NbChromosome elements
    posSNPsFile <- apply(posSNPsFile, 1, list)
    posSNPsFile <- lapply(posSNPsFile, unlist)
    posSNPsFile <- lapply(posSNPsFile, rbind, 0)
    posSNPsFile <- lapply(posSNPsFile, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
    NbSNPsFile <- sapply(posSNPsFile, function(x) length(na.omit(x[1,]))) ## need the na.omit because the NA stayed in the object
    file.remove(namePositionSNPs)
  }
  
  NbQTLsToDo <- NbQTLs - NbQTLsFile
  NbSNPsToDo <- NbSNPs - NbSNPsFile
  position <- vector("list", NbChromosome)
  NbMarkers <- NbSNPs + NbQTLs
  # shapeParameterQTLs <- 0.1 ## shape of the gamma distribution : affect the repartition of QTL (clusters, random positions, constant distances)
  shapeParameterSNPs <- 10 ## shape of the distribution (gamma) of the SNPs position : SNPs uniformly placed along the chromosomes
  print(paste("shape parameter QTL position", shapeParameterQTLs, sep = " "))
  
  for (iChr in 1:NbChromosome) {
    position[[iChr]] <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
    pos <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
    posSNPs <- matrix(NA, ncol = NbSNPsToDo[iChr], nrow = 2)
    posQTLs <- matrix(NA, ncol = NbQTLsToDo[iChr], nrow = 2)
    posSNPs[2,] <- rep(0, NbSNPsToDo[iChr])
    posQTLs[2,] <- rep(1, NbQTLsToDo[iChr])
    
#     ## the SNPs (neutral markers) are located regularly along the chromosome
#     ## hence the shape parameter of the gamma distribution > 1 (if SNPs placed randomly, = 1)
#     distSNPs <- round(rgamma(NbSNPsToDo[iChr], shape = shapeParameterSNPs, rate = (NbSNPsToDo[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
#     ## rate = (NbSNPs[iChr]*10)/geneticLength[iChr]), in order to have the right genetic length of the chromosome
#     while(sum(distSNPs) > geneticLength[iChr]) {
#       distSNPs <- round(rgamma(NbSNPsToDo[iChr], shape = shapeParameterSNPs, rate = (NbSNPsToDo[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
#     }
#     posSNPs[1,] <- cumsum(distSNPs)
    ## SNPs are uniformely distributed along the chromosome
    psnp <- 1:NbSNPs[iChr]
    posSNPs[1,] <- (psnp-0.5)*geneticLength[iChr]/NbSNPs[iChr]
    ##
    ## the QTLs follow an L-shaped distribution : clustering
#     distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLsToDo[iChr]), shape = shapeParameterQTLs, rate = NbQTLsToDo[iChr]*shapeParameterQTLs/geneticLength[iChr]))
#     distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
#     while(length(distQTLs) != NbQTLsToDo[iChr]) {
#       distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLsToDo[iChr]), shape = shapeParameterQTLs, rate = NbQTLsToDo[iChr]*shapeParameterQTLs/geneticLength[iChr]))
#       distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
#     }
    ##
    ## normal distribution of the QTLs position
    NbQTL <- NbQTLs[iChr]
    distQTLs <- 1:NbQTL
    distQTLs <- (distQTLs-0.5)*geneticLength[iChr]/NbQTL
    distQTLs <- unique(distQTLs)
    distQTLs <- distQTLs[order(distQTLs)]
    ##
    posQTLs[1,] <- round(distQTLs,3)
    pos <- cbind(posQTLs, posQTLsFile[[iChr]], posSNPs, posSNPsFile[[iChr]])
    pos <- pos[,order(pos[1,])]
    position[[iChr]] <- pos
    posQTLs <- matrix(NA, nrow = 1, ncol = max(NbQTLs))
    if(NbQTLs[iChr] > 0) posQTLs[,1:NbQTLs[iChr]] <- unlist(position[[iChr]][1,which(position[[iChr]][2,] == 1)])
    posSNPs <- matrix(NA, nrow = 1, ncol = max(NbSNPs))
    if(NbSNPs[iChr] > 0) posSNPs[,1:NbSNPs[iChr]] <- unlist(position[[iChr]][1,which(position[[iChr]][2,] == 0)])
    write.table(posQTLs, namePositionQTLs, sep = ",", append = TRUE, col.names = FALSE, row.names = FALSE)
    write.table(posSNPs, namePositionSNPs, sep = ",", append = TRUE, col.names = FALSE, row.names = FALSE)
  }
  
  return(position)
} 

selectionBC <- function(N, P) 
{
  ## truncation selection
  ## i.e. select only the "NbSelection" best individuals
  ## but also random selection
  ## arguments : N = integer : number of individuals
  ##             P = vector : contains the phenotypic values / percentage of recurrent parents of all individuals
  ##             selectionType = character string : "RS" (random selection) or "PS" (selection on the phenotypic values)
  ## return a vector containing the index of the selected individuals

  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  return(selectedIx)
}

selectionMrkrDon <- function(ixm, ixp, ind, iqtl)
{
  if(sum(ind[[iqtl]][5:6, ixm])<4 & sum(ind[[iqtl]][5:6, ixp])<4) { 
    ## donor: index = 1; recurrent parent = 2
    ## => both alleles from donor then sum(alleles) = 2
    ## one allele from donor the other from recurrent : sum = 3
    ## both from recurrent : sum = 4
    ## we want at least one from the donor parents -> sum<4
    return(1)
  }else {
    return(0)
  }
}

## function which returns the index of the markers used for MAS
## dmrk1, dmrk2 : distance (recombination rate between QTL and marker), in cM
## dgb : marker density in the genetic background
## iqtl : index of the carrier chromosome (with the QTL)
indexMarkerMAS <- function(iqtl, dgb, dmrk1, dmrk2, ixSNP, ixQTL, position, NbSNPs)
{
  beginZone <- position[[iqtl]][1, ixQTL] - dmrk1
  endZone <- position[[iqtl]][1, ixQTL] + dmrk2
  ixSNPzone <- which(position[[iqtl]][1,] >= beginZone & position[[iqtl]][1,] <= endZone)
  ixSNPzone <- ixSNPzone[!(ixSNPzone == ixQTL)]
  ixQTLMAS <- NULL
  if(length(ixSNPzone) > 1) ixQTLMAS <- c(min(ixSNPzone), max(ixSNPzone))
  if(length(ixSNPzone) == 1) ixQTLMAS <- ixSNPzone
  ixQTLMAS <- ixQTLMAS[!(ixQTLMAS == ixQTL)]
  backgroundGen1 <- NULL
  backgroundGen2 <- NULL
  if((dgb > 0) & (length(ixSNPzone) == 0)) backgroundGen2 <- round(seq(from = 1, to = NbSNPs[iqtl], by = 1/dgb)) ## if there is no mrkr in the neighborhood of the qtl,
                                                                                                                 ## take the whole chr with a density of dgb
  if((dgb > 0) & (length(ixSNPzone) > 0)) {
    ## divide the genetic  background into two parts only for the chromosome with the qtl
    if(ixSNPzone[1] > 1) backgroundGen1 <- round(seq(from = ixSNP[1], to = ixSNPzone[1]-1, by = 1/dgb))
    if(ixSNPzone[length(ixSNPzone)] < NbSNPs[iqtl]) backgroundGen2 <- round(seq(from = ixSNPzone[length(ixSNPzone)]+1, 
                                                                                                        to = NbSNPs[iqtl], by = 1/dgb))
  }
  ixQTLMAS <- c(backgroundGen1, ixQTLMAS, backgroundGen2)
  ## for the chromosomes not containing the qtl
  ixMAS <- lapply(NbSNPs[-iqtl], function(x) round(seq(from = 1, to = x, by = 1/dgb)))
  ixChr <- 1:length(position)
  ixChr <- ixChr[-iqtl]
  ixMAS[ixChr] <- ixMAS
  ## for the carrier chromosome
  ixMAS[[iqtl]] <- ixQTLMAS
  
  return(ixMAS)
}

## function which gives the index of the markers used for MAS and phenotypic selection
## when want to use only two markers to control the genetic background 
## will give one or two markers around the QTL depending on the position of the QTL
## if the QTL is at one of the end of the chromosome, will give one marker, else will two
index2MarkerMAS <- function(NbChromosome, iqtl, dmrk1, dmrk2, ixSNP, ixQTL, position) 
{
  ixMAS <- vector("list", NbChromosome)
  beginZone <- position[[iqtl]][1, ixQTL] - dmrk1
  endZone <- position[[iqtl]][1, ixQTL] + dmrk2
  ixSNPzone <- which(position[[iqtl]][1,] >= beginZone & position[[iqtl]][1,] <= endZone)
  ixSNPzone <- ixSNPzone[!(ixSNPzone == ixQTL)]
  ixQTLMAS <- NULL
  if(length(ixSNPzone) > 1) ixQTLMAS <- c(min(ixSNPzone), max(ixSNPzone))
  if(length(ixSNPzone) == 1) ixQTLMAS <- ixSNPzone
  
  if(length(ixQTLMAS) > 0) ixMAS[[iqtl]] <- ixQTLMAS[!(ixQTLMAS == ixQTL)] ## if there is nor mrkr in ixQTLMAS -> problem
  
  return(ixMAS)
}


## function which gives the index of the markers used for MAS and phenotypic selection
## when want to use all of the markers on the carriers chromosome 
## (nbr of mrkrs used controlled by dgb = density of markers used)
## -> NOT USED YET : TO TEST
indexMarker <- function(NbChromosome, NbSNPs, iqtl, dgb, ixSNP, ixQTL, position)
{
  ixMAS <- vector("list", NbChromosome)
  ixMAS[[iQTL]] <- ixSNP[seq(from = 1, to = NbSNPs[iqtl], by = 1/dgb)]
  
  return(ixMAS)
}



MAS <- function(NbChromosome, iqtl, dgb, dmrk1, dmrk2, position, generation, NbSNPs)
{
  ## function to do MAS (without phenotypic selection prior to it)
  ixSNP <- which(position[[iqtl]][2,] == 0) 
  ixQTL <- which(position[[iqtl]][2,] == 1)
  ixMrkrMAS <- indexMarkerMAS(iqtl = iqtl, dgb = dgb, dmrk1 = dmrk1, dmrk2 = dmrk2, ixSNP = ixSNP, ixQTL = ixQTL,
                          position = position, NbSNPs = NbSNPs)
  NbMrkrMAS <- sapply(ixMrkrMAS, length)

  ixM <- max(ixMrkrMAS[[iqtl]][ixMrkrMAS[[iqtl]] < ixQTL])
  ixP <- min(ixMrkrMAS[[iqtl]][ixMrkrMAS[[iqtl]] > ixQTL])
  ixMrkrMAS[[iqtl]] <- ixMrkrMAS[[iqtl]][-c(which(ixMrkrMAS[[iqtl]] == ixM), which(ixMrkrMAS[[iqtl]] == ixP))]
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  if(length(generation) == 1) {keepInd <- 1
  }else {keepInd <- sapply(generation, selectionMrkrDon, ixm = ixM, ixp = ixP, iqtl = iqtl)}

  percentage <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)
  percentage <- apply(percentage, 1, sum)
  ord <- order(percentage)
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  #selIx <- selIx[1:NSelected] ## keep the individuals with the less amount of donor

  return(selIx)
}


MASCarrier <- function(NbChromosome, iqtl, dgb, dmrk1, dmrk2, position, generation, NbSNPs)
{
  ## function to do MAS (without phenotypic selection prior to it)
  ixSNP <- which(position[[iqtl]][2,] == 0) 
  ixQTL <- which(position[[iqtl]][2,] == 1)
  ixMrkrMAS <- indexMarkerMAS(iqtl = iqtl, dgb = dgb, dmrk1 = dmrk1, dmrk2 = dmrk2, ixSNP = ixSNP, ixQTL = ixQTL,
                          position = position, NbSNPs = NbSNPs)
  NbMrkrMAS <- sapply(ixMrkrMAS, length)

  ixM <- max(ixMrkrMAS[[iqtl]][ixMrkrMAS[[iqtl]] < ixQTL])
  ixP <- min(ixMrkrMAS[[iqtl]][ixMrkrMAS[[iqtl]] > ixQTL])
  ixMrkrMAS[[iqtl]] <- ixMrkrMAS[[iqtl]][-c(which(ixMrkrMAS[[iqtl]] == ixM), which(ixMrkrMAS[[iqtl]] == ixP))]
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  if(length(generation) == 1) {keepInd <- 1
  }else {keepInd <- sapply(generation, selectionMrkrDon, ixm = ixM, ixp = ixP, iqtl = iqtl)}

  percentage <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)
  ## use only the percentage of the carrier chromosome to calculate the percentage used for selection
  percentage <- percentage[,iqtl]
  if(length(iqtl) > 1) percentage <- apply(percentage, 1, sum) ## if iqtl corresponds to more than one qtl, do the sum on the different chromosomes
  ord <- order(percentage)
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  #selIx <- selIx[1:NSelected] ## keep the individuals with the less amount of donor

  return(selIx)
}


## function to do MAS + phenotypic selection
## MAS, as in the MAS function
## but add condition on the QTL to not lose it
MASQTL <- function(N, NbChromosome, iqtl, dgb, P, position, generation, NbSNPs)
{

  ## function to do MAS
  ixSNP <- lapply(position, function(x) which(x[2,] == 0)) 
  ixMrkrMAS <- lapply(ixSNP, function(x) x[seq(from = 1, to = length(x), by = 1/dgb)])
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  
  ## phenotypic selection
  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  keepInd <- rep(0, N)
  keepInd[selectedIx] <- 1

  percentage <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)
  percentage <- apply(percentage, 1, sum)
  ord <- order(percentage)
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  #selIx <- selIx[1:NSelected] ## keep the individuals with the less amount of donor
  
  return(selIx)
}


## function to do MAS + phenotypic selection
## MAS, as in the MAS function
## but add condition on the QTL to not lose it
## PLUS: MAS is done only on the carrier chromosome
MASQTLCarrier <- function(N, NbChromosome, iqtl, dgb, P, position, generation, NbSNPs)
{
  
  ## function to do MAS
  ixSNP <- lapply(position, function(x) which(x[2,] == 0)) 
  ixMrkrMAS <- lapply(ixSNP, function(x) x[seq(from = 1, to = length(x), by = 1/dgb)])
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  
  ## phenotypic selection
  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  keepInd <- rep(0, N)
  keepInd[selectedIx] <- 1
  
  percentage <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)
  ## use only the percentage of the carrier chromosome to calculate the percentage used for selection
  percentage <- percentage[,iqtl]
  if(length(iqtl) > 1) percentage <- apply(percentage, 1, sum) ## if iqtl corresponds to more than one qtl, do the sum on the different chromosomes
  ord <- order(percentage)
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  #selIx <- selIx[1:NSelected] ## keep the individuals with the less amount of donor
  
  return(selIx)
}


indexClosestCO <- function(iqtl, ixSNP, ixQTL, ind)
{

	## function returning the index of the markers at the extremities of the haplotypic block around the target locus (position of the closest co from the target on its right and left)

	ixRecurrent <- which(apply(ind[[iqtl]][5:6,],2,sum) == 4)

	ixLeft <- ixRecurrent[which(ixRecurrent < ixQTL)]
	ixRight <- ixRecurrent[which(ixRecurrent > ixQTL)]

	## if the whole chromosome (or at least one side) is from the donor -> no crossover
	if(length(ixLeft) == 0) ixLeft <- 0 ## index of the marker before the chr
	if(length(ixRight) == 0) ixRight <- length(ixSNP) + 1 ## index of the marker after the chr

	return(c(max(ixLeft),min(ixRight)))
}

## function to select one recombinant on one side at one generation
## and on the other side at the next generation
## pre-selection of the ind with the target locus
MASQTLFlanking <- function(N, iqtl, P, position, generation)
{

	## function of selection
	## step 1: selection on the target locus
	## step 2: order the ind depending on the distance of their closest crossover to the target locus
	## return the index of the individual with respect to these two criteria

	## phenotypic selection
  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  keepInd <- rep(0, N)
  keepInd[selectedIx] <- 1

	ixSNP <- which(position[[iqtl]][2,] == 0)
	ixQTL <- which(position[[iqtl]][2,] == 1)

	posCO <- sapply(generation, indexClosestCO, iqtl = iqtl, ixSNP = ixSNP, ixQTL = ixQTL)
	lengthBlock <- abs(ixQTL - posCO) ## matrix 2xNind (1st row: co on the left, 2nd row: co on the right)

	if(SIDE == 0) { ## 1st generation for the selection on closest co
		closestCO <- apply(lengthBlock,2,min) ## for each individual keep only the closest co from the two (left/right)
		ord <- order(closestCO) ## order the distance target - co (want the smallest one
		keepInd <- keepInd[ord] ## order the individual depending on the distance target - closest co
		selIx <- ord*keepInd
		selIx <- selIx[selIx != 0]
		
		side <- 3 - which(lengthBlock[,selIx[1]] == min(lengthBlock[,selIx[1]]))[1] ## from which side was the closest co + for the next generation of selection, will look at the other side (1 ie left -> 2 ie right and 2 -> 1); keep only the first element ([1]) if crossovers on both sides happen at the same distance from the target locus
	}

	if(SIDE != 0) { ## 2nd generation for the selection on closest co
		ord <- order(lengthBlock[SIDE,]) ## look at one side of the target (the one not done) -> order the distance target - co
		keepInd <- keepInd[ord] ## order the individual depending on the distance target - closest co
		selIx <- ord*keepInd
		selIx <- selIx[selIx != 0]
		
		side <- 3 - SIDE ## for the next generation of selection, will look at the other side (1 ie left -> 2 ie right and 2 -> 1)
	}

	SIDE <<- side ## give its new value to the global variable SIDE
	return(selIx)

}


## function to order individuals based on the "look ahead" criterion
## pre-selection of the ind with the target locus
MASQTLCrit <- function(N, iqtl, P, position, generation, LG)
{

	## phenotypic selection
  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  keepInd <- rep(0, N)
  keepInd[selectedIx] <- 1

	proba <- sapply(generation, function(x) prod(mapply(function(w,y,z) probaChr(w[5:6,],y[1,],y[2,],z), x, position, LG)))
  ord <- order(proba, decreasing = TRUE) ## want to keep the ind with the highest proba
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  
  return(selIx)

}



Blocks <- function(snps)
{
 
	## return a vector with the number of blocks for each marker
  ## snps  : haplotypique sequence of the markers (2 if homozygote donor, 3 if heterozygote, 4 if homozygote recurrent)

	snps[snps == 2 | snps == 3] <- 1 ## for one marker, equal to 1 if there is at least one allele from the donor
	snps[snps == 4] <- 2 ## for one marker, equal to 2 if homozygote recurrent
 
  idon <- 1
  itot <- 1
  cdon <- 0 ## gives the block (from the donor) in which the marker is (vector as there is one element per marker)
  if(snps[1] == 1) cdon <- 1
  ctot <- 1 ## total number of blocks
  nbsnps <- length(snps)
  nbDon <- cdon ## number of blocks from the donor
 
  for(j in 2:nbsnps) {
    if(snps[j] == snps[j-1]) {
      cdon <- c(cdon, cdon[idon])
      ctot <- c(ctot, ctot[itot])
      itot <- itot + 1
      idon <- idon + 1
    }
    if(snps[j] != snps[j -1]) {
      if(snps[j] == 1) {cdon <- c(cdon, cdon[idon]+nbDon+1); nbDon <- nbDon + 1}
      if(snps[j] == 2) cdon <- c(cdon, 0) ## do not count as a marker from the donor
      ctot <- c(ctot, ctot[itot]+1)
      itot <- itot + 1
      idon <- idon + 1
    }
  }
 
 
  return(tot = ctot)
}

## function calculating the probability for a chromosome to be completely recurrent at the next generation
## (will stille keeping the target locus if it concerns the carrier chromosome)
probaChr <- function(ind, position, originMrkr, LG)
{

	## ind = matrix 2xNmarkers: origin of each markers (one row per homologous chromosome)
	## position = vector Nmarkers: position of the markers
	## originMrkr = vector Nmarkers: type of markers (snps/qtl)
	## LG = numeric: genetic length of the chromosome

	snps <- apply(ind,2,sum) ## for one marker, can be equal to 2 (homozygote donor), 3 (heterozygote) or 4 (homozygote recurrent)
	ctot <- Blocks(snps)
	position <- position/100 ## need the positions in Morgans
	LG <- LG/100 ## need the lengths in Morgans

	nsnps <- length(position)
	l <- NULL
	origin <- NULL
	ixqtl <- 0 ## initiate the index of the qtl at 0 (will need it if not on the carrier chr)
	proba <- 1 ## when need the proba value (as we multiply, giving it a value of 1 has no incidence on the calculation)

	if(sum(originMrkr == 1) > 0) ixqtl <- which(originMrkr == 1) ## in the case of the carrier chr, define the index of the qtl

	for(i in unique(ctot)) {
		ix <- which(ctot == i)
		pos <- position[ix]
		ixmin <- ix[pos == min(pos)]
		ixmax <- ix[pos == max(pos)]

		## origin of the marker (will be used to calculate the probability)
		or <- snps[ix]
		or[or == 2 | or == 3] <- 1
		or[or == 4] <- 2

		## do not simply take min(pos) and max(pos) as the mrkr positions are discrete and so, suppose that if a co happened, it happened in the middle of the interval
		pmin <- (position[ixmin] + position[max(1,ixmin-1)])/2 ## take max(1,ixmin-1) if ixmin = 1 (ie 1st markr of the interval if also the 1st mrkr of the chromosome
		if(ixmin == 1) pmin <- 0 ## if the 1st marker is not at the beginning of the chromosome, take 0 as the first position
		pmax <- (position[ixmax] + position[min(nsnps,ixmax+1)])/2 ## take min(nsnps,ixmax+1) if ixmax = nsnps (ie last markr of the interval if also the last mrkr of the chromosome
		if(ixmax == nsnps) pmax <- LG ## if the 1st marker is not at the beginning of the chromosome, take 0 as the first position


		if(sum(originMrkr == 1) > 0 & ixqtl %in% ix) { ## if we are on the carrier chr and in the blocks containing the qtl, then separate the block into two blocks: one on the right and another on the left of the qtl
			origin <- c(origin, unique(or),unique(or))
			l <- c(l, position[ixqtl]-pmin, pmax-position[ixqtl])
			## in this situation, l and origin will have one more element than the number of blocks
		} else {
			origin <- c(origin, unique(or))
			l <- c(l, pmax - pmin) ## block length
		}
	}

	if(sum(originMrkr == 1) > 0) { ## if we look at the carrier chr (originMrkr == 1 at the qtl)
		ixqtl <- which(originMrkr == 1)
		bqtl <- ctot[ixqtl]
		proba <- 0.5*exp(-LG)

		if(bqtl == (length(l)-1)) {proba <- proba*sinh(l[bqtl+1])} ## if qtl at the end of the chromosome
		if(bqtl == 1) {proba <- proba*sinh(l[bqtl])} ## if qtl at the beginning of the chromosome
		if(bqtl < (length(l)-1)) {proba <- proba*sinh(l[bqtl+1]+l[bqtl+2]); l <- l[-(bqtl+2)]; origin <- origin[-(bqtl+2)]} ## [bqtl-1] is the recurrent bloc adjacent on the right to the right part of the qtl block [bqtl+1]
		if(bqtl > 1) {proba <- proba*sinh(l[bqtl-1]+l[bqtl]); l <- l[-(bqtl-1)]; origin <- origin[-(bqtl-1)]} ## [bqtl-1] is the recurrent bloc adjacent on the left to the left part of the qtl block [bqtl]

		## remove the blocks at the extremity if they are homozygote recurrent
		if(origin[1] == 2) {proba <- proba*exp(l[1]); l <- l[-1]; origin <- origin[-1]}
		if(origin[length(origin)] == 2) {proba <- proba*exp(l[length(l)]); l <- l[-length(l)]; origin <- origin[-length(origin)]}

		proba <- proba*prod(cosh(l[origin == 2])) ## if there is no more block from the recurrent, prod(numeric(0)) = 1

	} else { ## otherwise, we consider non carrier chr
		if(sum(snps == 4) == nsnps) { ## ie all the markers are homozygote recurrent
			proba <- 1
		} else {
			## remove the blocks at the extremity if they are homozygote recurrent
			if(origin[1] == 2) {proba <- proba*exp(l[1]); l <- l[-1]; origin <- origin[-1]}
			if(origin[length(origin)] == 2) {proba <- proba*exp(l[length(l)]); l <- l[-length(l)]; origin <- origin[-length(origin)]}

			## if remove the recurrent block at the extremity, must also take it into account when calculate the probability: as we use exp(-LG), must take the removed length by multiplying the proba by the exp of the removed lengths

			proba <- proba*0.5*exp(-LG)*prod(cosh(l[origin == 2]))
		}
	}
	return(proba)
}



## function for the phenotypic selection with one marker 
MASP <- function(N, NbChromosome, iqtl, dmrk1, dmrk2, P, position, generation)
{
  ## function to do MAS, with one marker (with phenotypic selection prior to it)
  ixSNP <- which(position[[iqtl]][2,] == 0) 
  ixQTL <- which(position[[iqtl]][2,] == 1)
  ixMrkrMAS <- index2MarkerMAS(NbChromosome = NbChromosome, iqtl = iqtl, dmrk1 = dmrk1,
                               dmrk2 = dmrk2, ixSNP = ixSNP, ixQTL = ixQTL, position = position) 
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  
  ## phenotypic selection
  NbInd <- 1:N
  selectedIx <- NbInd[P == max(P)] ## take all the individuals who have the max phenotypic value in the pop
  keepInd <- rep(0, N)
  keepInd[selectedIx] <- 1
  
  ## marker selection
  percentage <- pgDonPerChr1Hom(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation, iqtl = iqtl)
  percentage <- apply(percentage, 1, sum, na.rm = TRUE)
  ord <- order(percentage)
  keepInd <- keepInd[ord]
  selIx <- ord*keepInd
  selIx <- selIx[selIx != 0]
  
  return(selIx)
  
}


PassageGenerationBC <- function(NbChromosome, NbQTLs, NbSNPs, geneticLength, modeRecombination, reproductionType, 
                                position, recurrentParent, generationSelected, degreeOfDominance, ixParents, nuGamma, pGamma)
{
  
  ## function to create a new diploid (pair of chromsomes) individual
  ## arguments : NbChromosome = integer : number of chromosome
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             NbSNPs = vector : number of SNPs for each chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             modeRecombination = string of character : "Haldane", "completeInterference", "freeRecombination"
  ##             reproductionType = string of character : "HD" (doubled haploid), "SP" (selfing), "SC" (panmixie)
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             generationSelected = list of list of matrix : effect, index, type and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals (selected)
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs)) 
  ##             ixParents = vector or integer : index, in the population, of the parent(s)
  ## return a list of matrix (NbChromosome matrices of size 5xNbQTL)
  
  l <- length(generationSelected)
  genome <- vector("list", NbChromosome)
  k <- ixParents
  
  ## always standard cross (panmixie)
  individu1 <- recurrentParent
  individu2 <- generationSelected[[k]]
  gamete1 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu1, nuGamma = nuGamma, pGamma = pGamma)
  gamete2 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu2, nuGamma = nuGamma, pGamma = pGamma)
  
  for (k in 1:NbChromosome) {
    m <- matrix(nrow = 8, ncol = length(position[[k]][1,]))
    m[1,] <- gamete1[[k]][1,]
    m[2,] <- gamete2[[k]][1,]
    m[3,] <- gamete1[[k]][2,] ## change to accomodate adding lines for index alleles
    m[4,] <- gamete2[[k]][2,]
    m[5,] <- gamete1[[k]][3,] ## change to accomodate adding lines for origin alleles
    m[6,] <- gamete2[[k]][3,]
    m[7,which(position[[k]][2,] == 1)] <- degreeOfDominance[k,1:NbQTLs[k]] 
    m[7,which(position[[k]][2,] == 0)] <- rep(0.5, NbSNPs[k])
    m[8,] <- position[[k]][2,]
    genome[[k]] <- m
  }
  return(genome)
}


## when analyse only the chromosome which received part of the donor parent
percentageDonorParent1Hom <- function(ixMrkr, ind, iqtl)
{
  ## function which returns the number of snps coming from the donor parent of one individual
  ## (do not take into account the QTL to introgress)
  ## used in marker assisted selection 
  ## to select the individuals with the most snps from the donor parent
  ix <- which(ind[[iqtl]][5:6,which(ind[[iqtl]][8,] == 1)] == 1) + 4
#   snpIx <- lapply(ind, function(x) which(x[8,] == 0))
#   nsnp <- unlist(lapply(snpIx, length))
  nsnp <- unlist(lapply(ixMrkr, length))
  pgDon <- mapply(function(x,y) tabulate(x[ix,y], nbins = 2), ind, ixMrkr)
  return(pgDon[1,])
}

## when analyse only the chromosome which received part of the donor parent
## rmq : percentage of the snps which come from the donor parent
## ixMrkr : index of the mrkr used to do the marker-assisted selection
pgDonPerChr1Hom <- function(nchr, nbsnp, ixMrkr, pop, iqtl)
{
  pg <- lapply(pop, percentageDonorParent1Hom, ixMrkr = ixMrkr, iqtl = iqtl)
  pg <- lapply(pg, function(x) 100*x/(nbsnp))
  pg <- matrix(unlist(pg), ncol = nchr, byrow = TRUE)
  # pg <- apply(pg, 2, mean)
  return(pg)
}

## when analyse only the chromosome which received part of the donor parent
chunkChr1Hom <- function(snps)
{
  idon <- 2
  itot <- 2
  cdon <- 0 ## count the number of blocks from the donor (ie parent 1)
  if(snps[1] == 1) cdon <- 1
  ctot <- 1 ## total number of blocks
  nbsnps <- length(snps)
  
    for(j in 2:nbsnps) {
      if(snps[j] == snps[j-1]) {
        if(snps[j] == 1) {cdon <- c(cdon, cdon[idon-1]); idon <- idon + 1} 
        ## if do not put {} around, the second part (after;) will be done each time the if(snps[i] == snps[i-1]) is entered (indpdt of the second if)
        ctot <- c(ctot, ctot[itot-1])
        itot <- itot + 1
      }
      if(snps[j] != snps[j -1]) {
        if(snps[j] == 1) {cdon <- c(cdon, cdon[idon-1]+1); idon <- idon + 1}
        ctot <- c(ctot, ctot[itot-1]+1)
        itot <- itot + 1
      }
    }
  # cdon <- cdon[-1]
  return(list(totNbChunk = ctot, donorNbChunk = cdon))
}

## analysis for the homologous chromosome which does not come from the recurrent parents
## ixMrkr : index of the mrkrs being used for the marker-assisted selection
analysisBC1Hom <- function(name, rep, g, ixMrkr, NbChr, iqtl)
{
  if(g == 0) namePop <- paste(name, rep, "InitialPopulation.RData", sep = "")
  if(g > 0) namePop <- paste(name, rep, "_", g, ".RData", sep = "")
  load(namePop); pop <- generation
  
  ## step of selection -> analysis only on the ind which have the qtl from the donor parent
  GChr <- lapply(lapply(pop, lapply, function(x) x[1:2,]), sapply, sum)
  G <- lapply(GChr, sum) ## G for each individual
  G <- unlist(G)
  NInd <- 1:length(pop)
  keepIx <- NInd[G>0]
  pop <- pop[keepIx] ## pop with only the ind which contain the QTL of the donor parents on at least one chromosome
  
  if(length(pop) == 0) return(NULL)
  
  ix <- sapply(pop, function(x) which(x[[iqtl]][5:6,which(x[[iqtl]][8,] == 1)] == 1) + 4)
  ## return 5 if the QTL is on the homologous chromosome 1, 6 if it is on the homologous chromosome 2
  
  mrkrs <- mapply(function(x,y) lapply(x, function(z) z[y,]), pop, ix, SIMPLIFY = FALSE)
  ## haplotype of the homologous chromosome from the cross
  
  NbMrkr <- sapply(ixMrkr, length)
  percentage <- pgDonPerChr1Hom(nchr = NbChr, nbsnp = NbMrkr, ixMrkr = ixMrkr, pop = pop, iqtl = iqtl) ## interpolate the percentage found for the mrkrs to all the SNPs
  chunks <- lapply(mrkrs, lapply, chunkChr1Hom)
  
  pgPerInd <- apply(percentage, 1, mean)
  pgPerChr <- apply(percentage, 2, mean)
  pgPerChrSd <- apply(percentage, 2, sd)
  
  nbChunks <- lapply(chunks, lapply, function(x) max(x$totNbChunk))
  nbDonorChunks <- lapply(chunks, lapply, function(x) max(x$donorNbChunk))
  nbChunks <- matrix(unlist(nbChunks), ncol = NbChr, byrow = TRUE)
  nbDonorChunks <- matrix(unlist(nbDonorChunks), ncol = NbChr, byrow = TRUE)
  
  nbChunksPerInd <- apply(nbChunks, 1, mean)
  nbChunksPerChr <- apply(nbChunks, 2, mean)
  nbDonorChunksPerInd <- apply(nbDonorChunks, 1, mean)
  nbDonorChunksPerChr <- apply(nbDonorChunks, 2, mean)
  
  sizeChunkPerChr <- lapply(chunks, lapply, function(x) tabulate(x$donorNbChunk))
  meanSizeChunk <- rep(NA, NbChr)
  for(chr in 1:NbChr) {
    meanSizeChunk[chr] <- mean(unlist(sapply(sizeChunkPerChr, function(x) x[[chr]])))
  }
  
  
  return(list(NbInd = length(pop), pgPerChr = pgPerChr, pgPerChrSd = pgPerChrSd, ncPC = nbChunksPerChr, ndcPC = nbDonorChunksPerChr, sc = meanSizeChunk))
  
}



mainBC <- function(name, namePositionSNPs, namePositionQTLs,
                   N, NbChromosome, VG, h2, homozygous, coupling,
                   Lcoupling, geneticLength, NbQTLs, NbSNPs, NbAlleles,
                   shapeParameterPositionQTLs, shapeParameterEffectQTLs,
                   modeCreationGenome, modeCreationPosition, modeRecombination,
                   iqtl, degreeOfDominance, positionQTLs, gparameters, seed, repet, dgb, dmrk1, dmrk2, selectionGeneration1, nuGamma, pGamma, gSelfing)
{
  
  ## main function performing the backcross
  
  seed <- seed + repet
  set.seed(seed)
  print(seed) ## print the seed in the .out file if need to reproduce the same exact simulation (use the same seed)
  
  gmax <- length(gparameters$g)
  
  nameExtended <- paste(name, repet, sep = "")
  namePgDon <- paste(nameExtended, "DistribPgDon", sep = "")
  nameP <- paste(nameExtended, "DistribP", sep = "")
  nameEffect <- paste(nameExtended, "Effect", sep = "") ## for all of the repet, always have the same effect for the QTLs and alleles?
  namePEvolution <- paste(name, "Gain.txt", sep = "")
  if (repet == "1") write.table(list("repet", "g", "P", "varP", "G", "varG", "GNoDominance", "Pmax", "Gmax"), file = namePEvolution, append = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
  nameParameters <- paste(name, "Parameters.txt", sep="") ## name of the file which contains the diffrent parameters
  nameGenome <- readName(name = nameExtended, extension = "FinalPopulation", modeCreation = modeCreationGenome) ## name of the file containing the QTL values for the initial generation
  namePosition <- readName(name = name, extension = "pos", modeCreation = modeCreationPosition) ## name of the file containing the QTL positions for the initial generation
  nameLocus <- paste(name, "NbLocus.txt", sep="") ## name of the file containing the number of QTL for each chromosome of each individuals for the initial generation

  if (repet != "1" & repet != "" & (modeCreationPosition == "gaussian" | modeCreationPosition == "existGaussian")) modeCreationPosition <- "existGaussian"
  geneticLength0 <- geneticLength
  position0 <- creationPositionBC(namePositionSNPs = namePositionSNPs, namePositionQTLs = namePositionQTLs, NbChromosome = NbChromosome,
                                 shapeParameterQTLs = shapeParameterEffectQTLs, NbSNPs = NbSNPs, NbQTLs = NbQTLs, geneticLength = geneticLength0) ## creation of the QTL positions (or reading of the file containg the QTL positions) for the initial generation
  # position <- lapply(position, function(x) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE))
  if(modeCreationPosition == "exist") NbSNPs <- mapply(function(x) sum(1-x[2,which(x[2,] == 0)]), position0)
  positionSNPs0 <- lapply(position0, function(x) x[1,which(x[2,] == 0)]) ## position of the SNPs
  positionQTLs0 <- lapply(position0, function(x) x[1,which(x[2,] == 1)]) ## position of the QTLs

	position <- position0
	geneticLength <- geneticLength0
  
  generation <- creationValueParentsBC(N = N, NbChromosome = NbChromosome, Ms = NbQTLs, VG = VG, homozygous = homozygous, coupling = coupling, Lcoupling = Lcoupling, modeCreationMap = modeCreationGenome, nameGenome = nameGenome, nameEffect = nameEffect, NbAlleles = NbAlleles,
                                 shapeParameterQTLs = shapeParameterEffectQTLs, degreeOfDominance = degreeOfDominance, position = position0, snps = positionSNPs0) ## creation of the QTL values (or reading of the file containg the QTL values) for the initial generation
  recurrentParent <- generation[[2]]
  generation <- list(generation[[-2]])
  
  NbMarkers <- NbSNPs + NbQTLs
  # geneticLength <- increaseLG*geneticLength
  
  save(generation, file = paste(nameExtended, "InitialPopulation.RData", sep = ""))
  
  Ninitial <- N 
  N <- 1


	## in this part, test whether there is multiple level of recombination during the scheme
	rec <- unique(gparameters$Recombination)
	if(length(rec) == 1) { ## gpos is a vector containing the number of the generations where recombination is changed; thus when there is only one level of rec, there is nothing in gpos
		gpos <- NULL 
	} else { ## in this situation, we suppose that there will be only two level of recombination
		rec1 <- rec[1]
		rec2 <- rec[2]
		gpos <- which(gparameters$Recombination == rec2) ## the reference level of recombination is the one of the first generation (g = 1). Thus it will be always be 1 (the position.csv given as a parameter will refer to this level while positionBis.csv referes to rec2). Note that the name for rec1 is given as a parameter and can be changed while the name for rec2 will always be the same ie positionBis.csv (if want to change, change it directlmy in this script).
	}
	
  
  ############################################ SELECTION CYCLES ############################################
  

  g <- 0
  VE <- 0
  if(g == selectionGeneration1) VE <- calculVE(N = N, NbChromosome = NbChromosome, h2 = h2, generation = generation) 
  ## calculate the environmental variance for the first generation where the selection is applied
  ## keep the same after
  
  P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation) 
  ixMrkrMAS <- lapply(NbSNPs, function(x) 1:x)
  NbMrkrMAS <- sapply(ixMrkrMAS, length)
  pgDonP <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)

  for (g in 1:gmax) { ## loop on the generation
    
    selectionType <- as.character(gparameters$Selection[which(gparameters$g == g)])
    reproductionType <- as.character(gparameters$Reproduction[which(gparameters$g == g)])
    # selectionType <- "MASQTL"
    print(selectionType)
    NSelected <- gparameters$NSelected[which(gparameters$g == g)]


    generationSelected <- list()
    ## phenotypic selection
    if(selectionType == "PS") {
      indexSelected <- selectionBC(N = N, P = P)
    }
    ## marker-assisted selection : mrkr around the QTL from the donor, others from the recurrent
    if(selectionType == "MAS") {
       indexSelected <- MAS(NbChromosome = NbChromosome, iqtl = iqtl, dgb = dgb, dmrk1 = dmrk1, dmrk2 = dmrk2, 
                                 position = position, generation = generation, NbSNPs = NbSNPs)
       ## do not know how many individual were kept via MAS
       ## do selection only if the nbr of ind we want to keep is less than the number of individual which can be kept for the next generation
       if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    } 
    ## marker-assisted selection : mrkr around the QTL from the donor, others from the carrier (apart from inside the QTL segment) from the recurrent
    if(selectionType == "MASCarrier") {
       indexSelected <- MASCarrier(NbChromosome = NbChromosome, iqtl = iqtl, dgb = dgb, dmrk1 = dmrk1, dmrk2 = dmrk2, 
                                 position = position, generation = generation, NbSNPs = NbSNPs)
       ## do not know how many individual were kept via MAS
       ## do selection only if the nbr of ind we want to keep is less than the number of individual which can be kept for the next generation
       if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    }
    ## selection on one marker + phenotypic selection
    if(selectionType == "MASP") {
      indexSelected <- MASP(N = N, NbChromosome = NbChromosome, iqtl = iqtl, dmrk1 = dmrk1, dmrk2 = dmrk2,
                            P = P, position = position, generation = generation)
      if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    } 
    ## selection on all of the markers (carrier and non carrier chr) + phenotypic selection
    if(selectionType == "MASQTL") {
      indexSelected <- MASQTL(N = N, NbChromosome = NbChromosome, iqtl = iqtl, dgb = dgb,
                              P = P, position = position, generation = generation, NbSNPs = NbSNPs)
      if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    } 
    ## selection on all of the markers (carrier only) + phenotypic selection
    if(selectionType == "MASQTLCarrier") {
      indexSelected <- MASQTLCarrier(N = N, NbChromosome = NbChromosome, iqtl = iqtl, dgb = dgb,
                              P = P, position = position, generation = generation, NbSNPs = NbSNPs)
      if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    } 
    ## selection on the flanking markers (one side only) + phenotypic selection
    if(selectionType == "MASQTLFlanking") {
      indexSelected <- MASQTLFlanking(N = N, iqtl = iqtl, P = P, position = position, generation = generation)
      if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    } 
		## selection withe the selection criterion being the probability of getting the wholme chromosome from the recurrent at the next generation
    if(selectionType == "MASQTLCrit") {
      indexSelected <- MASQTLCrit(N = N, iqtl = iqtl, P = P, position = position, generation = generation, LG = geneticLength)
      if(NSelected < length(indexSelected)) indexSelected <- indexSelected[1:NSelected]
    }
	
    
    print(paste0("There were (was) ", length(indexSelected), " individual(s) selected for generation ", 
                 g, " and replicate ", repet))

		if(length(indexSelected) < 1) return() ## in the situation where there are no individuals selected, stop the replicate
    
    generationSelected <- generation[indexSelected]
    
		save(generationSelected, file = paste(nameExtended, "Selected_", g, ".RData", sep = ""))

    N <- gparameters$NOffspring[which(gparameters$g == g)]
    generation <- vector("list", N)

		if(g %in% gpos) { ## in the case where the level of recombination changed from the reference one (always g > 1)
			if(file.exists("positionBisSNPs.csv") & file.exists("positionBisQTLs.csv") & file.exists("geneticLengthBis.csv")) { ## 1st situation: there exists another files for the position of the SNPs and QTLs
				geneticLength <- unlist(read.table("geneticLengthBis.csv", header = FALSE, stringsAsFactors = FALSE, sep = ","), use.names = FALSE) ## read the file with the new genetic lengthes
				position <- creationPositionBC(namePositionSNPs = "positionBisSNPs.csv", namePositionQTLs = "positionBisQTLs.csv", NbChromosome = NbChromosome, shapeParameterQTLs = shapeParameterEffectQTLs, NbSNPs = NbSNPs, NbQTLs = NbQTLs, geneticLength = geneticLength)
			} else { ## 2nd situation: there is no position files and thus do a simple dilatation of the generic map
    			increaseLG <- gparameters$Recombination[which(gparameters$g == g)]
					geneticLength <- increaseLG * geneticLength0
					position <- lapply(position0, function(x) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE))
			}
		} else { ## if the level of rec is the same as the reference one, just use the initial positions
				geneticLength <- geneticLength0
				position <- position0
		}
    

    ixParents <- sample(1:length(indexSelected), N, replace = TRUE)
		if(g %in% gSelfing) { ## if generation of selfing use PassageGeneration
    	for (i in 1:N) {
      	ixParentsI <- ixParents[i] ## selfing so use only one parent
				generation[[i]] <- PassageGeneration(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
                                           reproductionType = reproductionType, position = position, generationSelected = generationSelected,
                                           degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
			}
    } else { ## if g is not a generation of selfing use PassageGenerationBC -> do a BC ie a cross with recurrent parent
    	for (i in 1:N) {
      	ixParentsI <- ixParents[i] 
      	generation[[i]] <- PassageGenerationBC(NbChromosome = NbChromosome, NbQTLs = NbQTLs, NbSNPs = NbSNPs, geneticLength = geneticLength, modeRecombination = modeRecombination,
                                           reproductionType = reproductionType, position = position, recurrentParent = recurrentParent, generationSelected = generationSelected,
                                           degreeOfDominance = degreeOfDominance, ixParents = ixParentsI, nuGamma = nuGamma, pGamma = pGamma)
			}
    }

    if(g == selectionGeneration1) VE <- calculVE(N = N, NbChromosome = NbChromosome, h2 = h2, generation = generation)
    
    P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation) 
    ixMrkrMAS <- lapply(NbSNPs, function(x) 1:x)
    NbMrkrMAS <- sapply(ixMrkrMAS, length)
    pgDonP <- pgDonPerChr(nchr = NbChromosome, nbsnp = NbMrkrMAS, ixMrkr = ixMrkrMAS, pop = generation)

    
    # save(generation, file = paste(nameExtended, "_", g, ".RData", sep = ""))
    
    ## select the individuals which respect the following condition :
    ## have the QTL with a positive effect from the donor parent
    ## have the minimum of snps coming from the recurrent parent

    
  }

}


######################

nbDon <- function(orMrkr, weight)
{
	## function that returns the number of markers from the donor parent
	## orMrkr = matrix 2xNsnp: origin (donor/recurrent) of the markers
	## weight = vector Nsnp: weight for each mrkrs

	orMrkr[orMrkr == 2] <- 0 ## remove all of the markers coming from the recurrent parent
	orMrkr[1,] <- orMrkr[1,]*weight ## multiply the markers by their weight
	orMrkr[2,] <- orMrkr[2,]*weight
	return(sum(orMrkr)) ## to have the weighted number of markers from the donor, add all of the values

}

## take into account both of the homologous chromosomes in the calculation (coming from the recurrent and from the backcross parents)
## calculation of the number of snps (per chromosome) coming from the donor parent (parent 1) in the individual ind
percentageDonorParent <- function(ixMrkr, ind)
{
  ## function which returns the number of snps coming from the donor parent of one individual
  ## (do not take into account the QTL to introgress)
  ## used in marker assisted selection 
  ## to select the individuals with the most snps from the donor parent
  snpIx <- lapply(ind, function(x) which(x[8,] == 0))
  # nsnp <- unlist(lapply(snpIx, length))
  nsnp <- unlist(lapply(ixMrkr, length))

	if(file.exists("weightMrkrs.csv")) { ## this part load the weight of each markers (if the file exist) else put 1 for all markers
		weight <- read.table("weightMrkrs.csv", header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE, col.names = 1:max(nsnp))
		weight <- apply(weight, 1, list)
  	weight <- lapply(weight, unlist)
  	weight <- lapply(weight, function(x) x[!is.na(x)])
	} else {
		weight <- lapply(snpIx, function(x) rep(1,length(x)))
	}

  pgDon <- mapply(function(x,y,z) nbDon(x[5:6,y],z), ind, snpIx, weight)
  return(pgDon)
}

## mean of the percentage of the donor genome for each chromosome
pgDonPerChr <- function(nchr, nbsnp, ixMrkr, pop)
{
  pg <- lapply(pop, percentageDonorParent, ixMrkr = ixMrkr)
  # pg <- lapply(pg, function(x) 100*x/(2*nbsnp)) ## should divide by sum of weight instead of by the number of snps as for the carrier, some snps weight less than the others
  pg <- matrix(unlist(pg), ncol = nchr, byrow = TRUE)
  # pg <- apply(pg, 2, mean)
  return(pg)
}

## calculation of the nbr of total nbr of chunks and those coming from the donor parents
## the results can also be used to measure the length of the chunks
chunkChr <- function(snps)
{
  idon <- 2
  itot <- 2
  cdon <- 0
  if(snps[1,1] == 1) cdon <- 1
  ctot <- 1
  nbsnps <- length(snps[1,])
  
  for(i in 1:2) {
    if(i == 2) {
      if(snps[2,1] == 1) {cdon <- c(cdon, cdon[idon-1]+1); idon <- idon + 1} 
      ctot <- c(ctot, ctot[itot-1] + 1)
      itot <- itot + 1
    }
    for(j in 2:nbsnps) {
      if(snps[i,j] == snps[i,j-1]) {
        if(snps[i,j] == 1) {cdon <- c(cdon, cdon[idon-1]); idon <- idon + 1} 
        ## if do not put {} around, the second part (after;) will be done each time the if(snps[i] == snps[i-1]) is entered (indpdt of the second if)
        ctot <- c(ctot, ctot[itot-1])
        itot <- itot + 1
      }
      if(snps[i,j] != snps[i,j -1]) {
        if(snps[i,j] == 1) {cdon <- c(cdon, cdon[idon-1]+1); idon <- idon + 1}
        ctot <- c(ctot, ctot[itot-1]+1)
        itot <- itot + 1
      }
    }
  }
  # cdon <- cdon[-1]
  return(list(totNbChunk = ctot, donorNbChunk = cdon))
}

analysisBC <- function(name, rep, g, NbSNPs, ixMrkr, NbChr)
{
  if(g == 0) namePop <- paste(name, rep, "InitialPopulation.RData", sep = "")
  if(g > 0) namePop <- paste(name, rep, "_", g, ".RData", sep = "")
  load(namePop); pop <- generation
  
  ## step of selection -> analysis only on the ind which have the qtl from the donor parent
  GChr <- lapply(lapply(pop, lapply, function(x) x[1:2,]), sapply, sum)
  G <- lapply(GChr, sum) ## G for each individual
  G <- unlist(G)
  NInd <- 1:length(pop)
  keepIx <- NInd[G>0]
  pop <- pop[keepIx] ## pop with only the ind which contain the QTL of the donor parents on at least one chromosome
  
  mrkrs <- lapply(pop, lapply, function(x) x[5:6,])
  
  percentage <- pgDonPerChr(nchr = NbChr, nbsnp = NbSNPs, ixMrkr = ixMrkr, pop = pop)
  chunks <- lapply(mrkrs, lapply, chunkChr)
  
  pgPerInd <- apply(percentage, 1, mean)
  pgPerChr <- apply(percentage, 2, mean)
  
  nbChunks <- lapply(chunks, lapply, function(x) max(x$totNbChunk))
  nbDonorChunks <- lapply(chunks, lapply, function(x) max(x$donorNbChunk))
  nbChunks <- matrix(unlist(nbChunks), ncol = NbChr, byrow = TRUE)
  nbDonorChunks <- matrix(unlist(nbDonorChunks), ncol = NbChr, byrow = TRUE)
  
  nbChunksPerInd <- apply(nbChunks, 1, mean)
  nbChunksPerChr <- apply(nbChunks, 2, mean)
  nbDonorChunksPerInd <- apply(nbDonorChunks, 1, mean)
  nbDonorChunksPerChr <- apply(nbDonorChunks, 2, mean)
  
  return(list(pgPerChr = pgPerChr, ncPC = nbChunksPerChr, ndcPC = nbDonorChunksPerChr))
  
}







