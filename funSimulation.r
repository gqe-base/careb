## functions for the simulation


resample <- function(x, ...) x[sample.int(length(x), ...)] ## function to be able to sample in x instead of 1:x

readName <- function(name, extension, modeCreation) 
{
  
  ## function which return the name the file containing either the values or the position of the QTLs 
  ## arguments : name = character string : basis of the name
  ##             extension = character string : extension of the name (number of the repetition)
  ##             modeCreation = character string : represents the way of creating the marker values. If "exists", verify that the file exists
  ## return a character string which corresponds to the name of the file
  
  name <- paste(name, extension, ".txt", sep = "")
  if(!file.exists(name) & modeCreation == "exist") {
    cat("The file was not found. Re enter a name.")
    name <- readExtension("Enter the beginning of the path 
                          of the files containing the values and position of QTL")
  }
  return(name)
}


readExtension <- function(quantity) 
{
  
  ## function which asks the user for the generic name of the files containing either the values and the position of the QTLs
  ## but also for the extension in case of having the same initial files (repetition)
  ## arguments : quantity = character string : quantity which file name we want
  ## return a character string representing the name of the quantity
  
  name <- readline(prompt = paste("Enter ", quantity,
                                  "(without quotation marks)")) 
  return(name)
}


## without coupling/repulsion
creationValue <- function(N, NbChromosome, Ms, VG, modeCreationMap, nameGenome, nameEffect, NbAlleles, shapeParameterQTLs, degreeOfDominance, position, snps) 
{
  
  ## function to create a list containing the values of the QTL for each chromosomes of each individuals of the population
  ## either create it or read it from a file
  ## arguments : N  = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             Ms = vector : number of QTL for each chromosome
  ##             VG  = double : genetic variance, for the creation of the gaussian values
  ##             modeCreationMap = character string : method to create (read if already exist) the genetic map, "gaussian" or "exist"
  ##             nameGenome = character string : name of the file where the map will be written or name of the file being read to extract the map
  ##             nameEffect
  ##             NbAlleles = integer : nombre of alleles different for each marker
  ##             shapeParameterQTLs
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs))
  ##             position = list of vector : position of the SNPs and QTLs for each chromosome
  ##             snps = list of matrix/vector : position of the SNPs for each chromosome
  ## return a list of list of matrix (N list of NbChromosome matrix of size 8xNbQTL), each matrix contains the effect, the index, the type (QTL/SNP), the origin (+dominance) of the alleles for each marker
  ## for each chromosomes of each individuals

  generation <- vector("list", N)
  
  if (modeCreationMap == "gaussian") {
    
    
    ## normal distribution of the QTL effects  
    #   AllelesList <- vector("list", NbChromosome)
    #   for(iChr in 1:NbChromosome) {
    #     NbQTL <- Ms[iChr]
    #     AllelesList[[iChr]] <- lapply(NbAlleles[[iChr]], function(x) rnorm(x, 0, sqrt(VG/NbQTL)))
    #   }
    ##
    
    nameEffectAllele <- paste(nameEffect, "Allele.txt", sep = "")
    nameEffectQTL <- paste(nameEffect, "QTL.txt", sep = "")
    
    #shapeParameterQTLs <- 1
    
    print(paste("shape parameter QTL effect", shapeParameterQTLs, sep = " "))
    
    ## gamma distribution of the QTL effects
    AllelesList <- vector("list", NbChromosome)
    for(iChr in 1:NbChromosome) {
      NbQTL <- Ms[iChr]
      QTLeffect <- rgamma(NbQTL, shape = shapeParameterQTLs, scale = sqrt(VG/shapeParameterQTLs)) ## take scale = sqrt(VG/shapeParameterQTLs) as expected variance of a gamma distribution = scale^2 * shape
      ## [cf Zhang and Smith, TAG, 1992] the scale = VG 
      ## scaling of the gamma distribution to unit variance : QTLeffect/sqrt(shapeParameterQTLs * scale * scale) i.e. expected variance
      QTLsign <- sample(c(-1,1), NbQTL, prob = c(0.5,0.5), replace = TRUE) ## sign of the QTL effect : 50% of chance to be positive, 50% negative
      ## if coupling (one parent with positive QTL effect, the other with only negative ones):
      ## P1 : QTLsign <- rep(1, NbQTL)
      ## P2 : QTLsign <- rep(-1, NbQTL)
      ## if repulsion (for each parents alternate between positive and negative QTL effect): 
      ## P1 and P2 : QTLsign <- replicate(ceiling(NbQTL/2), c(-1,1)) ## ceiling in case NbQTL is odd
      ## QTLsign <- QTLsign[1:NbQTL]
      QTLeffect <- QTLsign * QTLeffect
      AllelesList[[iChr]] <- mapply(FUN = function(x,y) runif(x, min = min(0,y), max = max(0,y)), NbAlleles[[iChr]], QTLeffect, SIMPLIFY = FALSE)
      ## write in a file the maximum effect of each QTL ie QTLeffect for all chromosome and each QTL
      ## write in a file the effect of each allele of each QTL for all chromosome
      write.table(t(c(paste("chromosome",iChr, sep = ""), QTLeffect)), file = nameEffectQTL, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
      lapply(AllelesList[[iChr]], function(x) write.table(t(c(paste("chromosome",iChr, sep = ""),unlist(x))), file = nameEffectAllele, append = "TRUE", sep = "\t", row.names = FALSE, col.names = FALSE))
    }
    ##
    
    ## map with gaussian values
    for (i in 1:N) {
      genome <- vector("list", NbChromosome)
      for (iChr in 1:NbChromosome) {
        NbMrkrs <- length(position[[iChr]][1,])
        genome[[iChr]] <- matrix(NA, nrow = 8, ncol = NbMrkrs)
        AllelesIx <- lapply(NbAlleles[[iChr]], function(x) sample(x, 2, replace = TRUE)) ## if want only heterozygous individuals: replace = FALSE
        genome[[iChr]][1:2,which(position[[iChr]][2,] == 1)] <- unlist(mapply(function(x,y) x[y], AllelesList[[iChr]], AllelesIx)) 
        genome[[iChr]][1:2,which(position[[iChr]][2,] == 0)] <- rep(0, 2*length(snps[[iChr]])) 
        genome[[iChr]][3:4,which(position[[iChr]][2,] == 1)] <- AllelesIx
        genome[[iChr]][3:4,which(position[[iChr]][2,] == 0)] <- sample(c(0,1), 2*length(snps[[iChr]]), replace = TRUE) ## SNPs' allele; suppose that the SNPs are biallelic
        genome[[iChr]][5:6,] <- rep(i, 2*NbMrkrs) 
        genome[[iChr]][7,which(position[[iChr]][2,] == 1)] <- degreeOfDominance[iChr,1:Ms[iChr]] 
        genome[[iChr]][7,which(position[[iChr]][2,] == 0)] <- rep(0.5, length(snps[[iChr]]))
        genome[[iChr]][8,] <- position[[iChr]][2,]  
        
        ## for the index to calculate the correlation, try with the haplotype
        ## suppose the snps have two alleles (Q and q)
        ## i.e. 2 if homozygous dominant (QQ)
        ## 1 if heterozygous (Qq)
        ## 0 if homozygous recessive (qq)
        ## for the qtls, like before
#         genome[[iChr]][3:4,which(position[[iChr]][2,] == 1)] <- rep(i, 2*Ms[iChr])
#         genome[[iChr]][3,which(position[[iChr]][2,] == 0)] <- sample(c(0,1,2), length(snps[[iChr]]))
#         genome[[iChr]][4,which(position[[iChr]][2,] == 0)] <- genome[[iChr]][3,which(position[[iChr]][2,] == 0)]
      }
      generation[[i]] <- genome
    }
    return(generation)
  } else  if (modeCreationMap == "exist" & file.exists(nameGenome)) {
    NbMrkrs <- mapply(length, position)/2
    generation <- readValueQTL(nameGenome = nameGenome, N = N, NbChromosome = NbChromosome, NbMarkers = NbMrkrs)
    return(generation)
  } else {
    cat("The option chosen is not valid. Make a new choice.")
    modeCreationMap <- readMode()
    creationValue(N = N, NbChromosome = NbChromosome, Ms = Ms, VG = VG, modeCreationMap = modeCreationMap, nameGenome = nameGenome, nameEffect = nameEffect,
                  NbAlleles = NbAlleles, shapeParameterQTLs = shapeParameterQTLs, degreeOfDominance = degreeOfDominance, position = position, snps = snps)
  }
}


## with coupling /repulsion
creationValueBis <- function(N, NbChromosome, Ms, VG, homozygous, coupling, Lcoupling, modeCreationMap, nameGenome,
                             nameEffect, NbAlleles, shapeParameterQTLs, degreeOfDominance, position, snps) 
{
  
  ## function to create a list containing the values of the QTL for each chromosomes of each individuals of the population
  ## either create it or read it from a file
  ## arguments : N  = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             Ms = vector : number of QTL for each chromosome
  ##             VG  = double : genetic variance, for the creation of the gaussian values
  ##             homozygous
  ##             coupling
  ##             Lcoupling
  ##             modeCreationMap = character string : method to create (read if already exist) the genetic map, "gaussian" or "exist"
  ##             nameGenome = character string : name of the file where the map will be written or name of the file being read to extract the map
  ##             nameEffect
  ##             NbAlleles = integer : nombre of alleles different for each marker
  ##             shapeParameterQTLs
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs))
  ##             position = list of vector : position of the SNPs and QTLs for each chromosome
  ##             snps = list of matrix/vector : position of the SNPs for each chromosome
  ## return a list of list of matrix (N list of NbChromosome matrix of size 8xNbQTL), each matrix contains the effect, the index, the type (QTL/SNP), the origin (+dominance) of the alleles for each markers
  ## for each chromosomes of each individuals
  

  
  generation <- vector("list", N)
  
  if (modeCreationMap == "gaussian") {
    
    nameEffectAllele <- paste(nameEffect, "Allele.txt", sep = "")
    nameEffectQTL <- paste(nameEffect, "QTL.txt", sep = "")
    
    print(paste("shape parameter QTL effect", shapeParameterQTLs, sep = " "))
    
    ## gamma distribution of the QTL effects
    AllelesList <- vector("list", NbChromosome)
    for(iChr in 1:NbChromosome) {
      NbQTL <- Ms[iChr]
      QTLeffect <- rgamma(NbQTL, shape = shapeParameterQTLs, scale = sqrt(VG/shapeParameterQTLs)) ## take scale = sqrt(VG/shapeParameterQTLs) as expected variance of a gamma distribution = scale^2 * shape
      ## [cf Zhang and Smith, TAG, 1992] the scale = VG 
      ## scaling of the gamma distribution to unit variance : QTLeffect/sqrt(shapeParameterQTLs * scale * scale) i.e. expected variance
      AllelesList[[iChr]] <- mapply(FUN = function(x,y) runif(x, min = 0, max = y), NbAlleles[[iChr]], QTLeffect, SIMPLIFY = FALSE)
      write.table(t(c(paste("chromosome",iChr, sep = ""), QTLeffect)), file = nameEffectQTL, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
      lapply(AllelesList[[iChr]], function(x) write.table(t(c(paste("chromosome",iChr, sep = ""),unlist(x))), file = nameEffectAllele, append = "TRUE", sep = "\t", row.names = FALSE, col.names = FALSE))
    }
    ##
    
    ## map with gaussian values
    for (i in 1:N) {
      genome <- vector("list", NbChromosome)
      for (iChr in 1:NbChromosome) {
        NbMrkrs <- length(position[[iChr]][1,])
        genome[[iChr]] <- matrix(NA, nrow = 8, ncol = NbMrkrs)
        QTLsIx <- which(position[[iChr]][2,] == 1)
        SNPsIx <- which(position[[iChr]][2,] == 0)
        if(!homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) sample(x, 2, replace = TRUE)) ## if want only heterozygous individuals: replace = FALSE; 
                                                                                                               ## if want the possibility of having homozygous QTL: replace = TRUE
        if(homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) rep(sample(x, 1),2))
        genome[[iChr]][1:2,QTLsIx] <- unlist(mapply(function(x,y) x[y], AllelesList[[iChr]], AllelesIx)) 
        genome[[iChr]][1:2,SNPsIx] <- rep(0, 2*length(snps[[iChr]])) 
        genome[[iChr]][3:4,QTLsIx] <- unlist(AllelesIx)
        genome[[iChr]][3:4,SNPsIx] <- replicate(length(snps[[iChr]]), sample(c(0,1), 2, replace = FALSE)) ## SNPs' allele; suppose that the SNPs are biallelic
        genome[[iChr]][5:6,] <- rep(i, 2*NbMrkrs) 
        genome[[iChr]][7,QTLsIx] <- degreeOfDominance[iChr,1:Ms[iChr]] 
        genome[[iChr]][7,SNPsIx] <- rep(0.5, length(snps[[iChr]]))
        genome[[iChr]][8,] <- position[[iChr]][2,] 
        
        posQTLs <- position[[iChr]][1,which(position[[iChr]][2,] == 1)]
        deltaX <- posQTLs[-1] - posQTLs[-Ms[iChr]]
        deltaX[deltaX == 0] <- 0.0000001 ## exp(0/0) = NaN, hence when two QTLs are in the same position introduce a dx of 10^-7 (arbitrary) in order to not have the NaN
        P <- 0.5*(1-exp(-deltaX/Lcoupling)) ## the probability that the sign of the QTL effect change/stay the same from the sign of the previous QTL depend from the distance between both QTLs
                                            ## if the two QTLs are far from each other (dx -> inf), then their sign are independant, i.e. P = 0.5
        P <- c(0.5,P) ## for the first QTL, the sign is taken randomly
        
        if(!homozygous) {
          for(iQTL in 1:Ms[iChr]) {
            if(iQTL == 1) QTLsign <- sample(c(1,-1), 1) ## both alleles have same sign
            if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
            if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
            ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same sign
            if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
            ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
            genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]] ## if one allele change its sign, the other change it too
          }
        }
        if(homozygous) {
          ## if homozygous individuals, both effects must have the same sign (else it is heterozygous)
          for(iQTL in 1:Ms[iChr]) {
            if(iQTL == 1) QTLsign <- sample(c(1,-1), 1)
            if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
            if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
            ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same signe
            if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
            ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
            genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]]
          }
        }
      }
      generation[[i]] <- genome
    }
    return(generation)
  } else  if (modeCreationMap == "exist" & file.exists(nameGenome)) {
    NbMrkrs <- mapply(length, position)/2
    generation <- readValueQTL(nameGenome = nameGenome, N = N, NbChromosome = NbChromosome, NbMarkers = NbMrkrs)
    return(generation)
  } else {
    cat("The option chosen is not valid. Make a new choice.")
    modeCreationMap <- readMode()
    creationValue(N = N, NbChromosome = NbChromosome, Ms = Ms, VG = VG, modeCreationMap = modeCreationMap, nameGenome = nameGenome, nameEffect = nameEffect,
                  NbAlleles = NbAlleles, shapeParameterQTLs = shapeParameterQTLs, degreeOfDominance = degreeOfDominance, position = position, snps = snps)
  }
}


## like creationValueBies i.e. with coupling/repulsion BUT
## do not use modeCreationMap
## can take values of QTL both from a file and simulated
creationValueTerNotUsed <- function(N, NbChromosome, NbQTLs, NbSNPs, VG, homozygous, coupling, Lcoupling,
                             nameEffect, nameQTLs, NbAlleles, shapeParameterQTLs, degreeOfDominance, orderPos, position)
{
  ## use vector NbQTLs instead of Ms
  
  generation <- vector("list", N)
  nameEffectAllele <- paste(nameEffect, "Allele.txt", sep = "")
  nameEffectQTLs <- paste(nameEffect, "QTL.txt", sep = "")
  
  print(paste("shape parameter QTL effect", shapeParameterQTLs, sep = " "))
  
  effectQTLs <- matrix(NA, nrow = NbChromosome, ncol = max(NbQTLs))
  
  if(!file.exists(nameQTLs)) {
    NbQTLsFile <- rep(0, NbChromosome)
  }
  
  if(file.exists(nameQTLs)) {
    ## read the effect of the QTL in the file
    effectQTLsFile <- read.table(nameQTLs, header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE)
    effectQTLs[,1:ncol(as.matrix(effectQTLsFile))] <- as.matrix(effectQTLsFile)
    NbQTLsFile <- apply(effectQTLsFile, 1, function(x) length(na.omit(x))) ## need the na.omit because the NA stayed in the object
    # file.remove(nameEffectQTLs) ## in write.table (last lines), append = T, and all QTLs are written : avoid repetition
  }
  
  NbQTLsToDo <- NbQTLs - NbQTLsFile
  
  ## gamma distribution of the QTL effects
  AllelesList <- vector("list", NbChromosome)
  for(iChr in 1:NbChromosome) {
    NbQTL <- NbQTLsToDo[iChr]
    QTLeffect <- rgamma(NbQTL, shape = shapeParameterQTLs, scale = sqrt(VG/shapeParameterQTLs)) ## take scale = sqrt(VG/shapeParameterQTLs) as expected variance of a gamma distribution = scale^2 * shape
    ## [cf Zhang and Smith, TAG, 1992] the scale = VG 
    ## scaling of the gamma distribution to unit variance : QTLeffect/sqrt(shapeParameterQTLs * scale * scale) i.e. expected variance
    if(NbQTL > 0) effectQTLs[iChr,which(is.na(effectQTLs[iChr,]))[1:NbQTL]] <- QTLeffect ## replace the QTL effect not given by the ones simulated
    ## replace only the right nbr of QTL (take the NbQTL first NA to be replace by a value of QTL effect else would have to much QTL)
    ## must scale the simulated QTL effect to not go above a total of 100? (QTL effect = phenotypic variance explained)
    QTLeffect <- effectQTLs[iChr,!is.na(effectQTLs[iChr,])]
    QTLeffect <- QTLeffect[orderPos[[iChr]]]
    AllelesList[[iChr]] <- mapply(FUN = function(x,y) runif(x, min = 0, max = y), NbAlleles[[iChr]], QTLeffect, SIMPLIFY = FALSE)
    write.table(t(c(paste("chromosome",iChr, sep = ""), QTLeffect)), file = nameEffectQTLs, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
    lapply(AllelesList[[iChr]], function(x) write.table(t(c(paste("chromosome",iChr, sep = ""),unlist(x))), file = nameEffectAllele, append = "TRUE", sep = "\t", row.names = FALSE, col.names = FALSE))
  }
  ##
  
  
  for (i in 1:N) {
    genome <- vector("list", NbChromosome)
    NbMrkrs <- NbQTLs + NbSNPs
    for (iChr in 1:NbChromosome) {
      genome[[iChr]] <- matrix(NA, nrow = 8, ncol = NbMrkrs[iChr])
      QTLsIx <- which(position[[iChr]][2,] == 1)
      SNPsIx <- which(position[[iChr]][2,] == 0)
      if(!homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) sample(x, 2, replace = TRUE)) ## if want only heterozygous individuals: replace = FALSE; 
      ## if want the possibility of having homozygous QTL: replace = TRUE
      if(homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) rep(sample(x, 1),2))
      genome[[iChr]][1:2,QTLsIx] <- unlist(mapply(function(x,y) x[y], AllelesList[[iChr]], AllelesIx)) 
      genome[[iChr]][1:2,SNPsIx] <- rep(0, 2*NbSNPs[iChr]) 
      genome[[iChr]][3:4,QTLsIx] <- unlist(AllelesIx)
      genome[[iChr]][3:4,SNPsIx] <- replicate(NbSNPs[iChr], sample(c(0,1), 2, replace = FALSE)) ## SNPs' allele; suppose that the SNPs are biallelic
      genome[[iChr]][5:6,] <- rep(i, 2*NbMrkrs[iChr]) 
      genome[[iChr]][7,QTLsIx] <- degreeOfDominance[iChr,1:NbQTLs[iChr]] 
      genome[[iChr]][7,SNPsIx] <- rep(0.5, NbSNPs[iChr])
      genome[[iChr]][8,] <- position[[iChr]][2,] 
      
      posQTLs <- position[[iChr]][1,QTLsIx]
      deltaX <- posQTLs[-1] - posQTLs[-NbQTLs[iChr]]
      deltaX[deltaX == 0] <- 0.0000001 ## exp(0/0) = NaN, hence when two QTLs are in the same position introduce a dx of 10^-7 (arbitrary) in order to not have the NaN
      P <- 0.5*(1-exp(-deltaX/Lcoupling)) ## the probability that the sign of the QTL effect change/stay the same from the sign of the previous QTL depend from the distance between both QTLs
      ## if the two QTLs are far from each other (dx -> inf), then their sign are independant, i.e. P = 0.5
      P <- c(0.5,P) ## for the first QTL, the sign is taken randomly
      
      if(!homozygous & NbQTLs[iChr]>0) {
        for(iQTL in 1:NbQTLs[iChr]) {
          if(iQTL == 1) QTLsign <- sample(c(1,-1), 1) ## both alleles have same sign
          if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
          if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
          ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same sign
          if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
          ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
          genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]] ## if one allele change its sign, the other change it too
        }
      }
      if(homozygous & NbQTLs[iChr]>0) {
        ## if homozygous individuals, both effects must have the same sign (else it is heterozygous)
        for(iQTL in 1:NbQTLs[iChr]) {
          if(iQTL == 1) QTLsign <- sample(c(1,-1), 1)
          if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
          if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
          ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same signe
          if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
          ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
          genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]]
        }
      }
    }
    generation[[i]] <- genome
  }
  return(generation)
  
}


## like creationValueBies i.e. with coupling/repulsion BUT
## do not use modeCreationMap
## can take values of QTL both from a file and simulated
## compared to TerNotUsed : another way of calculating the allelic effects
creationValueTer <- function(N, NbChromosome, NbQTLs, NbSNPs, VG, homozygous, coupling, Lcoupling,
                             nameEffect, nameQTLs, NbAlleles, shapeParameterQTLs, degreeOfDominance, orderPos, position)
{
  ## use vector NbQTLs instead of Ms
  
  generation <- vector("list", N)
  nameEffectAllele <- paste(nameEffect, "Allele.txt", sep = "")
  nameEffectQTLs <- paste(nameEffect, "QTL.txt", sep = "")
  
  print(paste("shape parameter QTL effect", shapeParameterQTLs, sep = " "))
  
  effectQTLs <- matrix(NA, nrow = NbChromosome, ncol = max(NbQTLs))
  
  if(!file.exists(nameQTLs)) {
    NbQTLsFile <- rep(0, NbChromosome)
  }
  
  if(file.exists(nameQTLs)) {
    ## read the effect of the QTL in the file
    effectQTLsFile <- read.table(nameQTLs, header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE)
    effectQTLs[,1:ncol(as.matrix(effectQTLsFile))] <- as.matrix(effectQTLsFile)
    NbQTLsFile <- apply(effectQTLsFile, 1, function(x) length(na.omit(x))) ## need the na.omit because the NA stayed in the object
    # file.remove(nameEffectQTLs) ## in write.table (last lines), append = T, and all QTLs are written : avoid repetition
  }
  
  NbQTLsToDo <- NbQTLs - NbQTLsFile
  
  ## gamma distribution of the QTL effects
  AllelesList <- vector("list", NbChromosome)
  for(iChr in 1:NbChromosome) {
    NbQTL <- NbQTLsToDo[iChr]
    QTLeffect <- rgamma(NbQTL, shape = shapeParameterQTLs, scale = sqrt(VG/shapeParameterQTLs)) ## take scale = sqrt(VG/shapeParameterQTLs) as expected variance of a gamma distribution = scale^2 * shape
    ## [cf Zhang and Smith, TAG, 1992] the scale = VG 
    ## scaling of the gamma distribution to unit variance : QTLeffect/sqrt(shapeParameterQTLs * scale * scale) i.e. expected variance
    if(NbQTL > 0) effectQTLs[iChr,which(is.na(effectQTLs[iChr,]))[1:NbQTL]] <- QTLeffect ## replace the QTL effect not given by the ones simulated
    ## replace only the right nbr of QTL (take the NbQTL first NA to be replace by a value of QTL effect else would have to much QTL)
    ## must scale the simulated QTL effect to not go above a total of 100? (QTL effect = phenotypic variance explained)
    QTLeffect <- effectQTLs[iChr,!is.na(effectQTLs[iChr,])]
    QTLeffect <- QTLeffect[orderPos[[iChr]]]
    AllelesList[[iChr]] <- mapply(FUN = function(x,y) c(y/2, runif(x-1, min = 0, max = y/2)), NbAlleles[[iChr]], QTLeffect, SIMPLIFY = FALSE)
		## for the allelic effects : the 1st two effects are equal to +-qtleffect/2 then the others are taken in the uniform distribution with these two limits
    write.table(t(c(paste("chromosome",iChr, sep = ""), QTLeffect)), file = nameEffectQTLs, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
    lapply(AllelesList[[iChr]], function(x) write.table(t(c(paste("chromosome",iChr, sep = ""),unlist(x))), file = nameEffectAllele, append = "TRUE", sep = "\t", row.names = FALSE, col.names = FALSE))
  }
  ##
  
  ## in the particular case where I have two homozygous lines in the initial population and I want a1 = a and a2 = -a -> have NbAlleles = 1
	## get a variable QTLsignBiallelic to stock the sign for each chr and qtl used for the 1st indivudal and take the opposite sign in each case for the second individual
	if(homozygous & (N==2)) {
		QTLsignBiallelic <- vector("list", NbChromosome)
		mapply(FUN = function(x,y) rep(NA, NbQTLs[y]), QTLsignBiallelic, 1:NbChromosome, SIMPLIFY = FALSE)
	}
  for (i in 1:N) {
    genome <- vector("list", NbChromosome)
    NbMrkrs <- NbQTLs + NbSNPs
    for (iChr in 1:NbChromosome) {
      genome[[iChr]] <- matrix(NA, nrow = 8, ncol = NbMrkrs[iChr])
      QTLsIx <- which(position[[iChr]][2,] == 1)
      SNPsIx <- which(position[[iChr]][2,] == 0)
      if(!homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) sample(x, 2, replace = TRUE)) ## if want only heterozygous individuals: replace = FALSE; 
      ## if want the possibility of having homozygous QTL: replace = TRUE
      if(homozygous) AllelesIx <- lapply(NbAlleles[[iChr]], function(x) rep(sample(x, 1),2))
      genome[[iChr]][1:2,QTLsIx] <- unlist(mapply(function(x,y) x[y], AllelesList[[iChr]], AllelesIx)) 
      genome[[iChr]][1:2,SNPsIx] <- rep(0, 2*NbSNPs[iChr]) 
      genome[[iChr]][3:4,QTLsIx] <- unlist(AllelesIx)
      genome[[iChr]][3:4,SNPsIx] <- replicate(NbSNPs[iChr], sample(c(0,1), 2, replace = FALSE)) ## SNPs' allele; suppose that the SNPs are biallelic
      genome[[iChr]][5:6,] <- rep(i, 2*NbMrkrs[iChr]) 
      genome[[iChr]][7,QTLsIx] <- degreeOfDominance[iChr,1:NbQTLs[iChr]] 
      genome[[iChr]][7,SNPsIx] <- rep(0.5, NbSNPs[iChr])
      genome[[iChr]][8,] <- position[[iChr]][2,] 
      
      posQTLs <- position[[iChr]][1,QTLsIx]
      deltaX <- posQTLs[-1] - posQTLs[-NbQTLs[iChr]]
      deltaX[deltaX == 0] <- 0.0000001 ## exp(0/0) = NaN, hence when two QTLs are in the same position introduce a dx of 10^-7 (arbitrary) in order to not have the NaN
      P <- 0.5*(1-exp(-deltaX/Lcoupling)) ## the probability that the sign of the QTL effect change/stay the same from the sign of the previous QTL depend from the distance between both QTLs
      ## if the two QTLs are far from each other (dx -> inf), then their sign are independant, i.e. P = 0.5
      P <- c(0.5,P) ## for the first QTL, the sign is taken randomly
      
      if(!homozygous & NbQTLs[iChr]>0) {
        for(iQTL in 1:NbQTLs[iChr]) {
          if(iQTL == 1) QTLsign <- sample(c(1,-1), 1) ## both alleles have same sign
          if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
          if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
          ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same sign
          if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
          ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
          genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]] ## if one allele change its sign, the other change it too
        }
      }
      if(homozygous & NbQTLs[iChr]>0) {
  			## if homozygous individuals, both effects must have the same sign (else it is heterozygous)
        for(iQTL in 1:NbQTLs[iChr]) {
          if(iQTL == 1) QTLsign <- sample(c(1,-1), 1)
          if(iQTL != 1) QTLsign <- sign(genome[[iChr]][1,QTLsIx[iQTL-1]])
          if(coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), -QTLsign, QTLsign) 
          ## if coupling, P is the probability that the sign will change : complete coupling : both QTLs have the same signe
          if(!coupling) QTLsign <- mapply(function(x,y) sample(c(x,y), 1, prob = c(P[iQTL],1-P[iQTL])), QTLsign, -QTLsign)
          ## if repulsion, P is the probability that the sign will stay the same : full repulsion : both QTLs have opposite sign
					if(homozygous & (N==2) & (i==1)) QTLsignBiallelic[[iChr]][iQTL] <- QTLsign ## particular case when there is two lines; 1st ind (give QTL sign
					if(homozygous & (N==2) & (i==2)) QTLsign <- -QTLsignBiallelic[[iChr]][iQTL] ## for the second individual, give the opposite sign of tyhe first one
          genome[[iChr]][1:2,QTLsIx[iQTL]] <- QTLsign * genome[[iChr]][1:2,QTLsIx[iQTL]]
        }
      }
    }
    generation[[i]] <- genome
  }
  return(generation)
  
}



## verify that this function indeed works for the new format of generation
readValueQTL <- function(nameGenome, N, NbChromosome, NbMarkers) 
{
  
  ## function to read a file containing the values of the QTL
  ## the lines of the file represent the individuals
  ## the columns, the different QTL
  ## arguments : N  = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             Ms = vector : number of QTL for each chromosome
  ##             nameGenome = character string : name of the file where the map will be written or name of the file being read to extract the map
  ## return a list of list of matrix (N list of NbChromosome matrix of size 5xNbQTL), each matrix contains the effect and the origin (+dominance) of the alleles for each marker
  ## for each chromosomes of each individuals
  
  gmat <- read.table(file = nameGenome, sep = "\t")
  generation <- vector("list", N)
  for (i in 1:N) {
    generation[[i]] <- vector("list", NbChromosome)
    indexChr <- 0
    for (j in 1:NbChromosome) {
      generation[[i]][[j]] <- matrix(as.numeric(gmat[i, (indexChr+1):(indexChr+NbMarkers[j]*6)]), nrow = 6)
      indexChr <- indexChr + NbMarkers[j] * 6
    }
  }
  return(generation)
}


## verify that it will works
readPosition <- function(namePosition,NbChromosome)
{
  
  ## function to read a file containing the positions of the QTL
  ## the columns represent the different markers
  ## arguments : NbChromosome = integer : number of chromosomes
  ##             namePosition = character string : name of the file where the map will be written or name of the file being read to extract the map
  ## return a list of vector (NbChromosome vector of size NbQTL) which contains the position (in cM) of each marker
  
  positionTot <- read.table(namePosition, sep = "\t", header = FALSE, fill = TRUE)
  position <- list()
  for (i in 1:NbChromosome) {
    position[[i]] <- as.numeric(positionTot[i,!is.na(positionTot[i,])])  ## remove the NA introduced by fill = TRUE of read.table
  }
  return(position)
}


creationPosition <- function(modeCreationPosition, namePositionSNPs, namePositionQTLs, NbChromosome, increaseLG, shapeParameterQTLs, NbSNPs, NbQTLs, geneticLength, positionQTLs)
{
  ## function to create or read the position of the SNPs and QTLs
  ## arguments : modeCreationPosition = character string : "gaussian", "existGaussian" or "exist"
  ##             namePositionSNPs = character string : name of the file containing the position of the SNPs
  ##             namePositionQTLs = character string : name of the file containing the position of the QTLs
  ##             NbChromosome = integer : number of chromosome
  ##             increaseLG
  ##             shapeParameterQTLs
  ##             NbSNPs = vector : number of SNPs for each chromosome
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             positionQTLs = list (NbChromosome element) of matrix (2 x NbQTLs) : contains the position of the QTLs for each chromosome; the second line contains ones (code if the locus we are looking at is a SNP(0) or a QTL(1))
  ## returns a list of vector containing the position of the SNPs and of the QTLs for all chromosomes
  
  ## namePositionSNPs <- CSV file with a column for the chromosome (LG as linkage group) and a column with the position in cM
  if(modeCreationPosition == "gaussian") {
    # shapeParameterQTLs <- 0.1 ## shape of the gamma distribution : affect the repartition of QTL (clusters, random positions, constant distances)
    shapeParameterSNPs <- 10 ## shape of the distribution (gamma) of the SNPs position : SNPs uniformly placed along the chromosomes
    
    print(paste("shape parameter QTL position", shapeParameterQTLs, sep = " "))
    
    position <- vector("list", NbChromosome)
    NbMarkers <- NbSNPs + NbQTLs
    
    ## considering clustering
    for (iChr in 1:NbChromosome) {
      position[[iChr]] <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
      pos <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
      posSNPs <- matrix(NA, ncol = NbSNPs[iChr], nrow = 2)
      posQTLs <- matrix(NA, ncol = NbQTLs[iChr], nrow = 2)
      posSNPs[2,] <- rep(0, NbSNPs[iChr])
      posQTLs[2,] <- rep(1, NbQTLs[iChr])
      
      ## the SNPs (neutral markers) are located regularly along the chromosome
      ## hence the shape parameter of the gamma distribution > 1 (if SNPs placed randomly, = 1)
      
      distSNPs <- round(rgamma(NbSNPs[iChr], shape = shapeParameterSNPs, rate = (NbSNPs[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
      ## rate = (NbSNPs[iChr]*10)/geneticLength[iChr]), in order to have the right genetic length of the chromosome
      while(sum(distSNPs) > geneticLength[iChr]) {
        distSNPs <- round(rgamma(NbSNPs[iChr], shape = shapeParameterSNPs, rate = (NbSNPs[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
      }
      posSNPs[1,] <- cumsum(distSNPs)
      
      ## the QTLs follow an L-shaped distribution : clustering
      distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLs[iChr]), shape = shapeParameterQTLs, rate = NbQTLs[iChr]*shapeParameterQTLs/geneticLength[iChr]))
      distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
      while(length(distQTLs) != NbQTLs[iChr]) {
        distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLs[iChr]), shape = shapeParameterQTLs, rate = NbQTLs[iChr]*shapeParameterQTLs/geneticLength[iChr]))
        distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
      }
      ##
      
      
      ## normal distribution of the QTLs position
#       NbQTL <- NbQTLs[iChr]
#       distQTLs <- 1:NbQTL
#       distQTLs <- (distQTLs-0.5)*geneticLength[iChr]/NbQTL
#       distQTLs <- unique(distQTLs)
#       distQTLs <- distQTLs[order(distQTLs)]
      ##
      
       
      posQTLs[1,] <- round(distQTLs,3)
      
      pos <- cbind(posQTLs, posSNPs)
      pos <- pos[,order(pos[1,])]
      position[[iChr]] <- pos
    }
    
    write.table(t(unlist(lapply(position, function(x) x[1,which(x[2,] == 1)]))), namePositionQTLs, sep = "\t", col.names = FALSE, row.names = FALSE)
    write.table(t(unlist(lapply(position, function(x) x[1,which(x[2,] == 0)]))), namePositionSNPs, sep = "\t", col.names = FALSE, row.names = FALSE)
    return(position)
  } 
  
  if(modeCreationPosition == "exist") {
    snp <- read.table((namePositionSNPs), stringsAsFactors = FALSE, header = TRUE)
    snp$LG <- as.numeric(unlist(lapply(strsplit(snp$LG, "G", fixed = TRUE), "[[", 2)))
    ## 0 = markers, 1 = QTLs
    ## transform the vector with chromosome and position into a list of NbChromosome element, each element containing the position of the mrkrs
    snps <- vector("list", max(snp$LG))
    for(i in 1:NbChromosome) {
      nbSnps <- length(which(snp$LG == i))
      snps[[i]] <- matrix(c(snp$Position..cM.[which(snp$LG == i)], rep(0,nbSnps)), ncol = nbSnps, nrow = 2, byrow = TRUE)
    }
    position <- mapply(cbind, positionQTLs, snps, SIMPLIFY = FALSE)
    position <- mapply(function(x) x[,order(x[1,])], position)
    
    position <- mapply(function(x) if(length(x) > 0) matrix(c(increaseLG*x[1,],x[2,]), nrow = 2, byrow = TRUE), position) ## if want to increase the original genetic length 
                                                                                                                          ## by a factor increaseLG : multiply all the positions by increase LG
    
    nameFile <- strsplit(namePositionQTLs, "QTLs.txt")[[1]]
    namePositionSNPs <- paste(nameFile, "SNPs.txt", sep = "")
    write.table(t(unlist(mapply(function(x) x[1,which(x[2,] == 1)], position))), namePositionQTLs, sep = "\t", col.names = FALSE, row.names = FALSE)
    write.table(t(unlist(mapply(function(x) x[1,which(x[2,] == 0)], position))), namePositionSNPs, sep = "\t", col.names = FALSE, row.names = FALSE)
    return(position)
  }
  
  if(modeCreationPosition == "existGaussian") { 
    ## if it is a repeat, use the position created in the first repeat (created via gaussian)
    posQTLs <- read.table(namePositionQTLs, header = FALSE, sep = "\t")
    posSNPs <- read.table(namePositionSNPs, header = FALSE, sep = "\t")
    posQTLs <- as.numeric(unlist(posQTLs))
    posSNPs <- as.numeric(unlist(posSNPs))
    positionQTLs <- vector("list", NbChromosome)
    positionSNPs <- vector("list", NbChromosome)
    indexQTLs <- 0
    indexSNPs <- 0

    for(iChr in 1:NbChromosome) {
      if(NbQTLs[iChr] > 0){
        positionQTLs[[iChr]] <- posQTLs[(indexQTLs+1):(indexQTLs+NbQTLs[iChr])]
        positionQTLs[[iChr]] <- rbind(positionQTLs[[iChr]], 1)
        indexQTLs <- indexQTLs + NbQTLs[iChr] 
      }
      positionSNPs[[iChr]] <- posSNPs[(indexSNPs+1):(indexSNPs+NbSNPs[iChr])]
      positionSNPs[[iChr]] <- rbind(positionSNPs[[iChr]], 0)
      indexSNPs <- indexSNPs + NbSNPs[iChr] 
    }

    position <- mapply(cbind, positionQTLs, positionSNPs, SIMPLIFY = FALSE)
    position <- lapply(position, function(x) x[,order(x[1,])])
    return(position)
  }
}



## difference with creationPosition :
## do not need the parameter modeCreationPosition
## it can take QTL ans SNP both from a file and simulated at the same time
creationPositionBis <- function(namePositionSNPs, namePositionQTLs, NbChromosome, shapeParameterQTLs, NbSNPs, NbQTLs, geneticLength)
{
  ## function to create or read the position of the SNPs and QTLs
  ## arguments : namePositionSNPs = character string : name of the file containing the position of the SNPs
  ##             namePositionQTLs = character string : name of the file containing the position of the QTLs
  ##             NbChromosome = integer : number of chromosome
  ##             shapeParameterQTLs
  ##             NbSNPs = vector : number of SNPs for each chromosome
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ## returns a list of vector containing the position of the SNPs and of the QTLs for all chromosomes
  
  ## if the files containing the position of the QTLs and SNPs do not already exist
  ## there is no QTL/SNP already defined
  if(!file.exists(namePositionQTLs)) {NbQTLsFile <- rep(0, NbChromosome); posQTLsFile <- NULL}
  if(!file.exists(namePositionSNPs)) {NbSNPsFile <- rep(0, NbChromosome); posSNPsFile <- NULL}
  
  ## if the files exist
  ## save the position and count the number of QTLs/SNPs in it
  ## format of the file : each line represents a chromosome -> number of markers = length of the line
  if(file.exists(namePositionQTLs)) {
    posQTLsFile <- read.table(namePositionQTLs, header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE) ## format of posQTL = list of NbChromosome elements
    ## blank.lines.skip = F because if there is no QTL then there will be a blank line -> want to take it into account
    posQTLsFile <- apply(posQTLsFile, 1, list)
    posQTLsFile <- lapply(posQTLsFile, unlist)
    posQTLsFile <- lapply(posQTLsFile, rbind, 1)
    posQTLsFile <- lapply(posQTLsFile, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
    NbQTLsFile <- sapply(posQTLsFile, function(x) length(na.omit(x[1,]))) ## need the na.omit because the NA stayed in the object
    file.remove(namePositionQTLs) ## in write.table (last lines), append = T, and all QTLs are written : avoid repetition
  }
  if(file.exists(namePositionSNPs)) {
    posSNPsFile <- read.table(namePositionSNPs, header = FALSE, stringsAsFactors = FALSE, sep = ",", blank.lines.skip = FALSE) ## format of posSNP = list of NbChromosome elements
    posSNPsFile <- apply(posSNPsFile, 1, list)
    posSNPsFile <- lapply(posSNPsFile, unlist)
    posSNPsFile <- lapply(posSNPsFile, rbind, 0)
    posSNPsFile <- lapply(posSNPsFile, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
    NbSNPsFile <- sapply(posSNPsFile, function(x) length(na.omit(x[1,]))) ## need the na.omit because the NA stayed in the object
    file.remove(namePositionSNPs)
  }
  
  NbQTLsToDo <- NbQTLs - NbQTLsFile
  NbSNPsToDo <- NbSNPs - NbSNPsFile
  position <- vector("list", NbChromosome)
  orderPos <- vector("list", NbChromosome) ## index of the markers when their positions are ordered
  NbMarkers <- NbSNPs + NbQTLs
  # shapeParameterQTLs <- 0.1 ## shape of the gamma distribution : affect the repartition of QTL (clusters, random positions, constant distances)
  shapeParameterSNPs <- 10 ## shape of the distribution (gamma) of the SNPs position : SNPs uniformly placed along the chromosomes
  print(paste("shape parameter QTL position", shapeParameterQTLs, sep = " "))
  
  for (iChr in 1:NbChromosome) {
    position[[iChr]] <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
    pos <- matrix(NA, ncol = NbMarkers[iChr], nrow = 2)
    posSNPs <- matrix(NA, ncol = NbSNPsToDo[iChr], nrow = 2)
    posQTLs <- matrix(NA, ncol = NbQTLsToDo[iChr], nrow = 2)
    posSNPs[2,] <- rep(0, NbSNPsToDo[iChr])
    posQTLs[2,] <- rep(1, NbQTLsToDo[iChr])
    
    ## the SNPs (neutral markers) are located regularly along the chromosome
    ## hence the shape parameter of the gamma distribution > 1 (if SNPs placed randomly, = 1)
    distSNPs <- round(rgamma(NbSNPsToDo[iChr], shape = shapeParameterSNPs, rate = (NbSNPsToDo[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
    ## rate = (NbSNPs[iChr]*10)/geneticLength[iChr]), in order to have the right genetic length of the chromosome
    while(sum(distSNPs) > geneticLength[iChr]) {
      distSNPs <- round(rgamma(NbSNPsToDo[iChr], shape = shapeParameterSNPs, rate = (NbSNPsToDo[iChr]*shapeParameterSNPs)/geneticLength[iChr]),3)
    }
    posSNPs[1,] <- cumsum(distSNPs)
    ## the QTLs follow an L-shaped distribution : clustering
    distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLsToDo[iChr]), shape = shapeParameterQTLs, rate = NbQTLsToDo[iChr]*shapeParameterQTLs/geneticLength[iChr]))
    distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
    while(length(distQTLs) != NbQTLsToDo[iChr]) {
      distQTLs <- cumsum(rgamma(10*(geneticLength[iChr]+NbQTLsToDo[iChr]), shape = shapeParameterQTLs, rate = NbQTLsToDo[iChr]*shapeParameterQTLs/geneticLength[iChr]))
      distQTLs <- distQTLs[distQTLs>9*geneticLength[iChr] & distQTLs<10*geneticLength[iChr]] - 9*geneticLength[iChr]
    }
    ##
    ## normal distribution of the QTLs position
    #       NbQTL <- NbQTLs[iChr]
    #       distQTLs <- 1:NbQTL
    #       distQTLs <- (distQTLs-0.5)*geneticLength[iChr]/NbQTL
    #       distQTLs <- unique(distQTLs)
    #       distQTLs <- distQTLs[order(distQTLs)]
    ##
    posQTLs[1,] <- round(distQTLs,3)
    pos <- cbind(posQTLsFile[[iChr]], posQTLs, posSNPsFile[[iChr]], posSNPs)
    orderPos[[iChr]] <- order(pos[1,pos[2,] == 1])
    pos <- pos[,order(pos[1,])]
    position[[iChr]] <- pos
    posQTLs <- matrix(NA, nrow = 1, ncol = max(NbQTLs))
    if(NbQTLs[iChr] > 0) posQTLs[,1:NbQTLs[iChr]] <- unlist(position[[iChr]][1,which(position[[iChr]][2,] == 1)])
    posSNPs <- matrix(NA, nrow = 1, ncol = max(NbSNPs))
    if(NbSNPs[iChr] > 0) posSNPs[,1:NbSNPs[iChr]] <- unlist(position[[iChr]][1,which(position[[iChr]][2,] == 0)])
    write.table(posQTLs, namePositionQTLs, sep = ",", append = TRUE, col.names = FALSE, row.names = FALSE)
    write.table(posSNPs, namePositionSNPs, sep = ",", append = TRUE, col.names = FALSE, row.names = FALSE)
  }
  
  return(list(position = position, orderPos = orderPos))
} 


crossMatrix2 <- function(Nc, N, reproductionType)
{
  ## function to create the matrix of cross
  ## parameters : Nc = integer : number of cross wanted
  ##              N = integer : number of parents (selected)
  ##              reproductionType
  ## return a matrix of size Ns x Ns. 
  ## The number inside represents the number of time the cross between the two parents will happen
  ## there is no distinction between males and females
  
  if (reproductionType == "SC") {
    MC <- matrix(0, nrow = N, ncol = N)
    if(Nc < N*(N-1)/2) {
      vecC <- 2:N
      ixL <- N-1
      vecL <- ixL:1
      vecM <- which(upper.tri(MC))
      vecCol <- NULL
      vecLine <- NULL
      for(i in 1:Nc) {
        sampling <- unlist(sapply(vecM, function(x) which(x == vecL[1] + (vecC-1)*N & vecC > vecL[1])))
        jC <- resample(sampling, 1)
        vecCol <- c(vecCol, vecC[jC])
        vecLine <- c(vecLine, vecL[1])
        vecM <- vecM[-which(vecM == vecL[1] + (vecC[jC]-1)*N)]
        vecC <- vecC[-jC]
        vecL <- vecL[-1]
        if(length(vecC) == 0) vecC <- rep(2:N,2*Nc/N)
        if(length(vecL) == 0 & ixL > 1) {ixL <- ixL-1; vecL <- ixL:1}
      }
      MC <- table(factor(vecLine, levels = 1:N),factor(vecCol, levels = 1:N))
    }
    if(Nc >= N*(N-1)/2) {
      m <- which(upper.tri(MC))
      mp <- sample(m, Nc-N*(N-1)/2)
      MC[m] <- 1
      MC[mp] <- MC[mp] + 1
    }
    return(MC)
  }
  if (reproductionType == "HD" | reproductionType == "SP") {
    if(Nc <= N) MC <- resample(1:N, Nc)
    if(Nc > N) MC <- c(resample(1:N), resample(1:N, Nc-N, replace = TRUE))
    return(MC)
  }
}


indexParents <- function(crossMat, reproductionType)
{
  ## function to create the index of parents which will be used to create the newt generation
  ## using the matrix of cross crossMat
  ## arguments : crossMat
  ##             reproductionType
  ## return a matrix NbCross x 2 (panmixia) or a vector NbCross (HD or selfing)
  if (reproductionType == "SC") {
    ixParents <- mapply(function(x,y) rep(c(x,y), crossMat[x,y]), row(crossMat), col(crossMat))
    ixParents <- matrix(unlist(ixParents), nrow = 2)
    return(ixParents)
  }
  if (reproductionType == "HD" | reproductionType == "SP") return(crossMat)
}


verifOneCross <- function(Ns, matCross)
{
  ## function to check that all the crosses possible have been done
  ## before doing some crosses again
  ## if that is not the case, remove some crosses which have been done too much
  ## en replace them by crosses which have not been done yet
  ## arguments : Ns
  ##             matCross
  ## return the matrix of crosses
  toCross <- sum(matCross == 0) - Ns
  toRemove <- sum(matCross > 1)
  toRemove <- min(toRemove, toCross)
  removal <- resample(which(matCross > 1), toRemove)
  matCross[removal] <- matCross[removal] - 1
  mCrossp <- which(matCross == 0)
  diagonal <- 1+(0:(Ns-1))*(Ns+1)
  mCrosspp <- sapply(diagonal, function(x) which(x == mCrossp))
  mCrossp <- mCrossp[-mCrosspp]
  cross <- resample(mCrossp, toRemove)
  matCross[cross] <- matCross[cross] + 1 
  return(matCross)
}


crossMatrix <- function(Nc, Ns, reproductionType) 
{
  ## function to create the matrix of cross
  ## parameters : Nc = integer : number of cross wanted
  ##              Ns = integer : number of parents (selected)
  ## return a matrix of size Ns x Ns. 
  ## The number inside represents the number of time the cross between the two parents will happen
  ## distinction between males and females
  
  if (reproductionType == "SC") {
    matCross <- matrix(0, ncol = Ns, nrow = Ns)
    
    if(Nc <= Ns/2) {
      ## case Nc <= Ns/2
      ## no selfing possible in case of panmixia (want to use all the parents at least once)
      ## if want only selfing : ix1 = ix2
      ix1 <- resample(1:Ns, Nc)
      sampix2 <- 1:Ns
      sampix2 <- sampix2[-sapply(ix1, function(x) which(sampix2 == x))]
      ix2 <- resample(sampix2, Nc)
      matCross <- table(factor(ix1, levels = 1:Ns),factor(ix2, levels = 1:Ns))
      while(sum(matCross > 1) != 0  &  sum(matCross == 0) > Ns) matCross <- verifOneCross(Ns = Ns, matCross = matCross)
    }
    
    if(Nc > Ns/2 & Nc < Ns) {
      ## case Ns/2 <= Nc < Ns
      ## no selfing possible
      ## selfing : ix1 = ix2
      ## if selfing allowed : resample(x:Ns, 1)
      ## or ix1 <- replicate(2, resample(1:Ns, Nc)) 
      ix1 <- resample(1:(Ns-1), Nc)
      sampix2 <- 1:Ns
      sampix2 <- sampix2[-sapply(ix1, function(x) which(sampix2 == x))]
      ixp1 <- ix1[-(1:length(sampix2))]
      ix2 <- c(resample(sampix2), sapply(ixp1, function(x) resample((x+1):Ns, 1)))
      matCross <- table(factor(ix1, levels = 1:Ns),factor(ix2, levels = 1:Ns))
      while(sum(matCross > 1) != 0  &  sum(matCross == 0) > Ns) matCross <- verifOneCross(Ns = Ns, matCross = matCross)
    }
    
    if(Nc >= Ns) {
      ## case Nc >= Ns
      ens <- 1:Ns
      ix1 <- c(resample(1:(Ns)), resample(1:(Ns-1), Nc-Ns, replace = TRUE))
      ix2 <- sapply(ix1, function(x) resample(ens[-which(ens == x)], 1))
      matCross <- table(factor(ix1, levels = 1:Ns),factor(ix2, levels = 1:Ns))
      while(sum(matCross > 1) != 0  &  sum(matCross == 0) > Ns) matCross <- verifOneCross(Ns = Ns, matCross = matCross)
    }
    return(matCross)
  }
  if (reproductionType == "HD" | reproductionType == "SP") {
    if(Nc <= Ns) matCross <- resample(1:Ns, Nc)
    if(Nc > Ns) matCross <- c(resample(1:Ns), resample(1:Ns, Nc-Ns, replace = TRUE))
    return(matCross)
  }
}

