## functions for the analysis

library(gplots)
library(RColorBrewer)

calculQTLMatrix <- function(N, NbChromosome, row1, row2, NbQTLs, generation)
{
  
  ## put all the individuals in a matrix (row = one chromosome of one individual, column = one marker)
  ## can put either the effect of the alleles (row = 1, 2) or the index of the alleles (row = 3,4) or the origins of the alleles (row = 5, 6)
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             row1 = integer (1 or 3 or 5) : 1 if keep the effects of the alleles, 3 if keep the index of the alleles, 5 if keep the origin of the alleles
  ##             row2 = integer (2 or 4 or 6) : 2 if keep the effects of the alleles, 4 if keep the index of the alleles, 6 if keep the origin of the alleles
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a list of matrix (NbChromosome matrix)
  ## each matrix is of size 2N x NbQTL (for the chromosome considered)
  
  ## keep only the QTLs
  
  Mij <- vector("list", NbChromosome)
  
  for (i in 1:NbChromosome) {
    Mij[[i]] <- matrix(NA, ncol = NbQTLs[i], nrow = 2*N)
    for (NbrInd in 1:N) {
      Mij[[i]][NbrInd, ] <- generation[[NbrInd]][[i]][row1,which(generation[[NbrInd]][[i]][8,] == 1)]
      Mij[[i]][NbrInd+N, ] <- generation[[NbrInd]][[i]][row2,which(generation[[NbrInd]][[i]][8,] == 1)]
    }
  }
  
  return(Mij)
}


calculMarkersMatrix <- function(N, NbChromosome, row1, row2, NbMarkers, generation)
{
  
  ## put all the individuals in a matrix (row = one chromosome of one individual, column = one marker)
  ## can put either the effect of the alleles (row = 1, 2) or the origins of the alleles (row = 3, 4)
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             row1 = integer (1 or 3) : 1 if keep the effects of the alleles, 3 if keep the origin of the alleles
  ##             row2 = integer (2 or 4) : 2 if keep the effects of the alleles, 4 if keep the origin of the alleles
  ##             NbMarkers = vector : number of markers (QTLs and SNPs) for each chromosome
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a list of matrix (NbChromosome matrix)
  ## each matrix is of size 2N x NbQTL (for the chromosome considered)
  
  ## keep QTLs and SNPs
  
  Mij <- vector("list", NbChromosome)

  for (i in 1:NbChromosome) {
    Mij[[i]] <- matrix(NA, ncol = NbMarkers[i], nrow = 2*N)
    for (NbrInd in 1:N) {
      Mij[[i]][NbrInd, ] <- generation[[NbrInd]][[i]][row1,]
      Mij[[i]][NbrInd+N, ] <- generation[[NbrInd]][[i]][row2,]
    }
  }
  
  return(Mij)
}


calculCorrelationMatrix <- function(Chr, g, nameCor, nameCorMean, repet, position, Mij) ## Mij12
{
  
  ## calculation of the correlation matrix (correlation between the QTLs) for one chromosome
  ## arguments : Chr = integer : number of the chromosome which the correlation is calculated for
  ##             g = integer : number of the generation
  ##             nameCor = string of character : name of the file where the correlation matrix will be written
  ##             nameCorMean = string of character : name of the file where the mean of the correlation matrix will be written
  ##             repet = integer : number of the rep
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Mij = list of matrix : matrix contains the effect of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write the correlation matrix in a file
  
  ## tryCatch : test if there is an error when try to plot the correlation
  ## if there are errors,  continue to run the script anyway 
  ## (if no error handling, the script would stop)
  
  nameCor <- paste(nameCor[Chr], "g", g, ".txt", sep = "")
  pos <- position[[Chr]]
  Mij <- Mij[[Chr]]
  Cor <- matrix(NA, nrow = length(pos), ncol = length(pos))
  
  tryCatch({Cor <- cor(Mij) ## calcul of the correlation
  
  CorOffDiagonal <- Cor
  diag(CorOffDiagonal) <- NA
  CorMoy <- mean(as.vector(CorOffDiagonal),na.rm = TRUE)
  
  write.table(Cor, file = nameCor, sep = "\t")
  vec <- NULL
  for (i in 1:(length(pos)-1)) {
    vec <- c(vec, CorOffDiagonal[i, i+1])
  }
  # write.table(t(c(repet, g, Chr, CorMoy, mean(vec))), file = nameCorMean, append = TRUE, col.names = FALSE, row.names = FALSE, sep = "\t")
  }, error = function(e) {})
}


calculCorrelationMatrixAllChr <- function(N, NbChromosome, g, nameExtended, nameCorMean, repet, position, Mij) ## Mij12
{
  
  ## calculation of the correlation matrix (correlation between the QTLs) for all chromosomes
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             g = integer : number of the generation
  ##             nameExtended = string of character : basis of the name of the file where the correlation matrix will be written
  ##             nameCorMean = string of character : name of the file where the mean of the correlation matrix will be written
  ##             repet = integer : number of the rep
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Mij = list of matrix : matrix contains the effect of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write the correlation matrix in a file
  
  ## calculate the correlation inter/intra chromosome
  ## tryCatch : test if there is an error when try to plot the correlation
  ## if there are errors,  continue to run the script anyway 
  ## (if no error handling, the script would stop)
  
  nameCor <- paste(nameExtended, "Corg", g, ".txt", sep = "")
  
  sumPos <- mapply(length, position)
  lengthTot <- sum(sumPos)
  MMij <- matrix(NA, ncol = NbQTL, nrow = 2*N)
  
#   for (iChr in 1:NbChromosome) {
#     MMij <- cbind(MMij, Mij[[iChr]])
#   }
  
  MMij <- matrix(unlist(Mij), nrow = 2*N)
  
  Cor <- matrix(NA, nrow = lengthTot, ncol = lengthTot)
  
  tryCatch({Cor <- cor(MMij) ## calcul of the correlation
  
  #   plotCorrelationMatrix(Cor)
  
  CorOffDiagonal <- Cor
  diag(CorOffDiagonal) <- NA
  CorMoy <- mean(as.vector(CorOffDiagonal),na.rm = TRUE)
  
  write.table(Cor, file = nameCor, sep = "\t")
  vec <- NULL
  for (i in 1:(length(pos)-1)) {
    vec <- c(vec, CorOffDiagonal[i, i+1])
  }
  write.table(t(c(repet, g, NbChromosome, CorMoy, mean(vec))), file = nameCorMean, append = TRUE, col.names = FALSE, row.names = FALSE, sep = "\t")
  }, error = function(e) {})
}


plotCorrelationMatrix <- function(Cor)
{
  
  ## plot of the correlation matrix (correlation between the QTLs) for one chromosomes
  ## arguments : Cor = matrix : correlation matrix 
  ## return nothing
  ## plot the correlation matrix
  
  CorOffDiagonal <- Cor
  diag(CorOffDiagonal) <- NA
  CorMoy <- mean(as.vector(CorOffDiagonal),na.rm = TRUE)
  CorVar <- var(as.vector(CorOffDiagonal),na.rm = TRUE)
  
  ## plot of the correlation between loci
  
  #   xlab <- rep("", nrow(CorOffDiagonal))  
  #   xlab[seq(10,nrow(CorOffDiagonal), 10*sign(nrow(CorOffDiagonal)-10))] <- seq(10, nrow(CorOffDiagonal), 10*sign(nrow(CorOffDiagonal)-10))  
  #   rownames(CorOffDiagonal) <- xlab 
  #   colnames(CorOffDiagonal) <- xlab
  
  maxCor <- CorMoy+1.96*sqrt(CorVar)
  minCor <- CorMoy-1.96*sqrt(CorVar)
  
  corSat <- CorOffDiagonal
  corSat[corSat > maxCor] <- -1/0
  ## color definition for the map
  myBreaks <- seq(from = minCor, to = maxCor,by = (maxCor-minCor)/10) 
  # myBreaks <- seq(from = -0.5, to = 0.5, by = 0.05) 
  myCol <- rev(rainbow((length(myBreaks)-1), start = 0, end = 0.7))
  heatmap.2(CorOffDiagonal, dendrogram = "none", Rowv = CorOffDiagonal[,1], Colv = CorOffDiagonal[1,], trace = "none", density.info = "none", 
            scale = "none", key = T, col = myCol, breaks = myBreaks, na.color = "black", srtCol = 0, symkey = FALSE)
}


plotCorrelationMatrixAllChr <- function(NbChromosome, position, Cor)
{
  
  ## plot of the correlation matrix (correlation between the QTLs) for all chromosomes
  ## put a white separation between chromosomes
  ## arguments : NbChromosome
  ##             Cor = matrix : correlation matrix 
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ## return nothing
  ## plot the correlation matrix
  
  
  sep <- 0
  sepVect <- NULL
  
  for (iChr in 1:NbChromosome) {
    pos <- unlist(position[[iChr]])
    pos <- pos[order(pos)]
    pos <- unique(pos)
    sep <- sep+length(pos)
    sepVect <- c(sepVect, sep)
  }
  
  CorOffDiagonal <- Cor
  diag(CorOffDiagonal) <- NA
  CorMoy <- mean(as.vector(CorOffDiagonal),na.rm = TRUE)
  CorVar <- var(as.vector(CorOffDiagonal),na.rm = TRUE)
  
  ## plot of the correlation between loci
  
  xlab <- rep("", nrow(CorOffDiagonal))  
  xlab[seq(10,nrow(CorOffDiagonal), 10)] <- seq(10, nrow(CorOffDiagonal), 10)  
  rownames(CorOffDiagonal) <- xlab 
  colnames(CorOffDiagonal) <- xlab
  
  maxCor <- CorMoy+1.96*sqrt(CorVar)
  minCor <- CorMoy-1.96*sqrt(CorVar)
  
  corSat <- CorOffDiagonal
  corSat[corSat > maxCor] <- -1/0
  ## color definition for the map
  myBreaks <- seq(from = minCor, to = maxCor,by = (maxCor-minCor)/10) 
  # myBreaks <- seq(from = -1, to = 1, by = 0.01) 
  myCol <- rev(rainbow((length(myBreaks)-1), start = 0, end = 0.7))
  heatmap.2(CorOffDiagonal, dendrogram = "none", Rowv = CorOffDiagonal[,1], Colv = CorOffDiagonal[1,], trace = "none", density.info = "none", 
            scale = "none", key = T, col = myCol, breaks = myBreaks, na.color = "black", srtCol = 0, symkey = TRUE, colsep = sepVect, rowsep = sepVect, sepcolor = "white")
}


readCorrelationMatrix <- function(nameCorz)
{
  
  ## read a correlation matrix (correlation between the QTLs)
  ## arguments : nameCorz = string of character : name of the file where the correlation matrix is written
  ## return the correlation matrix
  
  ## use  this function if we write all of the correlation matrix in the same file
  #   CorTot <- read.table(nameCorz, header = TRUE, sep = "\t")
  #   Cor <- list()
  #   NbLocus <- dim(CorTot)[2] ## dim(Cor)[2] = number of columns = NbLocus
  #   NbMatrix <- dim(CorTot)[1] / NbLocus ## dim(CorTot)[1] = number of rows => it's the number of correlation matrix times the number of QTL
  #   indexCor <- 0
  #   for (i in 1:NbMatrix) {  
  #     Cor[[i]] <- matrix(unlist(CorTot[(indexCor + 1):(indexCor + NbLocus), ]), ncol = NbLocus, nrow = NbLocus)
  #     indexCor <- indexCor + NbLocus
  #   }
  #   return(Cor)
  ## if there is only one correlation matrix per file
  
  CorTot <- read.table(nameCorz, header = TRUE, sep = "\t")
  return(as.matrix(CorTot))
}


## to modify (after modification of the format of generation)
plotSignal <- function(N, NbChromosome, g, geneticLength, nameSignalPlot, position, generation)
{
  
  ## plot the signal (effect of the alleles) along the chromosomes, in a file
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             g = integer : number of the generation
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             nameSignalPlot = string of character : name of the file where the plot will be
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return nothing
  ## plot in a file
  
  pdf(paste(nameSignalPlot, g, ".pdf", sep = ""))
  ## when the new generation is created
  ## plot of the signal along the chromosomes
  for(NbrInd in 1:N) {
    plot(1:sum(geneticLength), rep(0, sum(geneticLength)), col = "white", ylim = c(-1.5,1.5), xlab = "locus", ylab = "QTL value", xaxt = "n")
    abline(h = 0, lty = 3, col = "blue")
    positionIndex <- 0 ## used to put the chromosomes next to each other on the plot
    positionAt <- NULL ## used to have the position of each Locus and be able to the have the rigth x-axis label
    for (iChr in 1:NbChromosome) {
      points(positionIndex + position[[iChr]], apply(generation[[NbrInd]][[iChr]], 2, mean)) ## plot the value of each QTL by taking the mean between the homologous
      abline(v = positionIndex + geneticLength[iChr], lty = 3, col = "red") ## delimit the chromosomes
      positionAt <- c(positionAt, positionIndex + position[[iChr]])
      positionIndex <- positionIndex + geneticLength[iChr]
    }
    
    axis(side = 1, at = positionAt, labels = round(unlist(position), digits = 1), cex.axis = 0.75) ## labels of the x-axis
  }
  dev.off()
}


## to modify (after modification of the format of generation)
plotMeanSignal <- function(N, NbChromosome, geneticLength, position, generation) 
{
  
  ## plot the signal (effect of the alleles) along the chromosomes
  ## mean of the signal of each individuals
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a plot
  
  plot(1:sum(geneticLength), rep(0, sum(geneticLength)), col = "white", ylim = c(-1.5,1.5), xlab = "locus", ylab = "QTL value", xaxt = "n")
  abline(h = 0, lty = 3, col = "blue")
  positionIndex <- 0 ## used to put the chromosomes next to each other on the plot
  positionAt <- NULL ## used to have the position of each Locus and be able to the have the rigth x-axis label
  posLabels <- NULL
  
  for (iChr in 1:NbChromosome) {
    pos <- unlist(position[[iChr]])
    pos <- pos[order(pos)]
    pos <- unique(pos)
    
    ch <- lapply(generation, "[[", iChr)
    meanPos <- rep(NA, length(pos))
    
    for (iPos in 1:length(pos)) {
      sumPos <- NULL
      for (NbrInd in 1:N) {
        sumPos <- c(sumPos, ch[[NbrInd]][,match(position[[iChr]], pos[iPos])])
      }
      meanPos[iPos] <- mean(sumPos, na.rm = TRUE)
    }
    lines(positionIndex + pos, meanPos) ## plot the value of each QTL by taking the mean between the homologous
    abline(v = positionIndex + geneticLength[iChr], lty = 3, col = "red") ## delimit the chromosomes
    positionAt <- c(positionAt, positionIndex + pos)
    positionIndex <- positionIndex + geneticLength[iChr]
    posLabels <- c(posLabels, pos)
  }
  axis(side = 1, at = positionAt, labels = round(posLabels, digits = 1), cex.axis = 0.75) ## labels of the x-axis
}


plotCdist <- function(Chr, position, Cor) 
{
  
  ## plot the correlation between QTLs as a function of the distance inter-marker, for one chromosome (linear chromosome)
  ## arguments : Chr = integer : number of the chromosome of interest
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Cor = matrix : correlation matrix
  ## return a plot
  
  pos <- unlist(position[[Chr]])
  pos <- pos[order(pos)]
  pos <- unique(pos)
  
  Ms <- length(pos)
  distij <- rep(NA, (Ms*(Ms-1)/2))
  corij <- rep(NA, (Ms*(Ms-1)/2))
  
  istart <- 1
  for (iLocus in 1:(Ms-1)) {
    posi <- pos[iLocus]
    Xij <- pos[(iLocus+1):Ms]-posi
    numDist <- Ms-iLocus
    istop <- istart+numDist-1
    Cij <- Cor[iLocus,(iLocus+1):Ms]
    distij[istart:istop] <- Xij
    corij[istart:istop] <- Cij
    istart <- istop + 1
  }
  
  myx <- unique(distij)
  index <- 1
  myy <- rep(NA, length(myx))
  for (xs in myx) {
    myy[index] <- mean(corij[(distij==xs)])
    index <- index+1
  }
  
  plot(myx, myy)
  print(myy)
  mysmooth = loess.smooth(distij, corij, span = 0.2, degree = 2, family = "gaussian", evaluation = 1000)
  
  # dev.new()
  plot(mysmooth$x, mysmooth$y, col = "red")
}


plotCdistPeriodique <- function(Chr, geneticLength, position, Cor) 
{
  
  ## plot the correlation between QTLs as a function of the distance inter-marker, for one chromosome (circular chromosome)
  ## arguments : Chr = integer : number of the chromosome of interest
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Cor = matrix : correlation matrix
  ## return a plot
  
  pos <- unlist(position[[Chr]])
  pos <- pos[order(pos)]
  pos <- unique(pos)
  
  Ms <- length(pos)
  distij <- rep(NA, (Ms*(Ms-1)/2))
  corij <- rep(NA, (Ms*(Ms-1)/2))
  
  istart <- 1
  for (iLocus in 1:(Ms-1)) {
    posi <- pos[iLocus]
    Xij <- pos[(iLocus+1):Ms]-posi
    numDist <- Ms-iLocus
    istop <- istart+numDist-1
    Cij <- Cor[iLocus,(iLocus+1):Ms]
    distij[istart:istop] <- Xij
    corij[istart:istop] <- Cij
    istart <- istop + 1
  }
  
  #   for (i in 1:length(distij)) {
  #     distij[i] <- min(distij[i], abs(geneticLength - distij[i]))
  #   }
  
  myx <- unique(distij)
  index <- 1
  myy <- rep(NA, length(myx))
  for (xs in myx) {
    myy[index] <- mean(corij[(distij==xs)])
    index <- index+1
  }
  
  plot(myx, myy)
  print(myy)
  mysmooth = loess.smooth(distij, corij, span = 0.2, degree = 2, family = "gaussian", evaluation = 1000)
  
  plot(mysmooth$x, mysmooth$y, col = "red")
}


numberDifferentAlleles <- function(NbQTLs, NbChromosome, nameProba, Mij) ## Mij12
{
  
  ## calculate the number of different alleles for each QTL (of each chromosome)
  ## arguments : NbQTLs = vector  : number of QTLs for each chromosomes
  ##             NbChromosome = integer : number of chromosomes
  ##             nameProba = string of character : name of the file where the informations about the heterozygosity will be written
  ##             Mij = list of matrix : matrix contains the effect of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write in a file
  

  NbQTL <- sum(NbQTLs)
  
  indexQTL <- 1
  NbUnique <- rep(NA, NbQTL)
  for (i in 1:NbChromosome) {
    if (NbQTLs[i] != 0) {
      for (j in 1:NbQTLs[i]) { 
        x <- Mij[[i]][,j]
        uniqueXj <- unique(na.omit(x))
        NbUnique[indexQTL] <- length(uniqueXj)
        indexQTL <- indexQTL + 1
      }
    }
  }
  write.table(t(NbUnique), file = nameProba, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
}


probaIBD <- function(NbMarkers, NbChromosome, nameProba, Mij) ## Mij56
{
  
  ## calculate the IBD probability for each marker (of each chromosome)
  ## arguments : NbMarkers = vector : number of markers (SNPs + QTLs) for each chromosome
  ##             NbChromosome = integer : number of chromosomes
  ##             nameProba = string of character : name of the file where the informations about the IBD probability will be written
  ##             Mij = list of matrix : matrix contains the origin of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write in a file
  
  NbMrkrTot <- sum(NbMarkers)
  
  probaLocus <- rep(0, NbMrkrTot)
  indexQTL <- 1
  NbUnique <- rep(NA, NbMrkrTot)
  for (i in 1:NbChromosome) {
    for (j in 1:NbMarkers[i]) { 
      x <- Mij[[i]][,j]
      uniqueXj <- unique(na.omit(x))
      NbUnique[indexQTL] <- length(uniqueXj)
      numAlleles <- rep(NA, length(uniqueXj))
      for (k in 1:length(uniqueXj)) {
        numAlleles[k] <- sum(na.omit(x) == uniqueXj[k])
        probaLocus[indexQTL] <- probaLocus[indexQTL] + numAlleles[k]*(numAlleles[k]-1)
      }
      probaLocus[indexQTL] <- probaLocus[indexQTL]/(length(na.omit(x))*(length(na.omit(x))-1))
      if (length(na.omit(x)) <= 1) probaLocus[indexQTL] <- 1
      indexQTL <- indexQTL + 1
    }
  }
  write.table(probaLocus, file = nameProba, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
  # return(sum(probaLocus)/NbQTL) 
  ## return the mean of all the IBD probability over the locus
}


theoreticalHeterozygosity <- function(N, NbChromosome, NbQTLs, nameProba, Mij) ## Mij12
{
  
  ## calculate the theoretical heterozygosity (Hardy-Weinberg hypothesis) of each QTL of each chromosomes (1 - the expected number of homozygotes)
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             nameProba = string of character : name of the file where the informations about the heterozygosity will be written
  ##             Mij = list of matrix : matrix contains the origin of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write in a file
  
  He <- rep(NA, sum(NbQTLs))
  indexQTL <- 1
  for(iChr in 1:NbChromosome){
    if(NbQTLs[iChr] != 0) {
      for(iQTL in 1:NbQTLs[iChr]){
        xi <- Mij[[iChr]][,iQTL]
        tablei <- as.data.frame(table(xi))
        pi <- tablei$Freq/(2*N)
        He[indexQTL] <- 1 - sum(pi*pi)
        indexQTL <- indexQTL + 1
      }
    }
  }
  write.table(t(He), file = nameProba, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
}


heterozygosity <- function(N, NbChromosome, NbQTLs, nameProba, Mij) ## Mij12
{
  ## calculate the real heterozygosity
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             nameProba = string of character : name of the file where the informations about the heterozygosity will be written
  ##             Mij = list of matrix : matrix contains the origin/effect of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write in a file
  
  
  He <- rep(NA, sum(NbQTLs))
  indexQTL <- 1
  for(iChr in 1:NbChromosome){
    if(NbQTLs[iChr] != 0) {
      for(iQTL in 1:NbQTLs[iChr]){
        He[indexQTL] <- 0
        for(ind in 1:N) {
          if(Mij[[iChr]][ind,iQTL] != Mij[[iChr]][ind+N,iQTL]) He[indexQTL] <- He[indexQTL] + 1
        }
        indexQTL <- indexQTL + 1
      }
    }
  }
  He <- He/N
  write.table(t(He), file = nameProba, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
  
  
}


calculGDominance <- function(N, NbChromosome, generation) 
{
  
  ## calculate the genotypic value with dominance
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a vector containing the genotypic values for all the individuals
  
  GChr <- vector("list", NbChromosome) ## G for each chromosome and for each individual
  for(i in 1:NbChromosome) {
    GChr[[i]] <- vector("list", N)
    gg <- lapply(generation, function(x) 2*x[[i]][c(1,2),]) 
    ## factor 2 in order to have the same effect as when we add the effect of all the alleles
    degreeDominance <- lapply(generation, function(x) x[[i]][7,])
    minusDegreeDominance <- lapply((lapply(degreeDominance, '*', -1)), '+', 1)
    if(length(gg[[1]]) == 2) { ## if there is only one QTL
      ggMax <- max(gg)
      ggMin <- min(gg)
    } else {
      ggMax <- lapply(gg, apply, 2, max)
      ggMin <- lapply(gg, apply, 2, min)
    }
    GChr[[i]] <- mapply('+', mapply('*',degreeDominance,ggMax, SIMPLIFY = FALSE), mapply('*',minusDegreeDominance,ggMin, SIMPLIFY = FALSE), SIMPLIFY = FALSE)
    GChr[[i]] <- lapply(GChr[[i]], sum) 
  }
  GChr <- matrix(unlist(GChr), ncol = NbChromosome, nrow = N)
  G <- apply(GChr, 1, sum)
  return(G)
  
}


calculGNoDominance <- function(N, generation) 
{
  
  ## calculate the genotypic value without dominance
  ## arguments : N = integer : number of individuals
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a vector containing the genotypic values for all the individuals
  
  GChr <- vector("list", N)
  GChr <- lapply(lapply(generation, lapply, function(x) x[1:2,]), sapply, sum)
  G <- rapply(GChr, sum, how = "replace") ## G for each individual
  G <- unlist(G)
  return(G)
  
}

# calculGPValue <- function(N, NbChromosome, dominance, VE, g, namePEvolution, repet, generation)
calculGPValue <- function(N, NbChromosome, VE, g, namePEvolution, repet, generation)
{
  
  ## calculate the genotypic and phenotypic values as well as their variance
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosomes
  ##             VE = numeric : environmental variance
  ##             g = integer : number of the generation
  ##             namePEvolution = string of character : name of the file where the informations about the gain and variances will be written 
  ##             repet = integer : number of the rep
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## return a vector containing the phenotypic values for all the individuals
  
  
  ## without dominance
  # GChr <- rep(NA, NbChromosome) ## G for each chromosome
  # for(i in 1:NbChromosome) {
  #   GChr[i] <- sum(unlist(lapply(generation, function(x) x[[i]][1:2,])))
  # }
  # G <- sum(GChr) ## G total
  
  ## with dominance
  # GChr <- rep(NA, NbChromosome) ## G for each chromosome
  # for(i in 1:NbChromosome) {
  #   gg <- lapply(generation, function(x) x[[i]][c(1,2,5),])
  #   degreeDominance <- unlist(lapply(generation, function(x) x[[i]][5,]))
  #   ggMax <- unlist(lapply(gg, apply, 2, max))
  #   ggMin <- unlist(lapply(gg, apply, 2, min)) 
  #   GChr[i] <- sum(degreeDominance*ggMax + (1-degreeDominance)*ggMin) 
  # }
  # G <- sum(GChr) ## G total
  
  
  # if(dominance) {
  #   GChr <- vector("list", N)
  #   for(i in 1:N) {
  #     GChr[[i]] <- sapply(generation[[i]], function(x){2*sum(apply(x, 2, max))})
  #   }
  # } else {
  #   GChr <- lapply(generation, sapply, sum) ## genotypic value of each chromosome of each individuals (sum of all the QTL values for each chromosome)
  # }
  # G <- rapply(GChr, sum, how = "replace") ## genotypic value of each individuals (sum of the genotypic value of each chromosome)
  
  ## if want to put the boolean dominance agian decomment this part
  #   if(dominance) {
  #     G <- calculGDominance(N, NbChromosome, generation)
  #   } else {
  #     G <- calculGNoDominance(N, generation)
  #   }
  
  G <- calculGDominance(N = N, NbChromosome = NbChromosome, generation = generation)
  GnoDom <- calculGNoDominance(N = N, generation = generation)
  
  VarG <- var(G) ## variance of the genotypic value
  E <- rnorm(N, 0, sqrt(VE)) ## environmental effect
  P <- G + E ## phenotypic value
  VarP <- var(P) ## variance of the phenotypic value
  
  write.table(list(repet, g, mean(P), VarP, mean(G), VarG, mean(GnoDom), max(P), max(G)), file = namePEvolution, append = TRUE, row.names = FALSE, col.names = FALSE, sep = "\t")
	nameG <- unlist(strsplit(namePEvolution, split = "Gain.txt"))
	write.table(t(G), file = paste(nameG, "G.txt", sep = ""), append = TRUE, row.names = paste(repet, "_", g, sep = ""), col.names = FALSE, sep = "\t")
  return(P)
}

calculVE <- function(N, NbChromosome, h2, generation)
{
  ## function to calculate the environmental variance
  G <- calculGDominance(N = N, NbChromosome = NbChromosome, generation = generation)
  VarG <- var(G)
  VE <- VarG*(1-h2)/h2
  return(VE)
}

plotLD <- function(LD, LDPos)
{
  
  ## plot the linkage desequilibrium as a function of the distance between two markers
  ## arguments : LD = vector : linkage disequilibrium between markers
  ##             LDpos = vector : distance (in cM) between the markers
  ## return a plot
  
  NbDist <- length(unique(LDPos))
  myx <- unique(LDPos)
  index <- 1
  myy <- rep(NA, length(myx))
  for (xs in myx) {
    myy[index] <- mean(LD[(LDPos==xs)])
    index <- index+1
  }
  
  plot(myx, myy)
}


## to optimise -> take too much time
linkageDisequilibrium <- function(N, g, Chr, NbMarkers, nameLD, position, Mat) ## Mij34
{
  
  ## calculation of the LD, by using r^2, for one chromosome
  ## arguments : N = integer : number of individuals
  ##             g = integer : generation number
  ##             Chr = integer : number of the chromosome of interest
  ##             NbMarkers = vector : contanins the number of marker for each chromosome
  ##             nameLD = string of character : name of the file where the information about the LD will be written in
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Mat = list of matrix : matrix contains the origin of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## return nothing
  ## write in a file
  
  ## calculation of the LD by using rij^2, allele i at the first locus, allele j at the second
  ## and then by summing over all the i and j
  ## have a r^2 per pair of locus
  ## is between 0 and 1
  ## done for only one chromosome
  ## write the mean of the LD for the positions which are in the same bins
  ## (for example locus which positions are 2.1 and 2.2 cM are considered being in the same place
  ## and so their LD will be averaged)
  
  nameLD <- paste(nameLD[Chr], "g", g, ".txt", sep = "")
  
  NbMrkr <- NbMarkers[Chr]
  pos <- position[[Chr]]
  Mat <- Mat[[Chr]]
  # colnames(Mat) <- paste("locus", 1:NbMrkr, sep = "")
  
  if(NbMrkr == 1 | NbMrkr == 0) {
    return()
  }
  
  ## pooling of r2 based on allele frequency
  
  rij <- matrix(NA, ncol = sum(1:(NbMrkr-1)), nrow = 3)
  index <- 1
  
  for(i in 1:(NbMrkr-1)) {
    
    tablei <- table(Mat[,i])
    pi <- tablei/(2*N)
    
    for(j in (i+1):NbMrkr) {
      
      Mij <- Mat[, c(i,j)]
      tableG <- table(as.data.frame(Mij))
      pij <- tableG/(2*N)
      
      tablej <- table(Mat[,j])
      pj <- tablej/(2*N)
      
      rijab <- 0
      
      piMatrix <- matrix(pi, ncol = length(pj), nrow = length(pi), byrow = FALSE)
      pjMatrix <- matrix(pj, ncol = length(pj), nrow = length(pi), byrow = TRUE)
      
      piMinusMatrix <- 1 - piMatrix
      pjMinusMatrix <- 1 - pjMatrix
      
      pijMatrix <- piMatrix * pjMatrix
      pijMinus <- pij - pijMatrix
      pijMinusMatrix <- piMatrix * piMinusMatrix * pjMatrix * pjMinusMatrix
      pijMinusSquare <- piMatrix * pjMatrix * pijMinus * pijMinus
      rijab <- pijMinusSquare / pijMinusMatrix
      
      rijab <- sum(rijab)
      
      rij[1,index] <- paste(i, "-", j, sep = "") ## contains the number of the pair of locus which LD it is
      rij[2,index] <- rijab ## LD
      rij[3,index] <- abs(pos[i] - pos[j]) ## distance between the two locus
      index <- index +1
      
      rm(Mij, tableG, pj, pij, piMatrix, pjMatrix, piMinusMatrix, pjMinusMatrix,
         pijMinus, pijMinusMatrix, pijMinusSquare, rijab)
    }
  }
  
  ## bin of the position (for eg, here 0.1 cM)
  ## eg : if x is between 0.96 and 1.04, we will say that x = 1
  rij[3,] <- as.character(lapply(as.numeric(rij[3,]), round, digits = 1)) 
  ## if want to change it to 0.2 for eg, do our own function round()?
  
  write.table(t(rij), file = nameLD, append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
  
  ## to read the LD file
  #   LD <- read.table(nameLD[Chr])
  #   LD <- apply(LD, 2, as.character)
  #   ixLD <- seq(from = 2, to = length(LD[,1])-1, by = 3) ## take only the even line (to = is the number of line)
  #   ixPos <- seq(from = 3, to = length(LD[,1]), by = 3)
  #   LDPos <- LD[ixPos,] ## attention : they are characters, need to convert them via as.numeric
  #   LD <- LD[ixLD,] ## they are characters
  #   ## for a generation g
  #   LDPos <- as.numeric(LDPos[g,])
  #   LD <- as.numeric(LD[g,])
  #   plotLD(LD, LDPos) ## plot of the LD as a function of the interlocus distance for the generation g
}


## to modify like for the function linkageDisequilibrium -> to test !!
linkageDisequilibriumAllChr <- function(N, NbChromosome, NbMarkers, nameExtended, Mij)  ## Mij34
{
  
  ## calculation of the LD, by using r^2, for one chromosome
  ## arguments : N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosome
  ##             MbMarkers = vector : contanins the number of marker for each chromosome
  ##             nameExtended = string of character : basis for the name of the file where the information will be written in
  ##             Mij = list of matrix : matrix contains the origin of each allele of each marker for one chromosome for each individuals (size 2N x NbQTL)
  ## should add the position of the markers
  ## return nothing
  ## write in a file
  
  nameLD <- paste(nameExtended, "LD.txt", sep = "")
  
  NbMrkr <- sum(NbMarkers)
#   MMij <- matrix(NA, ncol = NbMrkr, nrow = 2*N)
#   
#   for (iChr in 1:NbChromosome) {
#     MMij <- cbind(MMij, Mij[[iChr]])
#   }
#   
  MMij <- matrix(unlist(Mij), nrow = 2*N)
  
  colnames(MMij) <- paste("locus", 1:NbMrkr, sep = "")

  rij <- matrix(NA, ncol = sum(1:(NbMrkr-1)), nrow = 2)
  index <- 1
  
  if(NbMrkr == 1) {
    return()
  }
  
  for(i in 1:(NbMrkr-1)) {
    for(j in (i+1):NbMrkr) {
      
      Mij <- MMij[, c(i,j)]
      tableG <- table(data.frame(Mij))
      #       pi <- apply(tableG, paste("locus", i, sep = ""), sum)/(2*N)
      #       pj <- apply(tableG, paste("locus", j, sep = ""), sum)/(2*N)
      
      tablei <- as.data.frame(table(MMij[,i]))
      tablej <- as.data.frame(table(MMij[,j]))
      
      pi <- tablei$Freq/(2*N)
      pj <- tablej$Freq/(2*N)
      
      pij <- tableG/(2*N)
      rijab <- 0
      
      #       for(a in 1:length(pi)) {
      #         pa <- pi[a]
      #         for(b in 1:length(pj)) {
      #           pb <- pj[b]
      #           pab <- pij[a,b]
      #           rijab <- rijab + ((pab-pa*pb)^2)/((1-pa)*(1-pb))
      #         }
      #       }
      
      piMatrix <- matrix(pi, ncol = length(pj), nrow = length(pi), byrow = FALSE)
      pjMatrix <- matrix(pj, ncol = length(pj), nrow = length(pi), byrow = TRUE)
      piminus <- 1-pi
      pjminus <- 1-pj
      piMinusMatrix <- matrix(piminus, nrow = length(piminus), ncol = length(pjminus), byrow = FALSE)
      pjMinusMatrix <- matrix(pjminus, nrow = length(piminus), ncol = length(pjminus), byrow = TRUE)
      
      pijMatrix <- piMatrix * pjMatrix
      pijMinus <- pij - pijMatrix
      pijMinusMatrix <- piMinusMatrix * pjMinusMatrix
      pijMinusSquare <- pijMinus * pijMinus
      rijab <- pijMinusSquare / pijMinusMatrix
      
      rijab <- sum(rijab)
      
      rij[1,index] <- paste(i, "-", j, sep = "") ## contains the number of the pair of locus which LD it is
      rij[2,index] <- rijab ## LD
      # rij[3,index] <- abs(pos[i] - pos[j]) ## distance between the two locus
      index <- index +1
      
      rm(list = c("Mij", "tableG", 
                  "pi", "pj", "pij", 
                  "piMatrix", "pjMatrix", 
                  "piminus", "pjminus",
                  "piMinusMatrix", "pjMinusMatrix", 
                  "pijMatrix", "pijMinus", "pijMinusMatrix",
                  "pijMinusSquare"))
      
    }
  }
  
  ## bin of the position (for eg, here 0.1 cM)
  ## eg : if x is between 0.96 and 1.04, we will say that x = 1
  # rij[3,] <- as.character(lapply(as.numeric(rij[3,]), round, digits = 1)) 
  ## if want to change ot to 0.2 for eg, do our own function round()?
  
  write.table((rij), file = nameLD[Chr], append = TRUE, sep = "\t", row.names = FALSE, col.names = FALSE)
  
}


allelesDistribution <- function(nameFile, generation) 
{
  ## return the distribution of the alleles (QTLs) effects i.e. the number of occurence for each effects (breaks)
  ## arguments : nameFile = character string : name of the file where the information about the alleles distribution will be written in
  ##             generation = list of list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals
  ## returns nothing
  ## write the break points and number of occurence of each effects in a file
  alleles1 <- unlist(sapply(generation, lapply, function(x) x[1,which(x[8,] == 1)]))
  alleles2 <- unlist(sapply(generation, lapply, function(x) x[2,which(x[8,] == 1)]))
  alleles <- c(alleles1, alleles2)
  distribAlleles <- hist(alleles, breaks=seq(from=round(min(alleles),2)-0.01, to = round(max(alleles),2)+0.01, by = 0.01), main = "allele distribution")
  write.table(t(distribAlleles$mids), file = nameFile, append = TRUE, col.names = FALSE, row.names = FALSE)
  write.table(t(distribAlleles$counts), file = nameFile, append = TRUE, col.names = FALSE, row.names = FALSE)
}


originAlleles <- function(Ninitial, N, NbChromosome, g, nameFile, NbQTLs, NbMarkers, position, Mij, Mij2) ## MijMarker56 et MijQTL12
{
  ## plot of the origin of alleles
  ## arguments : N = ineteger : number of individuals in the population
  ##             NbChromosome = integer : number of chromosome
  ##             g = integer : generation number
  ##             nameFile = character string : name of the file where the information about the origin of markers and effect of alleles will be written in
  ##             NbMarkers = vector : number of markers (QTLs + SNPs) for each chromosome
  ##             positionQTLs = list (NbChromosome element) of matrix (2 x NbQTLs) : contains the position of the QTLs for each chromosome; the second line contains ones (code if the locus we are looking at is a SNP(0) or a QTL(1))
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             Mij = list of matrix : matrix contains the origin of each allele of each marker for one chromosome for each individuals (size 2N x NbMarkers)
  ##             Mij2 = list of matrix : matrix contains the effect of each allele of each QTL for one chromosome for each individuals (size 2N x NbQTLs)
  ## return nothing
  
  pdf(paste(nameFile, g, ".pdf", sep = ""))
  
  for (iChr in 1:NbChromosome) {

    nameCol <- rep(NA, 2*N)
    indexQTLs <- which(position[[iChr]][2,] == 1)
    nameRow <- rep(NA, NbMarkers[iChr])
    nameRow[indexQTLs] <- position[[iChr]][1, indexQTLs]
    rowSideCol <- rep("white", NbMarkers[iChr])
    rowSideCol[indexQTLs] <- rep("black", length(indexQTLs))
    
    heatmap(t(Mij[[iChr]]), col = rainbow(Ninitial), Rowv = NA, Colv = NA, scale = "none", 
            labCol = nameCol, labRow = nameRow, xlab = "individuals", RowSideColors = rowSideCol,
            ylab = "origine markers", main = paste("chromosome ", iChr, sep = ""))
    
#     heatmap.2(t(Mij[[iChr]]), col = rainbow(Ninitial), dendrogram = "none", scale = "none", 
#             labCol = nameCol, labRow = nameRow, xlab = "individuals",
#             ylab = "origine markers", main = paste("chromosome ", iChr, sep = ""), trace = "none",
#             keysize = 1, denscol = "black", RowSideColors = rowSideCol) 
    
    
    if(NbQTLs[iChr] > 0) {
     nameRow <- rep(NA, NbQTLs[iChr])
     nameRow <- position[[iChr]][1,which(position[[iChr]][2,] == 1)]
      
      if(NbQTLs[iChr] == 1) Mij2[[iChr]] <- matrix(Mij2[[iChr]], ncol = 2, nrow = length(Mij2[[iChr]][,1]))
        
#       heatmap(t(Mij2[[iChr]]), col = rainbow(2*N), Rowv = NA, Colv = NA, scale = "none", 
#               labCol = nameCol, labRow = nameRow, xlab = "individuals",
#               ylab = "QTL allele effect", main = paste("chromosome ", iChr, sep = ""))
     
     heatmap.2(t(Mij2[[iChr]]), col = rainbow(5*NbQTLs[iChr], start = 0, end = 0.9), dendrogram = "none", scale = "none", 
               labCol = nameCol, labRow = nameRow, xlab = "individuals",
               ylab = "markers", main = paste("chromosome ", iChr, sep = ""), cexRow = 1, trace = "none",
               keysize = 1, denscol = "black") 
     
    }
  }
  
  dev.off()
}


percentageVarianceExplained <- function(repet, N, g, nameEffect, nameFile, NbQTLs, generation)
{
  ## function to calculate the number of effective QTLs
  ## and then to calculate the percentage of variance of each of these effective QTLs
  ## arguments : repet = integer : number of the rep
  ##             N 
  ##             g
  ##             nameEffect
  ##             nameFile
  ##             NbQTLs
  ##             generation
  
  M <- sum(NbQTLs) ## for now consider that all chr have the same nbr of qtls 

  
  #############################################################################################################################
  ## determination of the number of effective qtls
  
  qtl <- lapply(generation, lapply, function(x) x[1:2,which(x[8,] == 1)])
  
  ## for all individuals in the population
  ## calculation of the effective number of QTLs
  
  qtlTransformed <- lapply(qtl, lapply, apply, 2, sum) ## effect of one qtl = sum of the two alleles
  qtlTot <- matrix(unlist(qtlTransformed), nrow = N, ncol = M, byrow = TRUE)
  # hist(qtlTot, breaks = seq(min(qtlTot)-0.01, max(qtlTot)+0.01, by = 0.01), main = paste("distribution of QTL effect, g ", g, "rep ", repet, sep = ""))
  qtlTot2 <- apply(qtlTot, 2, mean)
  qtlTot <- apply(qtlTot, 2, sum)
  
  ## THIS IS THE ONE I USE TO DETERMINE THE EFFECTIVE NUMBER OF QTLs
  Yi <- abs(qtlTot)/sum(abs(qtlTot))
  QTLeff <- (sum(Yi)^2)/(sum(Yi^2))
  
  
  ## compared to the effective number found via the file effect QTL
  nameEffectQTL <- paste(nameEffect, "QTL.txt", sep = "")
  effect <- read.table(nameEffectQTL, sep = "\t", header = FALSE, fill = TRUE)
  effect <- effect[,-1] ## get rid of the first column as it contains the number of the chromosomes
  
  ## calculation chromosome per chromosome
  Yi <- apply(effect, 1, function(x) abs(x)/sum(abs(x), na.rm = TRUE))
  QTLeff2 <- apply(Yi, 2, function(x) (sum(x, na.rm = TRUE)^2)/(sum(x^2, na.rm = TRUE)))
  
  
  ## calculation by putting all the chromosomes together
  ## QTLeff on the whole genome
  effect <- c(t(effect))
  Yi <- abs(effect)/sum(abs(effect), na.rm = TRUE)
  QTLeff3 <- (sum(Yi, na.rm = TRUE)^2)/(sum(Yi^2, na.rm = TRUE))
  
  
  ## index of the QTL considered as effective
  # QTLsig <- order(abs(effect), decreasing = TRUE)[1:QTLeff3] 
  QTLsig <- order(abs(qtlTot2), decreasing = TRUE)[1:QTLeff] 

  
  #####################################################################################################
  ## percentage of variance explained by each QTLs, in the initial population
  ## use an ANOVA or a regression analysis (both gives the same results)
  
  qtlTransformed <- lapply(qtl, lapply, apply, 2, sum) ## effect of one qtl = sum of the two alleles
  ch <- matrix(unlist(lapply(qtlTransformed, unlist)), nrow = N, ncol = M, byrow = TRUE) 
  chsig <- ch[,QTLsig]
  
  Gi <- apply(ch, 1, sum) ## genotypic value for each individuals
  ## do not use Gi but Pi -> how to use it?
  
  ## note : 
  ## in the models, for each qtl, can consider, as parameters
  ## (1) only ch[,i]
  ## (2) ch[,i] and sum(ch[,-i]) i.e a second parameter which contains all the qtls
  ## except the one which we are trying to calculate the percentage of variance
  ## (3) all the qtls except the one of interest : sum(ch[,-i])
  ## (can do that becaus G = sum all qtls and all of the qtls have the same weight)
  ## (if do model with P and E = 0 or if we do the model with G,
  ## percVar = 1-r2 or SStot-SSparam because the residual will only be the qtls of interest
  ## but if E!=0 (model with P), residual = E + qtl of interest, hence this solution is not valid, 
  ## except if we put E in the model)
  
  ## with regression analysis
  # r2 <- rep(NA, M)
  # for(i in 1:M) {
  #   model <- summary(lm(Gi ~ ch[,i]))
  #   r2[i] <- model$r.squared
  #   print(model)
  # }
  
  ## with anova (for all QTLs)
  #   if(N > 1) {
  #     percVar <- rep(NA, M)
  #     for(i in 1:M) {
  #       chbis <- apply(ch[,-i], 1, sum)
  #       model <- summary(aov(Gi ~ chbis + ch[,i]))
  #       percVar[i] <- model[[1]][,'Sum Sq'][2] / sum(model[[1]][,'Sum Sq'])
  #       # print(model)
  #     }
  #   }
  
  ## with ANOVA, only on the QTLs with the most important effect
  ## the number of used QTLs is determined by IPR (QTLeff3)
  ## the other QTLs are considered as missing heritability
  if(N > 3) {
    percVar <- rep(NA, M)
    percVar2 <- rep(NA, M)
    for(i in QTLsig) { ## % of var caluclated only on the effective QTLs
      chbis <- apply(ch[,-i], 1, sum)
      model <- summary(aov(Gi ~ chbis * ch[,i])) ## with interactions
      # model <- summary(aov(Gi ~ ch[,-i] * ch[,i])) ## with interactions
      percVar[i] <- model[[1]][,'Sum Sq'][2] / sum(model[[1]][,'Sum Sq'])
      percVar2[i] <- var(ch[,i]) / var(Gi)
    }
    
    # barplot(percVar, xlab = "QTL", ylab = "percentage of variance explained by effective QTLs", main = paste("rep ", repet, "g ", g, sep = ""), names.arg = 1:M)
    
    QTLeffData <- c(repet, g, QTLeff, percVar)
    QTLeffData2 <- c(repet, g, QTLeff, percVar2)
    
    write.table(t(QTLeffData), append = TRUE,
                file = nameFile, col.names = FALSE, row.names = FALSE, sep = "\t")
    name <- strsplit(nameFile, ".txt")
    nameFile <- paste(name, "2.txt", sep = "")
    write.table(t(QTLeffData2), append = TRUE,
                file = nameFile, col.names = FALSE, row.names = FALSE, sep = "\t")
  }
  
  ## problem when the number of individuals is only one : cannot use the ANOVA
  if(N <= 3) {
    ## for now no calculation -> calculate it in the F2 population
  }
  
  return()
  
}


## return something for every function and write every indicator in the same file
Analysis <- function(repet, Ninitial, N, NbChromosome, g, VE, NbQTLs, NbMarkers, nameCor, nameCorMean, nameProba, nameProba2, namePEvolution,
                     nameLD, nameAllelesDistrib, nameOriginAlleles, namePercVar, nameEffect, position, generation) 
{
  
  ## function to calculate different indicator for the selection
  ## calculation of the genotypic and phenotypic values as well as their variance
  ## but also of various indices describing the population
  ## calculGPValue : genotypic and phenotypic value and their variances
  ## calculCorrelationMatrix : matrix of the correlation between the locus
  ## numberDifferentAlleles : number of different alleles for each locus
  ## probaIBD : probability that two alleles come from the same individual in the initial population (the more frequent allele)
  ## heterozygosity : expected probability to have heterozygote
  ## linkageDisequilibrium : LD between two locus for a distance (using r^2)
  ## arguments : repet = integer : number of the rep
  ##             Ninitial = integer : initial number of individuals
  ##             N = integer : number of individuals
  ##             NbChromosome = integer : number of chromosome
  ##             g = integer : number of the generation
  ##             VE = numeric : environmental variance
  ##             selectionGeneration1
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             NbMarkers = vector : contanins the number of marker for each chromosome
  ##             nameCor = string of character : name of the file where the correlation matrix will be written
  ##             nameCorMean = string of character : name of the file where the mean of the correlation matrix will be written
  ##             nameProba = string of character : name of the file where the informations about the heterozygosity will be written
  ##             nameProba2 = character string : name of the file where the informations about the IBD proba will be written
  ##             namePEvolution = string of character : name of the file where the informations about the gain and variances will be written
  ##             nameLD = string of character : name of the file where the information about the LD will be written in
  ##             nameAllelesDistrib = character string : name of the file where the information about the allele distribution will be written
  ##             nameOriginAlleles = character string : name of the file where the information about the origine of the markers will be written
  ##             namePercVar = character string : name of the file where the percentage of variance explained by the QTLs wil be written
  ##             nameEffect
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ## return a vector containing the phenotypic values for all the individuals
  
  
  ## define the name of the file here instead of in the main?
  
  ## rows = 1 and 2 -> use the allelic effects
  ## rows = 3 and 4 -> use the origin of the allele (number of the allele used)
  
  Mij12 <- calculQTLMatrix(N = N, NbChromosome = NbChromosome, row1 = 1, row2 = 2, NbQTLs = NbQTLs, generation = generation) 
  Mij34QTL <- calculQTLMatrix(N = N, NbChromosome = NbChromosome, row1 = 3, row2 = 4, NbQTLs = NbQTLs, generation = generation) ## that way, do not take into account the sign of the effect
  Mij34 <- calculMarkersMatrix(N = N, NbChromosome = NbChromosome, row1 = 3, row2 = 4, NbMarkers = NbMarkers, generation = generation)
  Mij56 <- calculMarkersMatrix(N = N, NbChromosome = NbChromosome, row1 = 5, row2 = 6, NbMarkers = NbMarkers, generation = generation)
  
  # P <- calculGPValue(N, NbChromosome, dominance, VE, g, namePEvolution, repet, generation)
  P <- calculGPValue(N = N, NbChromosome = NbChromosome, VE = VE, g = g, namePEvolution = namePEvolution, repet = repet, generation = generation)
  
  numberDifferentAlleles(NbQTLs = NbQTLs, NbChromosome = NbChromosome, nameProba = nameProba, Mij = Mij12) 
  
  probaIBD(NbMarkers = NbMarkers, NbChromosome = NbChromosome, nameProba = nameProba2, Mij = Mij56)
  
  heterozygosity(N = N, NbChromosome = NbChromosome, NbQTLs = NbQTLs, nameProba = nameProba, Mij = Mij12)
  
  allelesDistribution(nameFile = nameAllelesDistrib, generation = generation)
  
  percentageVarianceExplained(repet = repet, N = N, g = g, nameEffect = nameEffect, nameFile = namePercVar, NbQTLs = NbQTLs, generation = generation)
  
  ## do not want to look at it for now
#   originAlleles(Ninitial = Ninitial, N = N, NbChromosome = NbChromosome, g = g, nameFile = nameOriginAlleles, NbQTLs = NbQTLs, NbMarkers = NbMarkers,
#                 position = position, Mij = Mij56, Mij2 = Mij12)
    
  
  ## can do a loop over the chromosome
  ## if want to have the LD between the different chromosomes use
  ## linkageDisequilibriumAllChr(N, NbChromosome, position, generation, Ms, nameLD)
  ## but takes alot more time
  
#   for(iChr in 1:NbChromosome) {## juste pour une simu
#     calculCorrelationMatrix(Chr = iChr, g = g, nameCor = nameCor, nameCorMean = nameCorMean, repet = repet, position = position, Mij = Mij12)## juste pour une simu
#     linkageDisequilibrium(N = N, g = g, Chr = iChr, NbMarkers = NbMarkers, nameLD = nameLD, position = position, Mat = Mij34)## juste pour une simu (should be NbMarkers and Mij34)
#   }
  
  return(P)
}




## function to estimate the standard error of the base population
## in order to get the standardized genetic gain ie G/sd(G)
## need to source the file funSelection before using the function
## to get the function to create a new population 
estimationSDBasePop <- function(nameFolder, namePopulation, iLG, Nest, irep)
{
  
  ## return the genetic value of the population (vector)
  ## use to estimate the standard error of the base population
  
  ## nameFolder : name of the folder where the SNPs and QTLs positions
  ##              as well as the parameter files are
  ## namePopulation : name of the population file (.RData), parental population (F1 if want to recreate a F2)
  ## iLG : number -> increase of recombinattion compared to the standard recombination
  ## Nest : population size for the estimated population, 10000 should be sufficient
  ## irep : number of the rep (can be "" if the position of QTL do not depend of the rep
  
  load(namePopulation)
  pop <- generation
  rm(generation)
  
  ##### POSITIONS
  posSNPs <- read.csv(paste(nameFolder, "/positionSNPs.csv", sep = ""),
                      header=FALSE, stringsAsFactors=FALSE)
  posQTLs <- read.csv(paste(nameFolder, "/positionQTLs", irep, ".csv", sep = ""), 
                      header=FALSE, stringsAsFactors=FALSE)
  posQTLs <- apply(posQTLs, 1, list)
  posQTLs <- lapply(posQTLs, unlist)
  posQTLs <- lapply(posQTLs, rbind, 1)
  posQTLs <- lapply(posQTLs, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
  posSNPs <- apply(posSNPs, 1, list)
  posSNPs <- lapply(posSNPs, unlist)
  posSNPs <- lapply(posSNPs, rbind, 0)
  posSNPs <- lapply(posSNPs, function(x) matrix(x[,!is.na(x[1,])], nrow = 2))
  position <- mapply(function(x,y) cbind(x,y), posQTLs, posSNPs, SIMPLIFY = FALSE)
  position <- lapply(position, function(x) x[,order(x[1,])])
  
  ##### PARAMETERS
  parameters <- read.csv(paste(nameFolder, "/parameters.csv", sep = ""), 
                         header=FALSE, stringsAsFactors=FALSE)
  CIRCULARCHROMOSOME <- as.logical(parameters$V2[parameters$V1 == "CIRCULARCHROMOSOME"])
  NbChromosome <- as.numeric(parameters$V2[parameters$V1 == "NbChromosome"])
  modeRecombination <- parameters$V2[parameters$V1 == "modeRecombination"]
  geneticLength <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "geneticLength"], split = ",")))
  NbQTLs <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "NbQTLs"], split = ",")))
  NbSNPs <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "NbSNPs"], split = ",")))
  dominance <- parameters$V2[parameters$V1 == "dominance"]
  degreeOfDominance <- matrix(NA, ncol = max(NbQTLs), nrow = NbChromosome)
	nuGamma <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "nuGamma"], split = ",")))
	pGamma <- as.numeric(unlist(strsplit(parameters$V2[parameters$V1 == "p"], split = ",")))
  for(iChr in 1:NbChromosome) {
    if (dominance == "No") degreeOfDominance[iChr,] <- rep(0.5, max(NbQTLs))
    if (dominance == "Max") degreeOfDominance[iChr,] <- rep(1, max(NbQTLs))
    if (dominance == "Min") degreeOfDominance[iChr,] <- rep(0, max(NbQTLs))
    if (dominance == "Random") degreeOfDominance[iChr,] <- sample(0:10, max(NbQTLs), replace = TRUE)/10 
  }
  geneticLength <- iLG * geneticLength
  
  popOff <- vector("list", Nest)
  
  for(iOff in 1:Nest) {
    popOff[[iOff]] <- PassageGeneration(NbChromosome = NbChromosome, 
                                        NbQTLs = NbQTLs, NbSNPs = NbSNPs, 
                                        geneticLength = geneticLength, 
                                        modeRecombination = modeRecombination,
                                        reproductionType = "SP", 
                                        position = position, 
                                        generationSelected = pop, 
                                        degreeOfDominance = degreeOfDominance, 
                                        ixParents = 1, nuGamma = nuGamma, pGamma = pGamma)
  }
  
  G <- calculGNoDominance(Nest, popOff) ## F2 Nest ind
  
  return(G)
}













