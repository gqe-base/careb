## functions for the meiosis and the selection


## added the possibility of having HD and selfing as well as random selection

PassageGeneration <- function(NbChromosome, NbQTLs, NbSNPs, geneticLength, modeRecombination, reproductionType, 
                              position, generationSelected, degreeOfDominance, ixParents, nuGamma = nuGamma, pGamma = pGamma)
{
  
  ## function to create a new diploid (pair of chromsomes) individual
  ## arguments : NbChromosome = integer : number of chromosome
  ##             NbQTLs = vector : number of QTLs for each chromosome
  ##             NbSNPs = vector : number of SNPs for each chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             modeRecombination = string of character : "Haldane", "completeInterference", "freeRecombination", "interferenceGamma", "interferenceGamma2"
  ##             reproductionType = string of character : "HD" (doubled haploid), "SP" (selfing), "SC" (panmixie)
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             generationSelected = list of list of matrix : effect, index, type and origin of the alleles of the different markers (+dominance) for each chromosomes of each individuals (selected)
  ##             degreeOfDominance = matrix : contains the coefficient for the type of dominance chosen (NbChromosome x max(NbQTLs)) 
  ##             ixParents = vector or integer : index, in the population, of the parent(s)
  ## return a list of matrix (NbChromosome matrices of size 5xNbQTL)
  
  l <- length(generationSelected)
  genome <- vector("list", NbChromosome)
  k <- ixParents
  if(reproductionType == "HD") { ## if HD ( doubled haplodiploidy)
    # k <- sample(1:l,1) 
    individu <- generationSelected[[k]]
    gamete1 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu, nuGamma = nuGamma, pGamma = pGamma) 
    gamete2 <- gamete1
  } else if(reproductionType == "SP") { ## if selfing
    # k <- sample(1:l,1) 
    individu <- generationSelected[[k]]
    gamete1 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu, nuGamma = nuGamma, pGamma = pGamma)
    gamete2 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu, nuGamma = nuGamma, pGamma = pGamma)
  } else { ## if standard cross (panmixie)
    # k <- sample(1:l,2, replace = FALSE) ## if want to have selfing: replace = TRUE
    i <- k[1]
    j <- k[2]
    individu1 <- generationSelected[[i]]
    individu2 <- generationSelected[[j]]
    gamete1 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu1, nuGamma = nuGamma, pGamma = pGamma)
    gamete2 <- recombination(NbChromosome = NbChromosome, modeRecombination = modeRecombination, geneticLength = geneticLength, position = position, individu = individu2, nuGamma = nuGamma, pGamma = pGamma)
  }
  for (k in 1:NbChromosome) {
    m <- matrix(nrow = 8, ncol = length(position[[k]][1,]))
    m[1,] <- gamete1[[k]][1,]
    m[2,] <- gamete2[[k]][1,]
    m[3,] <- gamete1[[k]][2,] ## change to accomodate adding lines for index alleles
    m[4,] <- gamete2[[k]][2,]
    m[5,] <- gamete1[[k]][3,] ## change to accomodate adding lines for origin alleles
    m[6,] <- gamete2[[k]][3,]
    m[7,which(position[[k]][2,] == 1)] <- degreeOfDominance[k,1:NbQTLs[k]] 
    m[7,which(position[[k]][2,] == 0)] <- rep(0.5, NbSNPs[k])
    m[8,] <- position[[k]][2,]
    genome[[k]] <- m
    # write.table(genome[[k]], file = nameGenomeg, sep="\t", append = TRUE, col.names = FALSE, row.names = FALSE)
    # Ms[k] <- length(m[m[1,]!=0 | m[2,]!=0])/2
    
    ## with the haplotype
#     m[3,which(position[[k]][2,] == 1)] <- gamete1[[k]][2,]
#     m[4,which(position[[k]][2,] == 1)] <- gamete2[[k]][2,]
#     m[3,which(position[[k]][2,] == 1)] <- mapply(function(x,y) sample(c(x,y),1), gamete1[[k]][2,], gamete2[[k]][2,])
#     m[4,which(position[[k]][2,] == 1)] <- m[3,which(position[[k]][2,] == 1)]
    
  }
  # write.table(Ms, file = nameLocusg, append = TRUE, col.names = FALSE, row.names = FALSE, sep="\t")
  return(genome)
}


recombination <- function(NbChromosome, modeRecombination, geneticLength, position, individu, nuGamma, pGamma) 
{
  
  ## function to create the gamete of a diploid individual 
  ## arguments : NbChromosome = integer : number of chromosome
  ##             geneticLength = vector : genetic length (in cM) of each chromosome
  ##             modeRecombination = string of character : "Haldane", "completeInterference", "freeRecombination"
  ##             position = list of vector : position (in cM) on the chromosome of the markers, for each chromosome
  ##             individu = list of matrix : effect and origin of the alleles of the different markers (+dominance) for each chromosomes for one individual
  ## return a list of matrix (NbChromosome matrices of size 2xNbQTL)
  
  gamete <- vector("list", NbChromosome)
  if (modeRecombination == "Haldane") {
    for (iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      posChr <- position[[iChr]]
      LG <- geneticLength[iChr]
      gamete[[iChr]] <-  Haldane(LG = LG, position = posChr, genome = genome)
    }
    return(gamete)
  }
  if (modeRecombination == "completeInterference") {
    for (iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      posChr <- position[[iChr]]
      gamete[[iChr]]<- completeInterference(position = posChr, genome = genome)
    }
    return(gamete)
  }
  if (modeRecombination == "freeRecombination") {
    for (iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      posChr <- position[[iChr]]
      gamete[[iChr]] <- freeRecombination(position = posChr, genome = genome)
    }
    return(gamete)
  }
  if(modeRecombination == "noRecombination") {
    for(iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      iHaplotype <- sample(1:2,1)
      gamete[[iChr]] <- genome[c(iHaplotype, iHaplotype+2, iHaplotype+4),]
    }
    return(gamete)
  }
  if (modeRecombination == "interferenceGamma") {
    for (iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      posChr <- position[[iChr]]
      LG <- geneticLength[iChr]
      gamete[[iChr]] <-  interferenceGamma(nu = nuGamma[iChr], LG = LG, position = posChr, genotype = genome)
    }
    return(gamete)
  }
  if (modeRecombination == "interferenceGamma2") {
    for (iChr in 1:NbChromosome) {
      genome <- individu[[iChr]]
      posChr <- position[[iChr]]
      LG <- geneticLength[iChr]
      gamete[[iChr]] <-  interferenceGamma2(nu = nuGamma[iChr], p = pGamma[iChr], LG = LG, position = posChr, genotype = genome)
    }
    return(gamete)
  }
}


Haldane <- function(LG, position, genome) 
{
  
  ## function to create the gamete of a diploid individual using the Haldane model
  ## jump from the position of one crossover to another using a Poisson law
  ## thinning process (crossover not kept with a probability of 0.5)
  ## creates the gamete for one pair of chromosomes
  ## arguments : LG = integer : genetic length (in cM) of the chromosome
  ##             position = vector : position (in cM) on the chromosome of the markers for one chromosome
  ##             genome = matrix : effect and origin of the alleles of the different markers (+dominance) for one chromosome of one individual
  ## return a matrix (size 2xNbQTL)
  
	CIRCULARCHROMOSOME <- FALSE
  
  gamete <- matrix(NA, ncol = length(position[1,]), nrow = 3) ## change to accomodate adding lines for index and origin alleles
  
  if (CIRCULARCHROMOSOME) { ## if circular chromosome : even number of crossovers
    # We thus treat only without thinning
    ## determine mean of poisson law which gives genetic length when restricted to even number of crossovers
    mu <- uniroot(function(x) x*tanh(x)-LG/100, lower = 0, upper = 1+2*LG/100)$root
    Nbco <- 1
    while(Nbco%%2 == 1) {
      Nbco <- rpois(1, mu)
    }
    if (Nbco == 0) {
      homologue <- sample(1:2,1)
      return(genome[c(homologue,homologue+2,homologue+4),]) ## change to accomodate adding lines for origin alleles
    }
    posCOvect <- runif(Nbco)*LG
    posCOvect <- posCOvect[order(posCOvect)]
    keepCO <- c(0, posCOvect, LG)
    
  } else { ## case with thinning, only possible with linear chromosome
    positionCO <- NULL
    keepCO <- c(0) 
    posCO <- rexp(1,2/100)  ## position in cM (1/100) and thinning (factor 2 : probability of an half to keep a crossover) => rate = 2/100
    while (posCO < LG) {
      positionCO <- c(positionCO,posCO)
      jumpLength <- rexp(1,2/100)
      posCO <- posCO+jumpLength
    }
    if (length(positionCO) != 0) { ## thinning (not all DSB become crossovers)
      vec01 <- sample(c(FALSE,TRUE),length(positionCO),replace=TRUE)
      keepCO <- c(keepCO,positionCO[vec01])
    }
    keepCO <- c(keepCO,LG) ## I begin and end the vector of kept crossover by 0 (beginning of the chromosome) and the genetic length (end of the chromosome) for technical reasons, for when I recombine the allelic effects to create the gamete.
  }
  homologue <- sample(1:2,1) ## parental chromosome selected at origin
  for (jLeft in 1:(length(keepCO)-1)) {
    Ix <- which(keepCO[jLeft]<=position[1,]  & position[1,]<=keepCO[jLeft+1])
    if (length(Ix)) {
      gamete[1,Ix] <- genome[homologue,Ix]
      gamete[2,Ix] <- genome[homologue+2,Ix] ## change to accomodate adding lines for index alleles
      gamete[3,Ix] <- genome[homologue+4,Ix] ## change to accomodate adding lines for origin alleles
    }
    homologue <- 3-homologue
  }
  # write.table(length(keepCO)-2, file = "nbco.txt", append = TRUE, col.names = FALSE, row.names = FALSE)
  return(gamete)
}

interferenceGamma <- function(nu, LG, position, genotype)
{
  ## funtion to simulates crossovers between parental haplotypes to generate
  ## gamete haplotypes using the Gamma model for crossover interference
  ## arguments: nuGamma = numeric: interference strength expressed as the nu parameter
  ##                      of the Gamma model. nu=1 for no interference. nu>1 for positive interference
  ##            LG = numeric: genetic length (in centiMorgans)of the chromosome considered
  ##            position = matrix: 1 x NbMarkers containing genetic positions of all markers
  ##            genotype =  matrix: 2 x NbMarkers with the diploid genotypes of parent
  ## return matrix: 1 x NbMarkers containing the gamete haplotype

  gamete <- matrix (NA, ncol = length(position[1,]), nrow = 3)
  positionCO <- NULL
  posCO <- -10   #  Stationary Renewal Process starts at -10 Morgans to lose memory of the origin
  while (posCO < LG/100) {  # LG is in centiMorgans
    positionCO <- c(positionCO,posCO)
    jumpLength <- rgamma(1,shape=nu,rate=2*nu)  # simulations are in Morgans
    posCO <- posCO + jumpLength
  }
  positionCO <- positionCO[positionCO > 0]
  positionCO <- positionCO * 100  #  distances in centiMorgans
  vec01 <- NULL
  if (length(positionCO) != 0) { # thinning
    vec01 <- sample(c(FALSE,TRUE), length(positionCO), replace=TRUE)
  }
  keepCO <- c(0, positionCO[vec01], LG)  # forcing "a false COs" at the extremities of the chromosome
  homologue <- sample(1:2,1) # parental chromosome selected at origin
  for (jLeft in 1:(length(keepCO)-1)) {
    Ix <- which(keepCO[jLeft]<=position[1,] & position[1,]<=keepCO[jLeft+1])
    if (length(Ix)>0) {
      gamete[1,Ix] <- genotype[homologue,Ix]
      gamete[2,Ix] <- genotype[homologue+2,Ix] ## change to accomodate adding lines for index alleles
      gamete[3,Ix] <- genotype[homologue+4,Ix] ## change to accomodate adding lines for origin alleles
    }
    homologue = 3-homologue
  }
  return(gamete)
  
  
}

listCO <- function(LG, nu)
{
	## function to simulate the position of crossovers for a chromosome of length LG
	## positions taken from a gamma distribution of parameter nu (if no interference, nu = 1)
	## with thinning
	## return a vector containing the crossover positions

  positionCO <- NULL
  posCO <- -10   #  Stationary Renewal Process starts at -10 Morgans to lose memory of the origin
  while (posCO < LG/100) {  # LG is in centiMorgans
    positionCO <- c(positionCO,posCO)
    jumpLength <- rgamma(1,shape=nu,rate=2*nu)  # simulations are in Morgans
    posCO <- posCO + jumpLength
  }
  positionCO <- positionCO[positionCO > 0]
  positionCO <- positionCO * 100  #  distances in centiMorgans
  vec01 <- NULL
  if (length(positionCO) != 0) { # thinning
    vec01 <- sample(c(FALSE,TRUE), length(positionCO), replace=TRUE)
  }
  keepCO <- positionCO[vec01]  # contrary to the functions Haldane and interferenceGamma, do not force a co at the extremities; do it afterwards

	return(keepCO)
}


interferenceGamma2 <- function(nu, p, LG, position, genotype)
{
  ## funtion to simulates crossovers between parental haplotypes to generate
  ## gamete haplotypes using the Gamma model for crossover interference (two pathway model with interfering and non interfering crossovers)
	## can be a substitute for the functions Haldane (p = 1, nu = 1) and interferenceGamma (p = 0)
  ## arguments: nuGamma = numeric: interference strength expressed as the nu parameter
  ##                      of the Gamma model. nu=1 for no interference. nu>1 for positive interference
	## 						p = numeric: proportion of interfering co (between 0 and 1)
  ##            LG = numeric: genetic length (in centiMorgans)of the chromosome considered
  ##            position = matrix: 1 x NbMarkers containing genetic positions of all markers
  ##            genotype =  matrix: 2 x NbMarkers with the diploid genotypes of parent
  ## return matrix: 1 x NbMarkers containing the gamete haplotype

  gamete <- matrix (NA, ncol = length(position[1,]), nrow = 3)

	posClassI <- NULL
	if(p != 0) {	
		LGI <- LG*p ## a proportion p of the crossovers are class I crossovers (interfering)
		posClassI <- listCO(LG = LGI, nu = nu)
		posClassI <- posClassI/p ## rescale the crossover positions to have crossovers up to LG
	}

	posClassII <- NULL
	if(p != 1) {	
		LGII <- LG*(1-p) ## a proportion 1-p of the crossovers are class II crossovers (non interfering)
		posClassII <- listCO(LG = LGII, nu = 1) ## under no interference, nu = 1
		posClassII <- posClassII/(1-p) ## rescale the crossover positions to have crossovers up to LG
	}

  keepCO <- c(0, sort(c(posClassI, posClassII)), LG)  # forcing "a false COs" at the extremities of the chromosome + order the crossovers previously obtained
  homologue <- sample(1:2,1) # parental chromosome selected at origin
  for (jLeft in 1:(length(keepCO)-1)) {
    Ix <- which(keepCO[jLeft]<=position[1,] & position[1,]<=keepCO[jLeft+1])
    if (length(Ix)>0) {
      gamete[1,Ix] <- genotype[homologue,Ix]
      gamete[2,Ix] <- genotype[homologue+2,Ix] ## change to accomodate adding lines for index alleles
      gamete[3,Ix] <- genotype[homologue+4,Ix] ## change to accomodate adding lines for origin alleles
    }
    homologue = 3-homologue
  }
  return(gamete)
  
  
}



completeInterference <- function(position, genome) 
{
  
  ## obligatory crossover
  ## function to create the gamete of a diploid individual by complete interference (one crossover)
  ## creates the gamete for one pair of chromosomes
  ## arguments : position = vector : position (in cM) on the chromosome of the markers for one chromosome
  ##             genome = matrix : effect, index, type and origin of the alleles of the different markers (+dominance) for one chromosome of one individual
  ## return a matrix (size 3xNbQTL)
  
  gamete <- matrix(NA, nrow = 3, ncol = length(position[1,]))
  k <- 1:length(position[1,])
  xrand <- 2*runif(1)
  kstar <- floor(length(position[1,])*xrand + 0.5)
  homologue <- sample(1:2,1)
  gamete[,k<=kstar] <- genome[c(homologue,homologue+2, homologue+4),k<=kstar]
  gamete[,k<=length(position[1,]) & k>kstar] <- genome[c(3-homologue, 3-homologue+2, 3-homologue+4),k<=length(position[1,]) & k>kstar]
  return(gamete)
}


freeRecombination <- function(position, genome) 
{
  ## without linkage
  ## function to create the gamete of a diploid individual without linkage
  ## i.e. there can be a crossover on each QTL
  ## creates the gamete for one pair of chromosomes
  ## arguments : position = vector : position (in cM) on the chromosome of the markers for one chromosome
  ##             genome = matrix : effect and origin of the alleles of the different markers (+dominance) for one chromosome of one individual
  ## return a matrix (size 2xNbQTL)
  
  gamete <- matrix(NA, nrow = 3, ncol = length(position[1,]))
  k <- sample(1:2, length(position[1,]), replace=TRUE)
  gamete[,k == 1] <- genome[c(1,3,5), k == 1]
  gamete[,k == 2] <- genome[c(2,4,6), k == 2]
  return(gamete)
}


selection <- function(N, NbSelection, P, selectionType) 
{
  ## truncation selection
  ## i.e. select only the "NbSelection" best individuals
  ## but also random selection
  ## arguments : N = integer : number of individuals
  ##             NbSelection = integer : number of individual we keep for breeding
  ##             P = vector : contains the phenotypic values of all individuals
  ##             selectionType = character string : "RS" (random selection) or "PS" (selection on the phenotypic values)
  ## return a vector containing the index of the selected individuals
  
  if(selectionType == "RS") { ## if random selection
    selectedIx <- sample(1:N, NbSelection, replace = FALSE)
    return(selectedIx)
  } else { 
    ## if selection on the phenotype ("PS") or rrBLUP ("BLUP", use the estimated phenotype)
    IndexKept <- N - NbSelection + 1
    indSelection <- list()
    ord <- order(P)
    selectedIx <- ord[IndexKept:length(P)]
    return(selectedIx)
  }
}


indexIndividuals <- function(Nselect, Noffspring, reproductionType)
{
  ## return the index of the parents for each child
  ## arguments : Nselect = integer : number of selected parents
  ##             Noffspring = integer : number of individuals in the next generation
  ##             reproductionType = string of character : "HD" (doubled haploid), "SP" (selfing), "SC" (panmixie)
  ## return a vector (HD or selfing) or a matrix (panmixie) containing the index of the parents of each individuals of the next generation
  
  if(reproductionType == "HD" | reproductionType == "SP") {
    ## in case of HD or selfing, return only one parents per child
    k <- ceiling(Noffspring/Nselect)
    ix <- sample(1:Nselect, replace = FALSE)
    # ix2 <- sample(rep(ix, k-1), replace = FALSE) ## used until 17/05/2017
    ix2 <- unlist(replicate(k-1, {sample(ix, replace = FALSE)})) ## better for recycling the parents
    ix <- c(ix, ix2) ## ensure that all parents are used at least once
    return(ix[1:Noffspring])
  } else { ## else panmictic 
    ix <- matrix(NA, ncol = Noffspring, nrow = 2) ## must return two parents for each child
    ix[,1:Noffspring] <- replicate(Noffspring, {sample(1:Nselect, 2, replace = FALSE)})
    return(ix) 
  }
}








